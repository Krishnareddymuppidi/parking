<?php 
header("Cache-Control: no cache");
session_cache_limiter("private_no_expire");
include_once("includes/header.php"); 
include_once("includes/db_connect.php");
$space = $_POST["space_title"];
$space_title= mysqli_real_escape_string($con,enc($space));

$user_details = 'user_details';
define("USER_DETAILS", $user_details);
?>
<?php
    $SQL="SELECT * FROM `location` JOIN `space` ON location.location_id=space.space_location_id WHERE location.location_name='".$space_title."'";
    $rs=mysqli_query($con,$SQL);
?>
<?php
$user_id= $_SESSION[USER_DETAILS]['user_id'];
?>
  <br> <br> <br>
 <section id="maincontent">
 <br>
   <div class="container">
           <fieldset style="width: 100%;">
            <legend>List of available spaces</legend>
            <?php
            if($_REQUEST['msg']) { 
            ?>
                <div class="alert alert-success" role="alert"><?=$_REQUEST['msg']?></div>
            <?php
            }
            if(mysqli_num_rows($rs) > 0) {
            ?>
            <form name="frm_location" action="lib/location.php" method="post">
                <div class="static" id="order_table">
                <table class="table table-striped table-advance table-hover" id="myDatatable" width="100%">
                    <thead>
                        <tr>
                        <th scope="col">Sr. No.</th>
                        <th scope="col">Location Name</th>                                                                
                        <th scope="col">Space Name</th>
                        <!--<th scope="col">Email</th>
                        <th scope="col">Total Space </th>
                        <th scope="col">Vacant Space </th>-->
                        <th scope="col" width="200">&nbsp;</th>
                        <th scope="col" style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $sr_no=1;
                        while($data = mysqli_fetch_assoc($rs))
                        {
                        ?>
                        <tr>
                            <td style="text-align:center; font-weight:bold;"><?=$sr_no++?></td>
                            <td><?=dec($data[location_name])?></td>
                            <td><?=dec($data[space_title])?></td>
                            <td><?//=$data[location_email]?></td>
                            <!--<td style="background-color:#067dc1; color: #FFF; font-weight: bold; text-align: center"><?//=$totalSpace?></td>
                            <td style="background-color:green; color: #FFF; font-weight: bold; text-align: center"><?//=$vacanct?></td>-->
                            <td style="text-align:center">
                            <?php
                            if($_SESSION[USER_DETAILS]['user_level_id'] == 1 || $_SESSION[USER_DETAILS]['user_level_id'] == 4)
                            {
                            ?>
                                <a href="list-space.php?location_id=<?php echo $data[location_id] ?>" class="btn btn-primary">View All Spaces</a>
                            <?php
                            }
                            ?>
                            <?php
                            $SQL2="SELECT * FROM `space` JOIN `documents` ON space.space_id=documents.space_id WHERE documents.doc_status='Approved'";
                            $rs2=mysqli_query($con,$SQL2);
                            $data2 = mysqli_fetch_all($rs2);
                        
                            $arr=[];
                            $space_id = $data['space_id'];
                            array_walk_recursive($data2, function($k){global $arr; $arr[]=$k;});
                                              
                            if(in_array($space_id, $arr))
                            {
                            ?>
                            <!--  For space and location table data fetching  -->
                    <?php
                    $SQL1="SELECT * FROM `parking` WHERE parking_space_id = $data[space_id]";
                    $rs1=mysqli_query($con,$SQL1);
                    $data1 = mysqli_fetch_all($rs1);
                    $tot = mysqli_num_rows($rs1)
                    ?>                    
                        <?php
                        if($user_id == '')
                        {
                        ?>
                        <a class="btn btn-primary" onclick="return logincheck()">Book Now</a>
                        <?php
                        }
                        else if($tot == $data[space_total_parkings])
                        {
                        ?>
                        <a class="btn btn-primary" onclick="return booknow()">Book Now</a>
                        <?php
                        }
                        else
                        {
                        ?>
                        <a href="parking.php?space_id=<?=$data[space_id]?>&user=<?=$_SESSION[USER_DETAILS]['user_id']?>&location=<?=$data[location_name]?>" class="btn btn-primary">Book Now</a>
                        <?php
                        }
                        ?>
                        <?php
                        }
                        else
                        {
                        ?>
                            <a class="btn btn-primary" style="pointer-events: none; background: #b9cbd0;">Book Now</a>
                        <?php
                        }
                        ?>
                        </td>    
                        </tr>
                        <?php 
                        }
                        ?>
                    </tbody>
                </table>
                </div>
                <input type="hidden" name="act" />
                <input type="hidden" name="location_id" />
            </form>
            <?php } else {?>
                <div class="alert alert-success" role="alert">Locations not found.</div>
            <?php } ?>
            </fieldset>
    </div>
    
</section>
<script>
function booknow()
{
    alert("Sorry currently no slots are available in this space please try in other space");
}
function logincheck()
{
    alert("Please login to book parking");
    return false;
}
</script>
<?php  include_once("includes/footer.php"); ?> 