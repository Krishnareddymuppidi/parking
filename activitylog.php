<?php 
    include_once("includes/header.php"); 
    include_once("includes/db_connect.php"); 

?>
<script>
function delete_parking(parking_id)
{
    if(confirm("Do you want to delete the parking?"))
    {
        this.document.frm_parking.parking_id.value=parking_id;
        this.document.frm_parking.act.value="delete_parking";
        this.document.frm_parking.submit();
    }
}
</script>
<script>
jQuery(document).ready(function() {
    jQuery('#mydatatable').DataTable();
});
</script>
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>Activity Log</h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section id="maincontent">
  <div class="container" style="width:900px;">  
  <form>
               <table>
               <tr>
               <td width="200">&nbsp;</td>
             <td><input type="text" name="from_date" id="from_date"  placeholder="From Date" /></td> 
               
                     <td><input type="text" name="to_date" id="to_date" placeholder="To Date" /></td>
            
                     <td><input type="button" name="filter" id="filter" value="Search" class="btn btn-info" style="margin-top: -10px;" /></td>
             
               
                </tr>
</table>                
</form>                
                <br />  
                <div id="order_table">  
                
                </div>  
           </div> 
</section>

<script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                //alert(from_date);
                if(from_date != '' && to_date != '')  
                {  
                     $.ajax({  
                          url:"viewlog.php",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date},  
                          success:function(data)  
                          {  
                               $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Date");  
                }  
           });  
      });  
 </script>
 
<?php include_once("includes/footer.php"); ?> 