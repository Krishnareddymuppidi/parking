<?php 
    include_once("includes/header.php"); 
    if($_REQUEST[location_id])
    {
        $SQL="SELECT * FROM `location` WHERE location_id = $_REQUEST[location_id]";
        $rs=mysqli_query($con,$SQL);
        $data=mysqli_fetch_assoc($rs);
    }
?>
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>Parking Location Entry Form</h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section id="maincontent">
   <div class="container">
           <fieldset>
            <legend>Parking Location Entry Form</legend>
                <?php if($_REQUEST['msg']) { ?>
                    <div class="alert alert-success" role="alert"><?=$_REQUEST['msg']?></div>
                <?php } ?>
                <?php if($_GET['name'] == 'nameerror') { ?>
                    <div class="alert alert-danger" role="alert">Invalid Location Name</div>
                <?php } ?>
                <?php if($_GET['email'] == 'emailerror') { ?>
                    <div class="alert alert-danger" role="alert">Invalid Email Address</div>
                <?php } ?>
                <?php if($_GET['contact'] == 'contacterror') { ?>
                    <div class="alert alert-danger" role="alert">Invalid Contact Number</div>
                <?php } ?>
                <?php if($_GET['zipcode'] == 'zipcodeerror') { ?>
                    <div class="alert alert-danger" role="alert">Invalid Zipcode</div>
                <?php } ?>
                <form action="lib/location.php" enctype="multipart/form-data" method="post" name="frm_location" class="form-horizontal my-forms">
                    
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Name</label> 
                        <div class="controls"><input name="location_name" type="text" class="bar" oninvalid="setCustomValidity('Please Enter Location Name ')" oninput="setCustomValidity('')" minlength="3" maxlength="15" required value="<?=dec($data[location_name])?>"/></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Contact Number</label> 
                        <div class="controls"><input name="location_contact" type="number" class="bar" oninvalid="setCustomValidity('Please Enter Valid Contact Number')" onchange="try{setCustomValidity('')}catch(e){}" title="Please Enter Valid Contact Number" oninput="if(value.length>10)value=value.slice(0,10)" required value="<?=dec($data[location_contact])?>"/></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Email ID</label> 
                        <div class="controls"><input name="location_email" type="email" class="bar" oninvalid="setCustomValidity('Please Enter Valid Email ')" oninput="setCustomValidity('')" required value="<?=dec($data[location_email])?>"/></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Location Latitude</label> 
                        <div class="controls"><input name="location_lat" type="text" class="bar" oninvalid="setCustomValidity('Please Enter Location Latitude ')" oninput="setCustomValidity('')" minlength="3" maxlength="8" required value="<?=dec($data[location_lat])?>"/></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Location Longitude</label> 
                        <div class="controls"><input name="location_log" type="text" class="bar" oninvalid="setCustomValidity('Please Enter Location Longitude ')" oninput="setCustomValidity('')" minlength="3" maxlength="8" required value="<?=dec($data[location_log])?>"/></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Location Zipcode</label> 
                        <div class="controls"><input name="location_zipcode" type="number" class="bar" oninvalid="setCustomValidity('Please Enter Zipcode ')" oninput="setCustomValidity('')" minlength="3" maxlength="8" required value="<?=dec($data[location_zipcode])?>"/></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Full Address</label> 
                        <div class="controls"><textarea name="location_address" cols="" rows="6" maxlength="250" required><?=dec($data[location_address])?></textarea></div>
                    </div>
                    <div class="control-group clear">
                        <div class="controls"><button type="submit" class="btn btn-primary">Save Details</button>
                        <button type="reset" class="btn btn-danger">Reset Details</button> 
                    </div>
                    </div>
                    <input type="hidden" name="act" value="save_location">
                    <input type="hidden" name="location_id" value="<?=$data[location_id]?>">
                </form>
                </fieldset>
    </div>
</section>
<?php include_once("includes/footer.php"); ?> 