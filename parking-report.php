<?php 
    include_once("includes/header.php"); 
    include_once("includes/db_connect.php"); 
    $SQL="SELECT * FROM `parking`,`space` WHERE parking_space_id = space_id";
    $rs=mysqli_query($con,$SQL);
?>
<script>
function delete_parking(parking_id)
{
    if(confirm("Do you want to delete the parking?"))
    {
        this.document.frm_parking.parking_id.value=parking_id;
        this.document.frm_parking.act.value="delete_parking";
        this.document.frm_parking.submit();
    }
}
</script>
<script>
jQuery(document).ready(function() {
    jQuery('#mydatatable').DataTable();
});
</script>
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>All Parking Report</h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section id="maincontent">
   <div class="container">
           <fieldset>
            <legend>All Parking Report</legend>
            <?php
            if($_REQUEST['msg']) { 
            ?>
                <div class="alert alert-success" role="alert"><?=$_REQUEST['msg']?></div>
            <?php
            }
            if(mysqli_num_rows($rs)) {
            ?>
            <form name="frm_parking" action="lib/parking.php" method="post">
                <div class="static">
                <table style="width:100%" id="mydatatable" class="table table-striped table-advance table-hover" >
                    <thead>
                      <tr class="tablehead bold">
                        <td scope="col">ID</td>
                        <td scope="col">Car No</td>
                        <td scope="col">Space</td>
                        <td scope="col">Slot No</td>
                        <td scope="col">Entry Date</td>
                        <td scope="col">Entry Time</td>
                        <td scope="col">Status</td>
                        <td scope="col">Action</td>
                      </tr>
                    </thead>
                    <tbody>
                    <?php 
                    $sr_no=1;
                    while($data = mysqli_fetch_assoc($rs))
                    {
                        if($data[parking_status] == 1)
                        {
                            $sts = "Allocated";
                        }
                        else
                        {
                            $sts = "Deallocated";
                        }
                    ?>
                      <tr>
                        <td><?=$data[parking_id]?></td>
                        <td><?=$data[parking_car_no]?></td>
                        <td><?=dec($data[space_title])?></td>
                        <td><?=$data[parking_slot_number]?></td>
                        <td><?=$data[parking_entry_date]?></td>
                        <td><?=$data[parking_intime]?></td>
                        <td><?=$sts?></td>
                        <td style="text-align:center">
                        <a class="btn btn-primary" href="parking.php?parking_id=<?php echo $data[parking_id] ?>">Edit</a> | 
                        <a class="btn btn-success" href="Javascript:delete_parking(<?=$data[parking_id]?>)">Delete</a></td>
                      </tr>
                    <?php } ?>
                    </tbody>
                    </table>
                </div>
                <input type="hidden" name="act" />
                <input type="hidden" name="parking_id" />
            </form>
            <?php } else {?>
                <div class="alert alert-success" role="alert">Parkings are not available.</div>
            <?php } ?>
            </fieldset>
    </div>
</section>
<?php include_once("includes/footer.php"); ?> 