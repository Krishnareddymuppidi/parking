<?php 
    include_once("includes/header.php");
?> 
<?php
$user_details = 'user_details';
define("USER_DETAILS", $user_details);

$user_level_id = 'user_level_id';
define("USER_LEVEL_ID", $user_level_id);

$user_id = 'user_id';
define("USER_ID", $user_id);
?>
<style>

/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
#mydatatable th
{
background-color: #464444 !important;    
}
</style>
<script>
jQuery(document).ready(function() {
    jQuery('#mydatatable').DataTable();
});
jQuery(document).ready(function() {
    jQuery('#mydatatable1').DataTable();
});
jQuery(document).ready(function() {
    jQuery('#mydatatable2').DataTable();
});
</script>
<section id="subintro">
   <div class="jumbotron subhead" id="overview">
      <div class="container">
         <div class="row">
            <div class="span12">
               <div class="centered">
                  <h3>Dashboard for Car Parking System</h3>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<section id="maincontent">
   <div class="container">
            <?php
            if($_REQUEST['msg']) 
            { 
            ?>
                <div class="msg"><?=$_REQUEST['msg']?></div>
            <?php
            }
            ?>
            <?php
            if($_REQUEST['delmsg']) 
            { 
            ?>
                <div class="msg"><?=$_REQUEST['delmsg']?></div>
            <?php
            }
            ?>
            <div style="text-align: right; margin-top: -19px; font-size: 18px;">Welcome <?php  echo dec($_SESSION[USER_DETAILS]['user_name']); ?></div>
            <fieldset>

            <legend>Welcome to Car Parking System</legend>
                <table style="width: 100%">
                    <tr>
                        <td style="width: 17%; position: absolute;">
                            <?php if($_SESSION[USER_DETAILS][USER_LEVEL_ID] == 1) {?>
                                <ul class='login-home'>
                                        <li><a href="location.php">Add Location</a></li>    
                                        <li><a href="space.php">Add Parking Space</a></li>    
                                        <li><a href="location-listing.php">Assign Parking</a></li>
                                        <li><a href="user.php">Add System User</a></li>
                                        <li><a href="search-parking.php">Search Car</a></li>
                                        <li><a href="location-report.php">Location Report</a></li>
                                        <li><a href="space-report.php">Parking Space Report</a></li>
                                        <li><a href="parking-report.php">Parking Report</a></li>    
                                        <li><a href="user-report.php">System User Report</a></li>    
                                        <li><a href="./user.php?user_id=<?php echo $_SESSION[USER_DETAILS][USER_ID]; ?>">My Account</a></li>
                                        <li><a href="change-password.php">Change Password</a></li>
                                        <li><a href="./lib/login.php?act=logout">Logout</a></li>
                                </ul>
                            <?php } ?>
                            <?php if($_SESSION[USER_DETAILS][USER_LEVEL_ID] == 3) {?>
                                <ul class='login-home'>
                                        <!--<li><a href="./index.php">Home</a></li>
                                        <li><a href="./about.php">About Us</a></li>
                                        <li><a href="locations.php">All Parking Locations</a></li> -->
                                        <li><a href="operatorbookparking.php">Book Parking</a></li>
                                        <li><a href="./user.php?user_id=<?php echo $_SESSION[USER_DETAILS][USER_ID]; ?>">My Account</a></li>
                                        <!--<li><a href="mybookings.php">My Bookings</a></li>-->
                                        <li><a href="change-password.php">Change Password</a></li>
                                        <li><a href="./lib/login.php?act=logout">Logout</a></li>
                                </ul>
                            <?php } ?>
                            <?php if($_SESSION[USER_DETAILS][USER_LEVEL_ID] == 2) {?>
                            <ul class='login-home'>
                            <li><a href="documents.php">Upload Documents</a></li>
                            </ul>
                            <?php
                            }
                            ?>
                            <?php if($_SESSION[USER_DETAILS][USER_LEVEL_ID] == 2 || $_SESSION[USER_DETAILS][USER_LEVEL_ID] == 4) {?>
                                <ul class='login-home'>
                                        <li><a href="operatorbookparking.php">Book Parking</a></li>
                                        <li><a href="operatorreport.php">Operator Report</a></li>
                                        <li><a href="slotscheck.php">Slots Check</a></li>
                                        <li><a href="./user.php?user_id=<?php echo $_SESSION[USER_DETAILS][USER_ID]; ?>">My Account</a></li>
                                        <li><a href="change-password.php">Change Password</a></li>
                                        <li><a href="./lib/login.php?act=logout">Logout</a></li>
                                </ul>
                            <?php } ?>
                        </td>
                        <td style="width: 80%; vertical-align: top; padding-top: 11px;">
<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'Mybookings')" id="defaultOpen">My Slots</button>
  </div>
<?php
     $created_by =  $_SESSION[USER_DETAILS]['created_by'];
     $space_id= $_SESSION[USER_DETAILS]['user_space_id'];
     $query = "SELECT * FROM `space` WHERE space_owner_id = '".$created_by."' AND space_id = '".$space_id."'";
     $rs=mysqli_query($con,$query);
     if(mysqli_num_rows($rs)) 
         {
        $data = mysqli_fetch_assoc($rs);
?>
<div id="Mybookings" class="tabcontent">
Total Slots:  <?php echo $total_slots = $data['space_total_parkings'];  ?>
<?php
$query1 = "SELECT * FROM `space` JOIN parking ON space.space_id=parking.parking_space_id WHERE space.space_owner_id = '".$created_by."' AND space.space_id = '".$space_id."'";
$rs1=mysqli_query($con,$query1);
$filled = mysqli_num_rows($rs1);
?><br>
Booked Slots: <?=$filled?> <br>
Available Slots: <?=$total_slots-$filled ?>
</div>
<?php
         }
         else
         {
             echo "No slots";
         }
         ?>
<div class="modal fade" id="viewlog" role="dialog">
    <div class="modal-dialog">
 
     <!-- Modal content-->
     <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Booking Info</h4>
        <button type="button" class="close" data-dismiss="modal" style="margin-top: -46px;">&times;</button>
      </div>
      <div class="modal-body">
 
      </div>
     <!-- <div class="modal-footer">
       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>-->
     </div>
    </div>
   </div>


                        </td>
                        </tr>
                </table>
            </fieldset>
   </div>
</section>
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
<!--  For customer slot cancellation  -->
<script>
function cancelslot(anchor)
{
   var conf = confirm('Are you sure want to delete this slot?');
   if(conf)
      window.location=anchor.attr("href");
}
</script>
<script>
$(document).ready(function(){

 $('.userinfo').click(function(){
   
   var userid = $(this).data('id');

   // AJAX request
   $.ajax({
    url: 'viewslot.php',
    type: 'post',
    data: {userid: userid},
    success: function(response){ 
      // Add response in Modal body
      $('.modal-body').html(response);

      // Display Modal
      $('#viewlog').modal('show'); 
    }
  });
 });
});
</script>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<?php include_once("includes/footer.php"); ?>