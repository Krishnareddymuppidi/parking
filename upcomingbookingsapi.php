<?php
include_once("includes/db_connect.php");
header('Access-Control-Allow-Headers: *'); 
header('Access-Control-Allow-Methods: POST, GET, PUT, OPTIONS, PATCH, DELETE');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Authorization, Content-Type, x-xsrf-token, x_csrftoken, Cache-Control, X-Requested-With');
header("Content-Type:application/json");
 if($_SERVER['REQUEST_METHOD'] == "POST"){
    
   
   
	//include('db.php');
	$parking_user_id = $_POST['parking_user_id'];
	$date = date('Y-m-d');
	$SQLQRY="SELECT parking_id, parking_space_id, space_title, parking_slot_number, parking_car_no, parking_intime, parking_entry_date, parking_qr_image, parking_space_image, parking_outtime, parking_exit_date FROM parking JOIN space ON parking.parking_space_id=space.space_id JOIN user ON parking.parking_user_id=user.user_id WHERE parking.parking_date>'$date' AND parking.parking_user_id =$parking_user_id ORDER BY parking.parking_id DESC";
	$result = mysqli_query($con,$SQLQRY);
	if(mysqli_num_rows($result)>0){
	   $rows = array();

//retrieve every record and put it into an array that we can later turn into JSON
$i=0;
while($r = mysqli_fetch_assoc($result)){
    $image=dec($r['parking_space_image']);
    $rows[$i][parking_id] = $r['parking_id'];
    $rows[$i][parking_space_id] = $r['parking_space_id'];
    $rows[$i][space_title] = dec($r['space_title']);
    $rows[$i][parking_slot_number] = $r['parking_slot_number'];
    $rows[$i][parking_car_no] = $r['parking_car_no'];
    $rows[$i][parking_intime] = $r['parking_intime'];
    $rows[$i][parking_entry_date] = $r['parking_entry_date'];
    $rows[$i][parking_outtime] = $r['parking_outtime'];
    $rows[$i][parking_exit_date] = $r['parking_exit_date'];
    $rows[$i][parking_qr_image] = 'https://eparkingnext.com/PMS/qr_images/'.$r['parking_qr_image'];
    $rows[$i][parking_space_image] = 'https://eparkingnext.com/PMS/spaces/'.$image;
    $rows[$i][message] = 'success';
$i++;
}

//echo result as json
echo json_encode($rows);


	
	mysqli_close($con);
	}else{
		//response(NULL, NULL, 200,"No Record Found");
	
    	
            //$response["message"] = "Bookings not found";

            //echo json_encode($response);exit;
            $rows = array(
                "message" => "Bookings not found"
            ); 
		echo json_encode([$rows]);
		}
}

?>