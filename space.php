<?php 
    include_once("includes/header.php"); 
    if($_REQUEST[space_id])
    {
        $SQL="SELECT * FROM `space` WHERE space_id = $_REQUEST[space_id]";
        $rs=mysqli_query($con,$SQL);
        $data=mysqli_fetch_assoc($rs);
    }
?>
<script>

jQuery(function() {
    jQuery( "#space_from_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
       yearRange: "0:+1",
       dateFormat: 'd MM,yy'
    });
    jQuery( "#space_to_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
       yearRange: "0:+1",
       dateFormat: 'd MM,yy'
    });
});
</script> 
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>Parking Space Entry Form</h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section id="maincontent">
   <div class="container">
           <fieldset>
            <legend>Parking Space Entry Form</legend>
                <?php if($_REQUEST['msg']) { ?>
                    <div class="alert alert-success" role="alert"><?=$_REQUEST['msg']?></div>
                <?php } ?>
                <form action="lib/space.php" enctype="multipart/form-data" method="post" name="frm_space" class="form-horizontal my-forms">
                <div class="control-group">
                        <label class="control-label" for="inputEmail">Select Location</label> 
                        <div class="controls">
                            <select name="space_location_id" id="space_location_id" class="bar" required>
                            <option value="">Select Location</option>
                            <?php
                            $sql = mysqli_query($con, "SELECT * FROM location WHERE location_owner_id =". $_SESSION['user_details']['user_id']);
                            $row = mysqli_num_rows($sql);
                            while ($row = mysqli_fetch_array($sql)){
                            echo "<option value='". $row['location_id'] ."'>" .dec($row['location_name']) ."</option>" ;
                            }
                            ?>
                            </select>
                            </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Space Title</label> 
                        <div class="controls"><input name="space_title" type="text" class="bar" oninvalid="setCustomValidity('Please Enter Space Title ')" oninput="setCustomValidity('')" minlength="3" maxlength="15" required value="<?=dec($data[space_title])?>"/></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Total Parking Spaces</label> 
                        <div class="controls"><input name="space_total_parkings" type="number" class="bar" oninvalid="setCustomValidity('Please Enter Total Parking Spaces ')" oninput="setCustomValidity('')" required value="<?=$data[space_total_parkings]?>"/></div>
                    </div>
                    <div class="control-group">
                    <label class="control-label" for="inputEmail">Space Image</label> 
                    <div class="controls"><input name="space_image" type="file" class="bar" /></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Description</label> 
                        <div class="controls"><textarea name="space_description" oninvalid="setCustomValidity('Please Enter Description ')" oninput="setCustomValidity('')" cols="" rows="6" maxlength="500" required><?=dec($data[space_description])?></textarea></div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Amenities</label> 
                        <div class="controls"><textarea name="space_amenities" cols="" rows="6" maxlength="500" oninvalid="setCustomValidity('Please Enter Amenities ')" oninput="setCustomValidity('')" required><?=dec($data[space_amenities])?></textarea></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Access Hours</label> 
                        <div class="controls"><textarea name="space_hours" cols="" rows="6" maxlength="500" oninvalid="setCustomValidity('Please Enter Access Hours ')" oninput="setCustomValidity('')" required><?=dec($data[space_hours])?></textarea></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Things you should know</label> 
                        <div class="controls"><textarea name="space_things" cols="" rows="6" maxlength="500" oninvalid="setCustomValidity('Please Enter The Things you should know ')" oninput="setCustomValidity('')" required><?=dec($data[space_things])?></textarea></div>
                    </div>
                    
                    <div class="control-group clear">
                        <div class="controls">
                            <button type="submit" class="btn btn-primary">Save Details</button>
                            <button type="reset" class="btn btn-danger">Reset Details</button> 
                        </div>
                    </div>
                    <input type="hidden" name="act" value="save_space">
                    <input type="hidden" name="space_id" value="<?=$data[space_id]?>">
                </form>
                </fieldset>
    </div>
</section>
<?php include_once("includes/footer.php"); ?> 