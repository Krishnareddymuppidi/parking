<?php 
    include_once("includes/header.php"); 
    include_once("includes/db_connect.php"); 
    include('phpqrcode/qrlib.php'); 
    $parkingId=$_REQUEST['parking_id'];
    global $SERVER_PATH;
    $SQL="SELECT * FROM `space`, `parking`, `location` WHERE space_location_id = location_id AND parking_space_id = space_id AND parking_id = ".$_REQUEST['parking_id'];
    $rs=mysqli_query($con,$SQL);
    $data = mysqli_fetch_assoc($rs);
    $url = "parking_slip.php?msg=".$msg."&parking_id=".$parkingId;
?>

<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>Parking Slip</h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<div style="text-align: center; font-size: 18px; font-weight: bold;">Thank you for booking the slot :: Your booking details are</div><br>
<section id="maincontent">
   <div class="container">
           <fieldset>
           
<form action="lib/user.php" enctype="multipart/form-data" method="post" name="frm_user" onsubmit="return validateForm(this)" class="form-horizontal my-forms">

<style>
    td 
    {
        text-align:center;
    }
    #myBtn1 
    {
    position: absolute;
    top: 203px;
    right: 30px;
    z-index: 99;
    font-size: 14px;
    border: none;
    outline: none;
    background-color: #385ee7;
    color: white;
    cursor: pointer;
    padding: 5px;
    border-radius: 4px;
    }
    #myBtn
    {
        display: none !important;
    }
</style>
<?php 
$parking_car_no = $data['parking_car_no'];
 $text=$parking_car_no;
 
$text_dec=$parking_car_no;
$path = 'qr_images/'; 
$file = $path.$text_dec.".png"; 
$parking_qr_image = $text_dec.".png";
$temp_name = $text_dec.".png";
$path_filename_ext = $path.$text_dec.".png"; 
$ecc = 'L'; 
$pixel_Size = 10; 
$frame_Size = 10; 
 move_uploaded_file($temp_name,$path_filename_ext);
QRcode::png($text_dec, $file, $ecc, $pixel_Size, $frame_Size); 
?>
<?php
$parking_exit_date = $data['parking_exit_date'];
$exit_date = date('d F,Y',strtotime($parking_exit_date));

?>
<?php

$SQL1 = "UPDATE `parking` SET parking_qr_image = '$parking_qr_image' WHERE `parking_id` = ".$parkingId;
$rs = mysqli_query($con,$SQL1);

?>
<a href="login-home.php" id="myBtn1" title="Go to top" style="display: block;">Back</a>
<div align="center">
<div id="dvContainer">
<table style="text-align:center">
    <tr><td><h3>Vehicle No: <?=$parking_car_no?></h3></td></tr>
    <tr><td><h3>Parking Location Name: <?=dec($data['location_name'])?></h3></td></tr>
    <tr><td><h3>Parking Space Name: <?=dec($data['space_title'])?></h3></td></tr>
    <tr><td><h3>Parking Intime: <?=$data['parking_intime']?></h3></td></tr>
    <tr><td><h3>Parking Outtime: <?=$data['parking_outtime']?></h3></td></tr>
    <tr><td><h3>Parking Entry Date: <?=$data['parking_entry_date']?></h3></td></tr>
    <tr><td><h3>Parking Exit Date: <?=$exit_date?></h3></td></tr>
    <tr><td><h3>Parking Charges: <?=dec($data['parking_charges'])?></h3></td></tr>
    <tr><td><img src='<?=$file?>' style='height:260px; width:260px'></td></tr>
    <tr><td><?//=$data['parking_entry_date']?> <?//=$data['parking_intime']?></td></tr>
    <tr><td style="font-weight:bold"><?//=$data['location_address']?></td></tr>
</table></div>
<!--<input type="button" class="btn btn-danger" value="Print" onClick="window.open('<?//=$url?>', '_blank', 'toolbar=0,location=0,menubar=0,width=710,height=555,left=160,top=170')">-->
<input type="button" class="btn btn-danger" value="Print" id="btnPrint" >

</div>
</form>
        </fieldset>
    </div>
</section>
<script type="text/javascript">
        $("#btnPrint").live("click", function () {
            var divContents = $("#dvContainer").html();
            var printWindow = window.open('', '', 'height=700,width=1000,overflow:hidden');
            printWindow.document.write('<html><head>');
            printWindow.document.write('</head><body>');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
    </script>
<?php include_once("includes/footer.php"); ?>