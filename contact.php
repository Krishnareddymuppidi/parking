<?php include_once("includes/header.php"); ?>
  <section id="subintro">
    <div class="jumbotron subhead" id="overview">
      <div class="container">
        <div class="row">
          <div class="span12">
            <div class="centered">
              <h3>Contact page</h3>
              <!--<p>
                Lorem ipsum dolor sit amet, modus salutatus honestatis ex mea. Sit cu probo putant. Nam ne impedit atomorum.
              </p>-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="breadcrumb">
    <div class="container">
      <div class="row">
        <div class="span12">
          <ul class="breadcrumb notop">
            <li><a href="#">Home</a><span class="divider">/</span></li>
            <li class="active">Contact</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <section id="maincontent">
    <div class="container">
      <div class="row">
        <div class="span4">
          <aside>
            <div class="widget">
              <h4>Get in touch with us</h4>
              <ul>
                <li><label><strong>Phone : </strong></label>
                  <p>
                    +900 888 707 123
                  </p>
                </li>
                <li><label><strong>Email : </strong></label>
                  <p>
                    <a href="mailto:info@datadotlabs.com" style="color: #7f6666;">info@datadotlabs.com</a>
                  </p>
                </li>
                <li><label><strong>Adress : </strong></label>
                  <p>
                    ASIA PACIFIC HQ - MALAYSIA,<br>
                    #55-11, The Boulevard Office, Mid Valley, 59200, <br> Kuala Lumpur.
                  </p>
                </li>
              </ul>
            </div>
            <div class="widget">
              <h4>Social network</h4>
              <ul class="social-links">
                <li><a href="#" title="Twitter"><i class="icon-rounded icon-32 icon-twitter"></i></a></li>
                <li><a href="#" title="Facebook"><i class="icon-rounded icon-32 icon-facebook"></i></a></li>
                <li><a href="#" title="Google plus"><i class="icon-rounded icon-32 icon-google-plus"></i></a></li>
                <li><a href="#" title="Linkedin"><i class="icon-rounded icon-32 icon-linkedin"></i></a></li>
                <li><a href="#" title="Pinterest"><i class="icon-rounded icon-32 icon-pinterest"></i></a></li>
              </ul>
            </div>
          </aside>
        </div>
        <div class="span8">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3983.908305781518!2d101.67528441475719!3d3.11895399772887!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cc49892dcb3503%3A0xd6ed2b2df5587e22!2sDSS%20SOFTWARE%20SOLUTIONS%20SDN%20BHD!5e0!3m2!1sen!2sin!4v1616129105203!5m2!1sen!2sin" width="770" height="500" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

          <div class="spacer30"></div>
          <div id="sendmessage">Your message has been sent. Thank you!</div>
          <div id="errormessage"></div>
          <form action="" method="post" role="form" class="contactForm">
            <div class="row">
              <div class="span4 form-group">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" oninvalid="setCustomValidity('Please Enter Name With Minimum 4 Charecters!')" oninput="setCustomValidity('')" maxlength="20" minlength="4" pattern="[A-Za-z\s]+" required="" />
                <div class="validation"></div>
              </div>

              <div class="span4 form-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" oninvalid="setCustomValidity('Please Enter Email')" oninput="setCustomValidity('')" required />
                <div class="validation"></div>
              </div>
              <div class="span8 form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" oninvalid="setCustomValidity('Please Enter Subject')" oninput="setCustomValidity('')" maxlength="30" minlength="4" required />
                <div class="validation"></div>
              </div>
              <div class="span8 form-group">
                <textarea class="form-control" name="message" rows="5" maxlength="250" placeholder="Message"></textarea>
                <div class="validation"></div>
                <div class="text-center">
                  <button class="btn btn-color btn-rounded" type="submit">Send message</button>
                </div>
              </div>
            </div>
          </form>

        </div>
      </div>
    </div>
  </section>
  <!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></script>-->
  <!-- Contact Form JavaScript File -->
  <!--<script src="${contextPath}/resources/contactform/contactform.js"></script>

  <script src="${contextPath}/resources/assets/js/custom.js"></script>-->
<?php include_once("includes/footer.php"); ?>
