<?php 
    include_once("includes/header.php"); 
    include_once("includes/db_connect.php");
    
?>
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>Book Parking Space</h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>

 <section id="maincontent">
   <div class="container">
           <fieldset style="width: 950px;">
            <legend>List of available spaces</legend>
            <?php
            if($_REQUEST['msg']) { 
            ?>
                <div class="alert alert-success" role="alert"><?=$_REQUEST['msg']?></div>
            <?php
            }
            ?>
            <form name="frm_location" action="lib/location.php" method="post">
                <div class="static" id="order_table">
                <table class="table table-striped table-advance table-hover" id="myDatatable" style="width:100%;">
                    <thead>
                        <tr>
                       
                      <th scope="col">Location Name</th>                                                             
                        <th scope="col">Space Name</th>
                     
                        <th scope="col" width="200">&nbsp;</th>
                        <th scope="col" style="text-align: center;">Action</th>
                        
                        </tr>
                    </thead>
                    <tbody>
                      <?php
                      $created_by =  $_SESSION['user_details']['created_by'];
                      $user_space_id =  $_SESSION['user_details']['user_space_id'];
                      ?>
                        <?php
                $query = "SELECT * FROM `space` JOIN user ON space.space_id=user.user_space_id JOIN location ON space.space_location_id=location.location_id WHERE user.created_by = '".$created_by."' AND user.user_space_id = '".$user_space_id."'";
                $rs=mysqli_query($con,$query);
                if(mysqli_num_rows($rs)) 
                {
                while($data = mysqli_fetch_assoc($rs))
                {
                
                ?>
                        <tr>
                            <td><?php echo dec($data['location_name']);  ?></td>
                            <td><?php echo dec($data['space_title']);  ?></td>
                            <td><?//=$data[location_email]?></td>
                            <!--<td style="background-color:green; color: #FFF; font-weight: bold; text-align: center"><?//=$vacanct?></td>-->
                            <td style="text-align:center">
                            <a href="parking.php?space_id=<?=$data[space_id]?>&user=<?=$_SESSION['user_details']['user_id']?>&location=<?=$data[location_name]?>&mode=<?=operator?>" class="btn btn-primary">Book Now</a>
                        
                            </td>    
                        </tr>
                      <?php
                }
                }
                else
                {
                    echo "Bookings are not available";
                }
                ?>
                    </tbody>
                </table>
                </div>
                <input type="hidden" name="act" />
                <input type="hidden" name="location_id" />
            </form>
            
            </fieldset>
    </div>
</section>
<script>
function booknow()
{
    alert("Sorry currently no slots are available in this space please try in other space");
}
</script>
<?php include_once("includes/footer.php"); ?> 