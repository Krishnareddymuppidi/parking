<?php
    include_once("includes/header.php"); 
    include_once("includes/db_connect.php"); 
?>

<style>
/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
#mydatatable th
{
background-color: #464444 !important;    
}
.tab button
{
 font-size: 13px !important;
}
form
{
 margin: 0 0 0px !important;
}
.message
{
margin-bottom: 32px;
font-size: 18px;
color: #a98e0c;
}
#btnExportdel
{
    background: #004a87;
    color: #fff;
    border: 2px solid #004a87;
    border-radius: 4px;	
	margin-top: -75px;
}
</style>
<script>
jQuery(document).ready(function() {
    jQuery('#mydatatable').DataTable();
});
</script>

<section id="subintro">
   <div class="jumbotron subhead" id="overview">
      <div class="container">
         <div class="row">
            <div class="span12">
               <div class="centered">
                  <h3>Report for <?php echo dec($_SESSION['user_details']['user_name']); ?></h3>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<section id="maincontent">
   <div class="container">
            <?php
            if($_REQUEST['msg']) 
            { 
            ?>
                <div class="msg"><?=$_REQUEST['msg']?></div>
            <?php
            }
            ?>
            <?php
            if($_REQUEST['delmsg']) 
            { 
            ?>
                <div class="msg"><?=$_REQUEST['delmsg']?></div>
            <?php
            }
            ?>
			
			<?php
			$msg=$_GET['message'];
			if($msg == 'success')
			{
			?>	
			<div class="message" align="center">Email sent successfully</div>
			<?php
			}
			?>
            <div style="text-align: right; margin-top: -19px; font-size: 18px;">Welcome <?php  echo dec($_SESSION['user_details']['user_name']); ?></div>
            
            <table style="width: 100%">
                <tr style="border: 0;">
                    <td style="width: 17%; position: absolute; border: 0;">
                        <?php if($_SESSION['user_details']['user_level_id'] == 1) {?>
                            <ul class='login-home'>
                                    <li><a href="location.php">Add Location</a></li>    
                                    <li><a href="space.php">Add Parking Space</a></li>    
                                    <li><a href="location-listing.php">Assign Parking</a></li>
                                    <li><a href="user.php">Add System User</a></li>
                                    <li><a href="search-parking.php">Search Car</a></li>
                                    <li><a href="location-report.php">Location Report</a></li>
                                    <li><a href="space-report.php">Parking Space Report</a></li>
                                    <li><a href="parking-report.php">Parking Report</a></li>    
                                    <li><a href="user-report.php">System User Report</a></li>    
                                    <li><a href="./user.php?user_id=<?php echo $_SESSION['user_details']['user_id']; ?>">My Account</a></li>
                                    <li><a href="change-password.php">Change Password</a></li>
                                    <li><a href="./lib/login.php?act=logout">Logout</a></li>
                            </ul>
                        <?php } ?>
                        <?php if($_SESSION['user_details']['user_level_id'] == 3) {?>
                            <ul class='login-home'>
                                    <!--<li><a href="./index.php">Home</a></li>
                                    <li><a href="./about.php">About Us</a></li>
                                    <li><a href="locations.php">All Parking Locations</a></li> -->
                                    <li><a href="operatorbookparking.php">Book Parking</a></li>
                                    <li><a href="./user.php?user_id=<?php echo $_SESSION['user_details']['user_id']; ?>">My Account</a></li>
                                    <!--<li><a href="mybookings.php">My Bookings</a></li>-->
                                    <li><a href="change-password.php">Change Password</a></li>
                                    <li><a href="./lib/login.php?act=logout">Logout</a></li>
                            </ul>
                        <?php } ?>
                        <?php if($_SESSION['user_details']['user_level_id'] == 2) {?>
                        <ul class='login-home'>
                        <li><a href="documents.php">Upload Documents</a></li>
                        </ul>
                        <?php
                        }
                        ?>
                        <?php if($_SESSION['user_details']['user_level_id'] == 2 || $_SESSION['user_details']['user_level_id'] == 4) {?>
                            <ul class='login-home'>
                                    <li><a href="operatorbookparking.php">Book Parking</a></li>
                                    <li><a href="operatorreport.php">Operator Report</a></li>
                                    <li><a href="slotscheck.php">Slots Check</a></li>
                                    <li><a href="vehicledetails.php">Vehicle Check</a></li>
                                    <li><a href="./user.php?user_id=<?php echo $_SESSION['user_details']['user_id']; ?>">My Account</a></li>
                                    <li><a href="change-password.php">Change Password</a></li>
                                    <li><a href="./lib/login.php?act=logout">Logout</a></li>
                            </ul>
                        <?php } ?>
                    </td>
                    <td style="border: 0; width: 80%; vertical-align: top; padding-top: 11px;">


<?php
$space_id = $_SESSION['user_details']['user_space_id'];
$SQLQRY="SELECT * FROM `space` WHERE space_id = '".$space_id."'";    
$rsset=mysqli_query($con,$SQLQRY);
$datafetch = mysqli_fetch_array($rsset);
$location = $datafetch['space_location_id'];

$SQLQRY1="SELECT * FROM `location` WHERE location_id = '".$location."'";    
$rsset1=mysqli_query($con,$SQLQRY1);
$datafetch1 = mysqli_fetch_array($rsset1);
$locname = dec($datafetch1[location_name]);
?>
<h3 style="margin-top: -49px;">Welcome to Car Parking System   &nbsp;&nbsp;  Location Name: <?php  echo $locname; ?></h3>

<div class="tab">
<button class="tablinks" onclick="openCity(event, 'DRR')" id="defaultOpen">Daily Revenue Report</button>
<button class="tablinks" onclick="openCity(event, 'DCR')">Daily Cancellations Report</button>
<button class="tablinks" onclick="openCity(event, 'Walk-in')">Walk-ins Report</button>
<!--<button class="tablinks" onclick="openCity(event, 'EORD')">Entry of Reservations Done Report</button>-->
<button class="tablinks" onclick="openCity(event, 'EOD')">EOD Delinquency Report</button>
</div>
<form>
<div id="DRR" class="tabcontent">
<div>Select Date: <input type="date" name="date" id="date" class="date" >
</div>
<?php
$user_id = $_SESSION['user_details']['user_id'];
?>
<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>" >
<div id="load" style="display:none" align="center"><img src="images/preloader.gif"/></div>
<div id="order_table">               
</div> 
</div>
</form>

<form>
<div id="DCR" class="tabcontent">
<div>
Select Date: <input type="date" name="date1" id="date1" class="date1" >
</div>
<?php
$user_id1 = $_SESSION['user_details']['user_id'];
?>
<input type="hidden" name="user_id1" id="user_id1" value="<?php echo $user_id1; ?>" >
<div id="load1" style="display:none" align="center"><img src="images/preloader.gif"/></div>
<div id="order_table1">               
</div></div>
</form>

<form>
<div id="Walk-in" class="tabcontent">
<div>
From: <input type="date" name="date2" id="date2" class="date2" >
&nbsp;&nbsp; To: <input type="date" name="date3" id="date3" class="date3" >
</div>
<?php
$user_id2 = $_SESSION['user_details']['user_id'];
?>
<input type="hidden" name="user_id2" id="user_id2" value="<?php echo $user_id2; ?>" >
<div id="load2" style="display:none" align="center"><img src="images/preloader.gif"/></div>
<div id="order_table2">               
</div></div>
</form>

<form>
<div id="EOD" class="tabcontent">
<?php
$date= date('Y-m-d');
$new_date = date('d-m-Y', strtotime($date));
?>

<h4>Delinquency report on today(<?=$new_date ?>)</h4>
 <p style="text-align: right;" ><input type="button" id="btnExportdel" value="Export to pdf" /></p>
<table style="width:100%" id="mydatatable" class="table table-striped table-advance table-hover" >
                    <thead>
                      <tr class="tablehead bold">
                        <td scope="col">ID</td>
                        <!--<td scope="col">Parking Slot Number </td>-->
                        <td scope="col">Parking Car Number</td>
                        <td scope="col">Parking Charges    </td>
                        <td scope="col">Booking From Date</td>
                        <td scope="col">Booking To Date</td>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
$user_space_id = $_SESSION['user_details']['user_space_id'];
$today = date('Y-m-d');
$SQL="SELECT * FROM `parking` WHERE parking_space_id = '".$user_space_id."' AND parking_exit_date < '".$today."'";    
    $rs=mysqli_query($con,$SQL);
    $sr_no=1;
    if(mysqli_num_rows($rs)) {
                    while($data = mysqli_fetch_assoc($rs))
                    {
                    ?>
                    <?php
                    $parking_booked_date = $data[parking_booked_date];
                    $parking_exit_date = $data[parking_exit_date];
                    $new_parking_booked_date = date('d-m-Y', strtotime($parking_booked_date));
                    $new_parking_exit_date = date('d-m-Y', strtotime($parking_exit_date));
                    ?>
                      <tr>
                        <td><?=$sr_no?></td>
                        <!--<td><?//=$data[parking_slot_number]?></td>-->
                        <td><?=$data[parking_car_no]?></td>
                        <td><?=dec($data[parking_charges])?></td>
                        <td><?=$new_parking_booked_date?></td>
                        <td><?=$new_parking_exit_date?></td>
                      </tr>
                    <?php } 
    } else {
                    ?>
                    <div class="alert alert-success" role="alert"></div>
                    <?php } ?>
                    </tbody>
                    </table>
            </div>
</form>
                    </td>
                    </tr>
            </table>
   </div>
</section>
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
<!--  For customer slot cancellation  -->
<script>
function cancelslot(anchor)
{
   var conf = confirm('Are you sure want to delete this slot?');
   if(conf)
      window.location=anchor.attr("href");
}
</script>
<script>
$(document).ready(function(){

 $('.userinfo').click(function(){
   
   var userid = $(this).data('id');

   // AJAX request
   $.ajax({
    url: 'viewslot.php',
    type: 'post',
    data: {userid: userid},
    success: function(response){ 
      // Add response in Modal body
      $('.modal-body').html(response);

      // Display Modal
      $('#viewlog').modal('show'); 
    }
  });
 });
});
</script>
 <!-- For Daily revenue report  -->
<script>  
      $(document).ready(function(){  
          $('.date').on('change', function(){
               //event.preventDefault();
                var date = $('#date').val();
                var user_id = $('#user_id').val();                 
                //alert(daily_date);
                if(date != '')  
                {  
            $("#load").delay(3000).css("display","block");
                     $.ajax({  
                          url:"dailyrevenueop.php",  
                          method:"POST",  
                          data:{date:date,user_id:user_id},  
                          success:function(data)  
                          {  
                         $("#load").css("display","none");
                         $('#order_table').html(data);            
                          }  
                     });  
                }                  
           });  
      });  
 </script>
 <!-- Daily revenue report ends  -->
 
 <!-- For Daily cancellation report  -->
<script>  
      $(document).ready(function(){  
          $('.date1').on('change', function(){
               //event.preventDefault();
                var date = $('#date1').val();
                var user_id = $('#user_id1').val();                 
                //alert(daily_date);
                if(date != '')  
                {  
            $("#load1").delay(3000).css("display","block");
                     $.ajax({  
                          url:"dailycancelop.php",  
                          method:"POST",  
                          data:{date:date,user_id:user_id},  
                          success:function(data)  
                          {  
                         $("#load1").css("display","none");
                          $('#order_table1').html(data);            
                          }  
                     });  
                }                  
           });  
      });  
 </script>
 <!-- Daily cancellation report ends  -->
 
  <!-- For Walk-in report  -->
<script>  
      $(document).ready(function(){  
          $('.date2,.date3').on('change', function(){
               //event.preventDefault();
                var date2 = $('#date2').val();
                var date3 = $('#date3').val();
                if(date2 == '')
                {
                    alert("Please select from date");
                }
                else if(date3 == '')
                {
                    alert("Please select to date");
                }
                else if(date2>date3)
                {
                    alert("From date should be less than To date");
                }
                else
                {
                var user_id = $('#user_id2').val();                 
                //alert(daily_date);
                if(date != '')  
                {  
            $("#load2").delay(3000).css("display","block");
                     $.ajax({  
                          url:"dailywalkop.php",  
                          method:"POST",  
                          data:{date2:date2,date3:date3,user_id:user_id},  
                          success:function(data)  
                          {  
                         $("#load2").css("display","none");
                               $('#order_table2').html(data);            
                          }  
                     });  
                } 
                }                
           });  
      });  
 </script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
   
 <script>
$('#btnExportdel').click(function() {
  var pdf = new jsPDF();  
  pdf.addHTML($("#mydatatable"), function() {
	pdf.save('Delinquency-report.pdf');
  });
});
   </script>
 <!-- Daily Walk-in report ends  -->

<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<?php include_once("includes/footer.php"); ?>