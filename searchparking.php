<?php 
    //include_once("includes/header.php"); 
    session_start();
    include_once("includes/db_connect.php");
    $location_zipcode= enc($_REQUEST["location_zipcode"]);  
    $zipcode = substr($location_zipcode, 0, 3);
    
    $user_details = 'user_details';
    define("USER_DETAILS", $user_details);
?>
<style>
@media (max-width:450px){
#myDatatable
{
    overflow-x: scroll !important;
    max-width: 40% !important;
    display: block !important;
    white-space: nowrap !important;
}
}
</style>
<?php
    $SQL="SELECT * FROM `location` JOIN `space` ON location.location_id=space.space_location_id WHERE location.location_zipcode LIKE '".$zipcode."%'";
    $rs=mysqli_query($con,$SQL);

 ?>
 <section id="maincontent">
   <div class="container">
           <fieldset style="width: 950px;">
            <legend>List of available parking spaces close to zipcode <?php echo dec($location_zipcode);  ?></legend>
            <?php
            if($_REQUEST['msg']) { 
            ?>
                <div class="alert alert-success" role="alert"><?=$_REQUEST['msg']?></div>
            <?php
            }
            if(mysqli_num_rows($rs) > 0) {
            ?>
            <form name="frm_location" action="lib/location.php" method="post">
                <div class="static" id="order_table">
                <table class="table table-striped table-advance table-hover" id="myDatatable" width="100%">
                    <thead>
                        <tr>
                        <!--<th scope="col">S.No</th>-->
                        <th scope="col">Location Name</th>                                                                
                        <th scope="col">Space Name</th>
                        <th scope="col" style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $sr_no=1;
                        while($data = mysqli_fetch_assoc($rs))
                        {
                        ?>
                        
                        <tr>
                            <!--<td style="text-align:center; font-weight:bold;"><?//=$sr_no++?></td>-->
                            <td><?=dec($data[location_name])?></td>
                            <td><?=dec($data[space_title])?></td>
                            <td style="text-align:center;">
                            <?php
                            if($_SESSION[USER_DETAILS]['user_level_id'] == 1 || $_SESSION[USER_DETAILS]['user_level_id'] == 4)
                            {
                            ?>
                                <a href="list-space.php?location_id=<?php echo $data[location_id] ?>" class="btn btn-primary">View All Spaces</a>
                            <?php
                            }
                            ?>
                            <?php
                            $SQL2="SELECT * FROM `space` JOIN `documents` ON space.space_id=documents.space_id WHERE documents.doc_status='Approved'";
                            $rs2=mysqli_query($con,$SQL2);
                            $data2 = mysqli_fetch_all($rs2);
                            
                            $arr=[];
                         $space_id = $data['space_id'];
                         array_walk_recursive($data2, function($k){global $arr; $arr[]=$k;});
                                            
                         if(in_array($space_id, $arr))
                            {
                            ?>
                            <!--  For space and location table data fetching  -->
                    <?php
                    $SQL1="SELECT * FROM `parking` WHERE parking_space_id = $data[space_id]";
                    $rs1=mysqli_query($con,$SQL1);
                    $data1 = mysqli_fetch_all($rs1);
                    $tot = mysqli_num_rows($rs1)
                    ?>
                            <?php
                                    if($tot == $data[space_total_parkings])
                                    {
                                    ?>
                                    <a class="btn btn-primary" onclick="return booknow()">Book Now</a>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                    <a href="parking.php?space_id=<?=$data[space_id]?>&user=<?=$_SESSION[USER_DETAILS]['user_id']?>&location=<?=$data[location_name]?>" class="btn btn-primary">Book Now</a>
                                    <?php
                                    }
                                    ?>
                            <?php
                    }
                    else
                    {
                        ?>
                        <a class="btn btn-primary" style="pointer-events: none; background: #b9cbd0;">Book Now</a>
                        
                        <?php
                    }
                    ?>
                            </td>    
                        </tr>
                        <?php 
                        }
                        ?>
                    </tbody>
                </table>
                </div>
                <input type="hidden" name="act" />
                <input type="hidden" name="location_id" />
            </form>
            <?php } else {?>
                <div class="alert alert-success" role="alert">Locations not found.</div>
            <?php } ?>
            </fieldset>
    </div>
</section>
<script>
function booknow()
{
    alert("Sorry currently no slots are available in this space please try in other space");
}
</script>
