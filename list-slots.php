<?php 
    include_once("includes/header.php"); 
    include_once("includes/db_connect.php"); 
    $SQL="SELECT * FROM `space` WHERE space_id = $_REQUEST[space_id]";
    $rs=mysqli_query($con,$SQL);
    $data = mysqli_fetch_assoc($rs);
?>
<style>
.parking_space 
{
float:left; 
padding-top:80px;
width:280px; 
font-size:20px;
color:#101746;
font-weight:bold;
padding-left:10px;
}
</style>
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>Room Entry Form</h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section id="maincontent">
   <div class="container">
           <fieldset>
            <legend>All Parking Slots of <?=$data['space_title']?> (Click on slots to assign it to car)</legend>
            <div class="col1">
                <div class="contact">
                    <form action="parking.php" enctype="multipart/form-data" method="post" name="frm_parking">
                        <table>
                            <tr style="vertical-align: middle">
                                <td><div style="float:left; margin-right:10px"><img src="images/available.png" style="height:80px; float:left"> <div style="float:left; font-weight:bold; font-size:14px; margin-top:29px; margin-left:10px">: Available</div></div></td>
                                <td><div style="float:left; margin-right:10px"><img src="images/booked.png" style="height:80px; float:left"> <div style="float:left; font-weight:bold; font-size:14px; margin-top:29px; margin-left:10px">: Occupied</div></div></td>
                                <td width="40">&nbsp;</td>
                                <?php if($_SESSION['user_details']['user_level_id'] != 3) { ?>
                                <td style="font-weight:bold">Search Car Number :</td>
                                <td style="padding-top: 10px;"><input name="parking_car_no" type="text" class="bar" required value="<?=$data[parking_car_no]?>"/></td>
                                <td><input type="submit" value="Search Car" class="btn-primary"></td>
                                <?php } ?>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
            <table align="center">
                            <tr style="vertical-align: middle;">
                                <form action="datesearch.php" enctype="multipart/form-data" method="post" name="frm_parking">
                            <?php if($_SESSION['user_details']['user_level_id'] != 3) { ?>
                                <td style="font-weight:bold">Search By Date :</td>
                                <td style="padding-top: 10px;"><input name="parking_entry_date" type="date" id="date" class="bar" required value="<?=$data[parking_car_no]?>"/></td>
                                <input type="hidden" name="space_id" value="<?=$_GET['space_id'] ?>" >
                                <td><input type="submit" value="Search Slot" class="btn-primary"></td>
                                <?php } ?>
                                </form>
                            </tr>
                        </table>
            <?php
            if($_REQUEST['delmsg']) 
            { 
            ?>
            <div class="alert alert-success" role="alert" style="clear:both"><?=$_REQUEST['delmsg']?></div>
            <?php
            }
            ?>
            <?php
            if($_REQUEST['msg']) 
            { 
            ?>
            <div class="alert alert-success" role="alert" style="clear:both"><?=$_REQUEST['msg']?></div>
            <?php
            }
            if(mysqli_num_rows($rs)) 
            {
            ?>
            <div style="clear:both; "></div>
            <legend>Click on available slots to assign it to car</legend>
            <form name="frm_parking" action="lib/parking.php" method="post">
                <div class="static">
                    <div style="float:left; border:1px solid; margin:5px;">
                        <?php 
                        for($i=1;$i<=$data['space_total_parkings'];$i++) 
                        { 
                        ?>
                            <div style="float:left; border:1px solid; margin:4px;">
                            <?php 
                            $SQL = "SELECT * from parking WHERE parking_space_id = ".$data['space_id']." AND parking_slot_number = $i AND parking_status = 1";
                            $rs = mysqli_query($con,$SQL);
                            if(mysqli_num_rows($rs) ) 
                            { 
                            ?>
                                <div style="float:left; padding:2px;"><img src="images/booked.png" style="height:80px;"></div>
                                <div style="text-align:center; font-weight:bold; font-size:13px;">Slot - <?=$i?></div>
                                <?php
                                $SQLQRY = "SELECT * from parking WHERE parking_space_id = ".$data['space_id']." AND parking_slot_number = $i AND parking_status = 1 AND parking_user_id=".$_SESSION['user_details']['user_id']."";
                                $res = mysqli_query($con,$SQLQRY);
                                $data1=mysqli_fetch_assoc($res);
                                if($data1['parking_user_id'] == $_SESSION['user_details']['user_id'])
                                {
                                ?>
                                <a href="viewslot.php?space_id=<?=$data['space_id']?>&slot_no=<?=$i?>">View</a>|<a href="cancelslot.php?space_id=<?=$data['space_id']?>&slot_no=<?=$i?>&user=<?=$_SESSION['user_details']['user_id']?>" onclick="javascript:cancelslot($(this));return false;">Cancel</a>
                                <?php
                                }
                                if($_SESSION['user_details']['user_level_id'] == 1 || $_SESSION['user_details']['user_level_id'] == 4) 
                                {
                                ?>
                                <!--<p><a href="cancelslot.php?space_id=<?//=$data['space_id']?>&slot_no=<?//=$i?>">Cancel</a></p> -->
                                <a href="viewslot.php?space_id=<?=$data['space_id']?>&slot_no=<?=$i?>">View</a>|<a style="cursor: pointer;" href="cancelslot.php?space_id=<?=$data['space_id']?>&slot_no=<?=$i?>" onclick="javascript:cancelslot($(this));return false;">Cancel</a>
                                <!--<script>
                                function myFunction()
                                {
                                    var slot = '<?//=$i?>';
                                    alert(slot);
                                  var reason = prompt("Please enter reason:", "");
                                  
                                  if (reason == '') 
                                    {
                                        alert("Please enter reason");
                                        return true;
                                    } 
                                    else if(!reason)
                                    {
                                        alert("Please enter reason");
                                        return true;
                                    }
                                  else
                                    {                                            
                                  location.href="cancelslot.php?space_id=<?//=$data['space_id']?>&slot_no=<?//=$i?>&reason=" + reason;
                                    }
                                }
                                </script>-->
                                <?php
                                }
                                ?>
                            <?php
                            } 
                            else 
                            { 
                            ?>
                                <div style="float:left; padding:2px;"> 
                                <?php
                                if($_SESSION['user_details']['user_level_id'] == 1 || $_SESSION['user_details']['user_level_id'] == 4)
                                {
                                ?>
                                    <a href="parking.php?space_id=<?=$data['space_id']?>&slot_no=<?=$i?>">
                                        <img src="images/available.png" style="height:80px;"> 
                                    </a>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <a href="parking.php?space_id=<?=$data['space_id']?>&slot_no=<?=$i?>&user=<?=$_SESSION['user_details']['user_id']?>">
                                        <img src="images/available.png" style="height:80px;"> 
                                    </a>
                                <?php
                                }
                                ?>
                                </div>
                                <div style="text-align:center; font-weight:bold; font-size:13px;">Slot - <?=$i?></div>
                            <?php 
                            } 
                            ?>
                            </div>
                        <?php 
                        }
                        ?>
                    </div>
                </div>
                <input type="hidden" name="act" />
                <input type="hidden" name="parking_id" />
            </form>
            <?php 
            } 
            else 
            {
            ?>
                <div class="alert alert-success" role="alert">You Parking Added Yet.</div>
            <?php 
            } 
            ?>
            </fieldset>
    </div>
</section>
<!--  For customer slot cancellation    -->
<script>
function cancelslot(anchor)
{
   var conf = confirm('Are you sure want to delete this slot?');
   if(conf)
      window.location=anchor.attr("href");
}
</script>
<script>

$(function(){
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    //alert(maxDate);
    $('#date').attr('min', maxDate);
});

</script>
<?php include_once("includes/footer.php"); ?>