<?php 
    include_once("includes/header.php"); 
    if($_REQUEST[user_id])
    {
        $SQL="SELECT * FROM user WHERE user_id = $_REQUEST[user_id]";
        $rs=mysqli_query($con,$SQL);
        $data=mysqli_fetch_assoc($rs);
    }
?> 
<?php
$readonly = "readonly";
define("READONLY", $readonly);
$disabled = "disabled";
define("DISABLED", $disabled);
?>
<script>

jQuery(function() {
    jQuery( "#user_dob" ).datepicker({
      changeMonth: true,
      changeYear: true,
       yearRange: "-65:-10",
       dateFormat: 'd MM,yy'
    });
    jQuery('#frm_user').validate({
        rules: {
            user_confirm_password: {
                equalTo: '#user_password'
            }
        }
    });
});
function validateForm(obj) {
    if(validateEmail(obj.user_email.value))
        return true;
    jQuery('#error-msg').show();
    return false;
}
</script>
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>Operator Registrations Form</h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section id="maincontent">
   <div class="container">
           <fieldset>
            <legend>Operator Registration Form</legend>
                <?php
                if($_REQUEST['msg']) { 
                ?>
                <div class="alert alert-success" role="alert"><?=$_REQUEST['msg']?></div>
                <?php
                }
                ?>
                <?php
                if($_GET['pwd'] == 'pwdnotmatch') { 
                ?>
                <div class="alert alert-success" role="alert">Password's doesn't match</div>
                <?php
                }
                ?>
                <div class="alert alert-success" role="alert" style="display:none" id="error-msg">Enter valid EmailID !!!</div>
                <form action="lib/user.php" enctype="multipart/form-data" method="post" name="frm_user" class="form-horizontal my-forms">
                    
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Select Parking space</label> 
                        <div class="controls">
                        <select name="user_space_id" required  class="bar">
                        <option value="">Select Parking space</option>
                        <?php
                    $user = $_SESSION['user_details']['user_id'];
                    $SQL_QRY="SELECT * FROM space WHERE space_owner_id =".$user;
                    $rs1=mysqli_query($con,$SQL_QRY);
                    while($data1=mysqli_fetch_assoc($rs1))
                    {
                    ?>
                         <?php
                             $SQL="SELECT created_by, user_space_id FROM user";
                             $rs=mysqli_query($con,$SQL);
                             $data=mysqli_fetch_all($rs);
                             $arr=[];
                             array_walk_recursive($data, function($k){global $arr; $arr[]=$k;});
                             if((in_array($_SESSION['user_details']['user_id'], $arr)) && (in_array($data1['space_id'], $arr))   )
                             {
                         ?>
                                <option style="display: none;" value="<?=$data1['space_id'] ?>"><?=$data1['space_title'] ?></option>
                            <?php
                    }
                    else
                    {
                        ?>
                        <option value="<?=$data1['space_id'] ?>"><?=dec($data1['space_title']) ?></option>
                        <?php
                    }
                    }
                    ?>
                    </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Name</label> 
                        <div class="controls"><input name="user_name" type="text" class="bar" oninvalid="setCustomValidity('Please Enter Operator Name With Minimum 4 Charecters!')" oninput="setCustomValidity('')" maxlength="20" minlength="4" pattern="[A-Za-z\s]+" required="" <?php if($data[user_name] != '') { echo READONLY; } ?> value="<?=$data[user_name]?>"/></div>
                    </div>
                    <input type="hidden" name="user_level_id" value="4" >
                    <?php if(!(isset($_REQUEST['user_id'])) || $_REQUEST['user_id'] == "")  { ?>
                        <div class="control-group">
                        <label class="control-label" for="inputEmail">Operator Login ID</label> 
                        <div class="controls"><input name="user_username" type="text" class="bar" oninvalid="setCustomValidity('Please Enter Login ID With Minimum 4 Charecters!')" oninput="setCustomValidity('')" maxlength="20" minlength="4" pattern="[A-Za-z\s]+" required="" <?php if($data[user_username] != '') { echo READONLY; } ?> value="<?=$data[user_username]?>"/></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Password</label> 
                        <div class="controls"><input name="user_password" id="user_password" type="password" class="bar" oninvalid="setCustomValidity('Please Enter Strong Password With, One Uppercae Letter , One Lowercase Letter, One Special Charecter, One Number , And Password Length 8 Charecters ')" oninput="setCustomValidity('')" maxlength="10" minlength="8" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&amp;]).{8,10}$" required="" value="<?=$data[user_password]?>"/></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Confirm Password</label> 
                        <div class="controls"><input name="user_confirm_password" id="user_confirm_password" type="password" class="bar" oninvalid="setCustomValidity('Please Enter Confirm Password ')" oninput="setCustomValidity('')" required value="<?=$data[user_password]?>"/></div>
                    </div>
                    <?php } ?>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Mobile</label> 
                        <div class="controls"><input name="user_mobile" type="number" class="bar" oninvalid="setCustomValidity('Please Enter Valid Mobile Number')" onchange="try{setCustomValidity('')}catch(e){}" title="Please Enter Valid Mobile Number" oninput="if(value.length>10)value=value.slice(0,10)" required="" <?php if($data[user_username] != '') { echo READONLY; } ?> value="<?=$data[user_mobile]?>"/></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Email</label> 
                        <div class="controls"><input name="user_email" id="user_email" type="email" class="bar" oninvalid="setCustomValidity('Please Enter Valid Email ')" oninput="setCustomValidity('')" required <?php if($data[user_email] != '') { echo READONLY; } ?> value="<?=$data[user_email]?>" onchange="validateEmail(this)" /></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Date of Birth</label> 
                        <div class="controls"><input name="user_dob" id="user_dob" type="text" class="bar" required <?php if($data[user_dob] != '') { echo READONLY; } ?> value="<?=$data[user_dob]?>"/></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Address Line 1</label> 
                        <div class="controls"><input name="user_add1" type="text" class="bar" oninvalid="setCustomValidity('Please Enter Address Line1 ')" oninput="setCustomValidity('')" minlength="4" maxlength="200" required <?php if($data[user_add1] != '') { echo READONLY; } ?> value="<?=$data[user_add1]?>"/></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Address Line 2</label> 
                        <div class="controls"><input name="user_add2" type="text" class="bar" oninvalid="setCustomValidity('Please Enter Address Line2 ')" oninput="setCustomValidity('')" minlength="4" maxlength="200" required <?php if($data[user_add2] != '') { echo READONLY; } ?> value="<?=$data[user_add2]?>"/></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">City</label> 
                        <div class="controls">
                            <select name="user_city" required <?php if($data[user_city] != '') { echo DISABLED; } ?> class="bar"/>
                                <?php echo get_new_optionlist("city","city_id","city_name",$data[user_city]); ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">State</label> 
                        <div class="controls">
                            <select name="user_state" required <?php if($data[user_state] != '') { echo DISABLED; } ?> class="bar"/>
                                <?php echo get_new_optionlist("state","state_id","state_name",$data[user_state]); ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Country</label> 
                        <div class="controls">
                            <select name="user_country" required <?php if($data[user_country] != '') { echo DISABLED; } ?> class="bar"/>
                                <?php echo get_new_optionlist("country","country_id","country_name",$data[user_country]); ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Photo</label> 
                        <div class="controls"><input name="user_image" type="file" <?php if($data[user_image] != '') { echo DISABLED; } ?> class="bar"/></div>
                    </div>
                    <div class="clear"></div>
                    <div class="control-group clear">
                                <?php
                                if($_SESSION['user_details']['user_level_id'] == 1 || $_SESSION['user_details']['user_level_id'] == 4) 
                                {
                                ?>
                                <div class="controls"><button type="submit" class="btn btn-primary">Save Details</button>
                                <button type="reset" class="btn btn-danger">Reset Details</button> 
                                </div>
                                <?php
                                }
                                else if($_SESSION['user_details']['user_level_id'] != '')
                                {
                                ?>
                                <div class="controls"><button type="submit" class="btn btn-primary">Save Details</button>
                                <button type="reset" class="btn btn-danger">Reset Details</button> 
                                <!--   For checking user_id with parking table parking_user_id   -->
                                <?php
                                $SQL="SELECT parking_user_id FROM parking";
                                $rs=mysqli_query($con,$SQL);
                                $data=mysqli_fetch_all($rs);
                                $arr=[];
                                array_walk_recursive($data, function($k){global $arr; $arr[]=$k;});
                                if(in_array($_SESSION['user_details']['user_id'], $arr))
                                {
                                ?>
                                <a href="cancelsignin.php" onclick="javascript:confirmationDeleteslot($(this));return false;" class="btn btn-warning">Cancel signin</a>                                 
                                <?php
                                }
                                else
                                {
                                ?>
                                <a href="cancelsignin.php" onclick="javascript:confirmationDelete($(this));return false;" class="btn btn-warning">Cancel signin</a> 
                                <?php
                                }                                
                                ?>
                                </div>
                                <?php
                                }
                                else
                                {
                                ?>
                                <div class="controls"><button type="submit" class="btn btn-primary">Save Details</button>
                                <button type="reset" class="btn btn-danger">Reset Details</button> 
                                </div>
                                <?php
                                }
                                ?>
                    <input type="hidden" name="act" value="save_operator">
                    <input type="hidden" name="avail_image" value="<?=$data[user_image]?>">
                    <input type="hidden" name="user_id" value="<?=$data[user_id]?>">
                    </div>
                </form>
<?php
    if($_SESSION['user_details']['user_level_id'] != 1)
    {
?>
    <script>
        jQuery( "#user_level_id" ).val(3);
        jQuery( "#user_level" ).hide();
    </script>
<?php        
    }
?>
</fieldset>
    
</section>
<!-- For delete customer confirmation -->
<script>
function confirmationDeleteslot(anchor)
{
   var conf = confirm('You are having booked slots: are you sure want to delete your account?');
   if(conf)
      window.location=anchor.attr("href");
}

function confirmationDelete(anchor)
{
   var conf = confirm('Are you sure want to delete your account?');
   if(conf)
      window.location=anchor.attr("href");
}
</script>
<?php include_once("includes/footer.php"); ?> 