<?php 
    include_once("includes/header.php"); 
    include_once("includes/db_connect.php"); 
    $SQL="SELECT * FROM `user`";
    $rs=mysqli_query($con,$SQL);
    
    $query = "SELECT * FROM activitylog ORDER BY id desc";  
    $result = mysqli_query($con, $query);
?>
<style>
body
{
    overflow-x: hidden;
}
@media (max-width:450px){
  html
{
    overflow-x: hidden !important;
}
legend
{
    font-size: 14px !important;
}
.alert
{
    width: 30% !important;
}
#search
{
    width: 100% !important;
}
#avlloc
{
 margin-top: 20px !important;
}
}
@media (max-width:768px){
  html
{
    overflow-x: hidden !important;
}
fieldset
{
    width: 950px !important;
}
}
</style>
<script>
function delete_parking(parking_id)
{
    if(confirm("Do you want to delete the parking?"))
    {
        this.document.frm_parking.parking_id.value=parking_id;
        this.document.frm_parking.act.value="delete_parking";
        this.document.frm_parking.submit();
    }
}
</script>
<script>
jQuery(document).ready(function() {
    jQuery('#mydatatable').DataTable();
});
</script>
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>Book Parking Space</h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section id="maincontent">
  <div class="container" id="search" style="width:100%;">  
    <form>
  <p style="text-align: center; font-size: 16px;" id="avlloc">Please enter the zip code to see the available parking locations</p><br>
    <table>
               <tr>
               <td width="450">&nbsp;</td>           
                     <td><input type="number" name="location_zipcode" id="location_zipcode" placeholder="Enter Zip Code" /></td>
                     <td><input type="button" name="filter" id="filter" value="Search" class="btn btn-info" style="margin-top: -10px;" /></td>
                    <td width="400">&nbsp;</td> 
                <div style="clear:both"></div> 
                </tr>
    </table>                
    </form>                
                <br />  
                <div id="order_table">  
                     <!--<table class="table table-bordered" id="mydatatable">  
                          <tr>  
                               <th width="5%">User</th>  
                               <th width="30%">View Log</th>  
                          </tr>  
                     </table> --> 
                </div>  
           </div> 
</section>
<!-- For preloader gif -->
<div id="load" style="display:none" align="center"><img src="images/preloader.gif"/></div>
 
<!-- Fetching locations from locations table  -->
<script>  
      $(document).ready(function(){  
           
           $('#filter').click(function(){  
               //event.preventDefault();
                var location_zipcode = $('#location_zipcode').val();  
                //alert(from_date);
                if(location_zipcode != '')  
                {  
            $("#load").delay(3000).css("display","block");
                     $.ajax({  
                          url:"searchparking.php",  
                          method:"POST",  
                          data:{location_zipcode:location_zipcode},  
                          success:function(data)  
                          {  
                         $("#load").css("display","none");
                         $('#order_table').html(data);            
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Enter Zipcode");  
                }  
           });  
      });  
 </script>
 <?php include_once("includes/footer.php"); ?>