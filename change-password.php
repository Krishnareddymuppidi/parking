<?php include_once("includes/header.php"); ?> 
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>Change Password </h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section id="maincontent">
   <div class="container">
           <fieldset>
            <legend>Change Password</legend>
                <?php if($_REQUEST['msg']) { ?>
                    <div class="alert alert-success" role="alert"><?=$_REQUEST['msg']?></div>
                <?php } ?>
                <form action="lib/login.php" enctype="multipart/form-data" method="post" name="frm_location" class="form-horizontal">                        
                <div class="control-group">
                        <label class="control-label" for="inputEmail">Password</label> 
                        <div class="controls"><input name="user_new_password" type="password" class="bar"  oninvalid="setCustomValidity('Please Enter Strong Password With, One Uppercae Letter , One Lowercase Letter, One Special Charecter, One Number , And Password Length 8 Charecters ')" oninput="setCustomValidity('')" maxlength="10" minlength="8" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&amp;]).{8,10}$" required="" /></div>
                        </div>
                        <div class="control-group">
                        <label class="control-label" for="inputEmail">Confirm Password</label> 
                        <div class="controls"><input name="user_confirm_password" type="password" class="bar" oninvalid="setCustomValidity('Please Enter Strong Password With, One Uppercae Letter , One Lowercase Letter, One Special Charecter, One Number , And Password Length 8 Charecters ')" oninput="setCustomValidity('')" maxlength="10" minlength="8" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&amp;]).{8,10}$" required="" /></div>
                        </div>
                        <div class="clear"></div>
                        <div class="control-group clear">
                            <div class="controls"><button type="submit" class="btn btn-primary">Update Password</button>
                            <button type="reset" class="btn btn-danger">Reset Details</button> 
                        </div>
                        <input type="hidden" name="act" value="change_password">
                </form>
                </fieldset>
    </div>
</section>
<?php include_once("includes/footer.php"); ?>