<?php 
include_once("includes/header.php"); 
include_once("includes/db_connect.php"); 
global $SERVER_PATH;
?>
<style>
#myBtn
{
 display: none !important;
}
@media (max-width:450px){
  #searchbox {
    padding-left: 0px !important;
  }
  .slider_img
  {
      width:100% !important;
  }
  h2, h3
  {
      font-size: 24px !important;
  }
  h1
  {
      font-size: 20px !important;
      margin-top: 0px !important;
  }
}
</style>

<section id="intro">
    <div class="jumbotron masthead">
      <div class="container">
        <!-- slider navigation -->
        <div class="sequence-nav">
          <div class="prev">
            <span></span>
          </div>
          <div class="next">
            <span></span>
          </div>
        </div>
        <!-- end slider navigation -->
        <div class="row">
          <div class="span12">
            <div id="slider_holder">
              <div id="sequence">
                <ul>
                  <!-- Layer 1 -->
                  <li>
            
                    <div class="info animate-in">
                      <h2>DSS PMS</h2>
                      <br>
                      <h3>Parking Management System</h3>
                      <p>
                        A full solution for providing all facility of Car Parking System
                      </p>
                      <a class="btn btn-primary" href="#">Learn more &raquo;</a>
                    </div>
                    <img class="slider_img animate-in" src="assets/img/slides/sequence/img-3.jpg" alt="" style="height:300px; width:600px; margin-top:45px;">
                  </li>
                  <!-- Layer 2 -->
                  <li>
                    <div class="info">
                      <h2>Parking Management</h2>
                      <br>
                      <h3>Manage Parking Spaces with ease</h3>
                      <p>
                        Serve the Customer for their atmost satisfaction
                      </p>
                      <a class="btn btn-primary" href="#">Learn more &raquo;</a>
                    </div>
                    <img class="slider_img" src="assets/img/slides/sequence/img-3.jpg" alt="" style="height:300px; width:600px; margin-top:45px;">
                  </li>
                  <!-- Layer 3 -->
                  <li>
                    <div class="info">
                      <h2 Style="text-align:center;" >Don't Waste time in searching! </h2>
                      <br>
                      <h3>Reserve your slot with DSS PMS</h3>
                      <p>
                        Project has been developed over the high end latest technology and works on secure environment
                      </p>
                      <a class="btn btn-primary" href="#">Learn more &raquo;</a>
                    </div>
                    <img class="slider_img" src="assets/img/slides/sequence/img-3.jpg" alt="" style="height:300px; width:600px; margin-top:45px;">
                  </li>
                </ul>
              </div>
            </div>
            <!-- Sequence Slider::END-->
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="container" align="center">
   <div class="row">
   <div class="span12">
<?php
$user_id= $_SESSION['user_details']['user_id'];
?>
  <h1 style="margin-top: -30px;">DSS PMS. Choose Parking</h1>

</div></div></div>
  <form action="quicksearch.php" method="post">
  <div class="container" style="background: #004a87; padding: 24px; text-align: center;">
      <div class="row" style="padding-left: 104px;" id="searchbox">
     <div class="span3 form-group" style="margin-right: -40px;" id="loc">
     <b style="color: #fff;">Location</b><br>
                <input type="text" name="space_title" class="form-control" id="name" placeholder="Where would you like to park?" style="width: 220px; height: 32px;" oninvalid="setCustomValidity('Please Enter Location With Minimum 4 Charecters!')" oninput="setCustomValidity('')" maxlength="20" minlength="4" pattern="[A-Za-z\s]+" required="">
     </div>
     <div class="span3 form-group" id="offdate" style="margin-right: -40px;">
     <b style="color: #fff;">Vehicle Drop-Off Date</b><br>
     <input type="date" name="name" class="form-control date" id="name" placeholder="Location" style="width: 130px; height: 32px;" >
     <input type="time" name="time" style="width: 70px; height: 32px;">
     </div>
     <div class="span3 form-group" style="margin-right: -40px;">
     <b style="color: #fff;">Vehicle Pick-up Date</b><br>
     <input type="date" name="name" class="form-control" id="name" placeholder="Location" style="width: 130px; height: 32px;" >
     <input type="time" name="time" style="width: 70px; height: 32px;">
     </div>
     <div class="span2 form-group"><br>
    
     <button type="submit" class="btn btn-primary" style="height: 40px; background: #e74538; font-weight: bold;">Search</button>
         
    </div>
      </div>
  </div>
  </form>
  <section id="maincontent">
    <div class="container">
      <div class="row">
        <div class="span3 features">
          <i class="icon-circled icon-32 icon-suitcase left active" style="background: #444;"></i>
          <h4>Parking Management</h4>
          <div class="dotted_line">
          </div>
          <p class="left">
          A car parking system is a mechanical device that multiplies parking capacity inside a parking lot. Parking systems are generally powered by electric motors or hydraulic pumps that move vehicles into a storage position.
          </p>
          <a href="#">Learn more</a>
        </div>
        <div class="span3 features">
          <i class="icon-circled icon-32 icon-plane left"></i>
          <h4>Slots Management</h4>
          <div class="dotted_line">
          </div>
          <p class="left">
          A car parking system is a mechanical device that multiplies parking capacity inside a parking lot. Parking systems are generally powered by electric motors or hydraulic pumps that move vehicles into a storage position.
          </p>
          <a href="#">Learn more</a>
        </div>
        <div class="span3 features">
          <i class="icon-circled icon-32 icon-leaf left"></i>
          <h4>Goople Maps Integrateds</h4>
          <div class="dotted_line">
          </div>
          <p class="left">
          A car parking system is a mechanical device that multiplies parking capacity inside a parking lot. Parking systems are generally powered by electric motors or hydraulic pumps that move vehicles into a storage position.
          </p>
          <a href="#">Learn more</a>
        </div>
        <div class="span3 features">
          <i class="icon-circled icon-32 icon-wrench left"></i>
          <h4>With latest technology</h4>
          <div class="dotted_line">
          </div>
          <p class="left">
          A car parking system is a mechanical device that multiplies parking capacity inside a parking lot. Parking systems are generally powered by electric motors or hydraulic pumps that move vehicles into a storage position.
          </p>
          <a href="#">Learn more</a>
        </div>
      </div>
      <div class="row">
        <div class="span12">
          <div class="tagline centered">
            <div class="row">
              <div class="span12">
                <div class="tagline_text">
                  <h2>Don't miss this special offer for limited time only! Book Parking Now</h2>
                  <p>
                  A car parking system is a mechanical device that multiplies parking capacity inside a parking lot. Parking systems are generally powered by electric motors or hydraulic pumps that move vehicles into a storage position.
                  </p>
                </div>
                <div class="btn-toolbar cta">
                  <a class="btn btn-large btn-color" href="#">
                            <i class="icon-plane icon-white"></i> Book Your Parking Space </a>
                  <a class="btn btn-large btn-inverse" href="contact.php">
                            <i class="icon-shopping-cart icon-white"></i> Contact Us </a>
                </div>
              </div>
            </div>
          </div>
          <!-- end tagline -->
        </div>
      </div>
      <div class="row">
        <div class="home-posts">
          <div class="span12">
            <h3>Features of the Project</h3>
          </div>
          <div class="span3">
            <div class="post-image">
              <a href="post_right_sidebar.html">
                <img src="assets/img/slides/sequence/img-1.jpg" alt="" style="height:160px; width:270px">
              </a>
            </div>
            <!-- end .entry-meta -->
            <div class="entry-body">
              <a href="post_right_sidebar.html">
                <h5 class="title">Parking Management System</h5>
              </a>
              <p>
              A car parking system is a mechanical device that multiplies parking capacity inside a parking lot. Parking systems are generally powered by electric motors or hydraulic pumps that move vehicles into a storage position.
              </p>
            </div>
            <!-- end .entry-body -->
            <div class="clear">
            </div>
          </div>
          <div class="span3">
            <div class="post-image">
              <a href="#"><img src="assets/img/slides/sequence/img-2.jpg" alt="" style="height:160px; width:270px"></a>
            </div>
            <!-- end .entry-meta -->
            <div class="entry-body">
              <a href="post_right_sidebar.html">
                <h5 class="title">User Management System</h5>
              </a>
              <p>
              A car parking system is a mechanical device that multiplies parking capacity inside a parking lot. Parking systems are generally powered by electric motors or hydraulic pumps that move vehicles into a storage position.
              </p>
            </div>
            <!-- end .entry-body -->
            <div class="clear">
            </div>
          </div>
          <div class="span3">
            <div class="post-image">
              <a href="#"><img src="assets/img/slides/sequence/img-3.jpg" alt="" style="height:160px; width:270px"></a>
            </div>
            <!-- end .entry-meta -->
            <div class="entry-body">
              <a href="post_right_sidebar.html">
                <h5 class="title">Customer Management System</h5>
              </a>
              <p>
              A car parking system is a mechanical device that multiplies parking capacity inside a parking lot. Parking systems are generally powered by electric motors or hydraulic pumps that move vehicles into a storage position.
              </p>
            </div>
            <!-- end .entry-body -->
            <div class="clear">
            </div>
          </div>
         <div class="span3">
            <div class="post-image">
              <a href="#"><img src="assets/img/slides/sequence/img-4.jpg" alt="" style="height:160px; width:270px"></a>
            </div>
            <!-- end .entry-meta -->
            <div class="entry-body">
              <a href="post_right_sidebar.html">
                <h5 class="title">Space Management System</h5>
              </a>
              <p>
              A car parking system is a mechanical device that multiplies parking capacity inside a parking lot. Parking systems are generally powered by electric motors or hydraulic pumps that move vehicles into a storage position.
              </p>
            </div>
            <!-- end .entry-body -->
            <div class="clear">
            </div>
          </div>
              <!-- end flexslider -->
            </div>
            <!-- end .entry-body -->
          </div>
        </div>
      </div>
    </div>
  </section>

<?php include_once("includes/footer.php"); ?> 