<?php 
    include_once("includes/header.php"); 
    include_once("includes/db_connect.php"); 
    $SQL="SELECT * FROM space JOIN location ON space.space_location_id=location.location_id WHERE space.space_owner_id =". $_SESSION['user_details']['user_id'];
    $rs=mysqli_query($con,$SQL);
?>
<style>
@media (max-width:450px){
html, body
{
    overflow-x: hidden !important;
}
.static
{
    overflow-x: scroll !important;
    max-width: 40% !important;
    display: block !important;
    white-space: nowrap !important;
}
#myDatatable
{
    width: 820px;
}
}
</style>
<script>
function delete_location(location_id)
{
    if(confirm("Do you want to delete the parking location?"))
    {
        this.document.frm_location.location_id.value=location_id;
        this.document.frm_location.act.value="delete_location";
        this.document.frm_location.submit();
    }
}
</script>
<script>
jQuery(document).ready(function() {
    jQuery('#mydatatable').DataTable();
});
</script>
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>Parking Spaces</h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section id="maincontent">
   <div class="container">
           <fieldset>
            <legend>Parking Spaces</legend>
            <?php
            if($_REQUEST['msg']) 
            { 
            ?>
                <div class="alert alert-success" role="alert"><?=$_REQUEST['msg']?></div>
            <?php
            }
            if(mysqli_num_rows($rs)) {
            ?>
            <form name="frm_location" action="lib/location.php" method="post">
                <div class="static">
                <table class="table table-striped table-advance table-hover" id="myDatatable" width="100%">
                    <thead>
                        <tr>
                        <th scope="col">Sr. No.</th>
                        <th scope="col">Location Name</th>    
                        <th scope="col">Space Name</th>                        
                        <th scope="col">Total Spaces </th>
                        <th scope="col" style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $sr_no=1;
                        while($data = mysqli_fetch_assoc($rs))
                        {
                            $totalSpace = get_vacant_space($data[location_id]);
                            $occupiedSpace = get_occupied($data[location_id]);
                            $vacanct = $totalSpace  - $occupiedSpace 
                        ?>
                        <tr>
                            <td style="text-align:center; font-weight:bold;"><?=$sr_no++?></td>
                            <td><?=dec($data[location_name])?></td>
                            <td><?=dec($data[space_title])?></td>
                            <td style="text-align: center"><?php echo $data['space_total_parkings'] ?></td>
                            <td style="text-align:center">
                                <a href="priceadd.php?space_id=<?php echo $data[space_id] ?>" class="btn btn-primary">Price Add/Update</a> |
                                <a href="discountadd.php?space_id=<?php echo $data[space_id] ?>" class="btn btn-primary" style="background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#e89a24), to(#b58925));">Discount Add/Update</a>
                            </td>                            
                        </tr>
                        <?php 
                        } 
                        ?>
                    </tbody>
                </table>
                </div>
                <input type="hidden" name="act" />
                <input type="hidden" name="location_id" />
            </form>
            <?php } else {?>
                <div class="alert alert-success" role="alert">No spaces found.</div>
            <?php } ?>
            </fieldset>
    </div>
</section>
<?php include_once("includes/footer.php"); ?>