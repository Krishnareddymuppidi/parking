<?php
/* include_once("includes/db_connect.php");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, PUT, OPTIONS, PATCH, DELETE');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Authorization, Content-Type, x-xsrf-token, x_csrftoken, Cache-Control, X-Requested-With');
header("Content-Type:application/json");
 if (isset($_GET['location_id']) && $_GET['location_id']!="") {
    

	$location_id = $_GET['location_id'];
	$result = mysqli_query(
	$con,
	"SELECT * FROM `location` WHERE location_id=$location_id");
	if(mysqli_num_rows($result)>0){
	$row = mysqli_fetch_array($result);
	$location_name = $row['location_name'];
	$location_contact = $row['location_contact'];
	$location_email = $row['location_email'];
	response($location_id, $location_name, $location_contact,$location_email);
	mysqli_close($con);
	}else{
		response(NULL, NULL, 200,"No Record Found");
		}
}
else
{ */
?>

<?php 
    include_once("includes/header.php");
    include_once("includes/db_connect.php");
    $SQL="SELECT * FROM `location`";
    $rs=mysqli_query($con,$SQL);
    $rs_list=mysqli_query($con,$SQL);
    global $SERVER_PATH;
    
    /* For maps pointout   */
    $SQL="SELECT location_name,location_lat,location_log FROM `location` ORDER BY location_id DESC";
        $rs=mysqli_query($con,$SQL);
        $data=mysqli_fetch_all($rs);

        $arr = json_encode($data);
?>
<script>
$(document).on("click", ".delete-dialog", function () {
     var id = $(this).data('id');
     $("#recordID").val( id );
});
$(document).ready(function() {
    $('#myDatatable').DataTable();
} );
function delete_record(device_id)
{
    this.document.frm_device.act.value="delete_device";
    this.document.frm_device.submit();
}
</script>

<?php 
    include_once("includes/header.php"); 
?> 
<section id="subintro">
   <div class="jumbotron subhead" id="overview">
      <div class="container">
         <div class="row">
            <div class="span12">
               <div class="centered">
                  <h3>About Car Parking System</h3>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section id="maincontent">
   <div class="container">
        <fieldset>
            <legend>All Parking Spaces</legend>
            <!--<div id="mymap" style="width: 100%; height: 500px;"></div>-->
        </fieldset>
<script>
function initMap() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap',
    };
                    
    // Display a map on the web page
    map = new google.maps.Map(document.getElementById("mymap"), mapOptions);
    //map.setTilt(50);
        
    // Multiple markers location, latitude, and longitude
    var markers = <?=$arr?>;
                        
    // Info window content
    var infoWindowContent = [
        ['<div class="info_content">' +
        '<h3>Chennai</h3>' +
        '<p>Chennai is the capital of the state of Tamil Nadu.</p>' + '</div>'],
         ['<div class="info_content">' +
        '<h3>Hyderabad</h3>' +
        '<p>Hyderabad is the capital of the state of Telangana.</p>' + '</div>'],
        ['<div class="info_content">' +
        '<h3>Somajiguda</h3>' +
        '<p>Somajiguda is the city in the Hyderabad.</p>' + '</div>'],
        ['<div class="info_content">' +
        '<h3>Coimbatore</h3>' +
        '<p>Coimbatore is a city in the south Indian state of Tamil Nadu.</p>' +
        '</div>'],
        ['<div class="info_content">' +
        '<h3>Bengaluru</h3>' +
        '<p>Bengaluru is the capital of India southern Karnataka state.</p>' +
        '</div>']
    ];
        
    // Add multiple markers to map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    //alert(infoWindow);
    
    // Place each marker on the map  
    for( i = 0; i < markers.length; i++ ) {
        //alert(markers.length);
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        //alert(position);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        
        // Add info window to marker    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Center the map to fit all markers on the screen
        map.fitBounds(bounds);
    }

    // Set zoom level
var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(4);   // Map zoom
        google.maps.event.removeListener(boundsListener);
    });
    
}
</script>
<script async defer 
 src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWjSQkpYWRMa93lsB6UbQ8jeEWtH7J43s
&callback=initMap">
    </script>
            <!-- page end--><br>
            <fieldset>
            <legend>All Parking Spaces</legend>
                <table class="table table-striped table-advance table-hover" id="myDatatable" width="100%">
                    <thead>
                        <tr>
                        <th scope="col">Sr. No.</th>
                        <th scope="col">Location Name</th>                                                                
                        <!--<th scope="col">Contact </th>
                        <th scope="col">Email</th>-->
                        <th scope="col">Total Space </th>
                        <!--<th scope="col">Vacant Space </th>-->
                        </tr>
                    </thead>
                    <tbody>
                        <!--API call -->
                        
                        
                        
                        <!--API call ends  --> 
                        <?php 
                        $sr_no=1;
                        while($data = mysqli_fetch_assoc($rs_list))
                        {
                            $totalSpace = get_vacant_space($data[location_id]);
                            $occupiedSpace = get_occupied($data[location_id]);
                            $vacanct = $totalSpace  - $occupiedSpace;
                        
                        ?>
                        <?php
                        $format = strtolower($_GET['format']) == 'json' ? 'json' : 'xml'; 
                        if($format == 'json') 
                          {
                            header('Content-type: application/json');
                            echo json_encode($data);
                          }
                        else
                          {                                
                        ?>
                        <tr>
                            <td style="text-align:center; font-weight:bold;"><?=$sr_no++?></td>
                            <td><?=dec($data[location_name])?></td>
                            <!--<td><?//=$data[location_contact]?></td>
                            <td><?//=$data[location_email]?></td>-->
                            <td style="background-color:#004a87; color: #FFF; font-weight: bold; text-align: center"><?=$totalSpace?></td>
                            <!--<td style="background-color:green; color: #FFF; font-weight: bold"><?//=$vacanct?></td>-->
                        </tr>
                        <?php 
                          } 
                        } 
                        ?>
                    </tbody>
                </table>
            </fieldset>
            </div>
    </div>
</section>
<?php include_once("includes/footer.php"); ?>
<?php
/* }
	function response($location_id,$location_name,$location_contact,$location_email){
	$response['location_id'] = $location_id;
	$response['location_name'] = $location_name;
	$response['location_contact'] = $location_contact;
	$response['location_email'] = $location_email;
	
	$json_response = json_encode($response);
	echo $json_response;
}*/
?>