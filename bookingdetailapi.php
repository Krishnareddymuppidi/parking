<?php
include_once("includes/db_connect.php");
header('Access-Control-Allow-Headers: *'); 
header('Access-Control-Allow-Methods: POST, GET, PUT, OPTIONS, PATCH, DELETE');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Authorization, Content-Type, x-xsrf-token, x_csrftoken, Cache-Control, X-Requested-With');
header("Content-Type:application/json");
/* For prices       */
 if (isset($_GET['space_id']) && $_GET['space_id']!="" && $_GET['price']!="") {
    
   
	//include('db.php');
	$space_id = $_GET['space_id'];
	$SQLQRY="SELECT * FROM prices WHERE space_id =".$space_id;
	$result = mysqli_query($con,$SQLQRY);
	if(mysqli_num_rows($result)>0){
	   $rows = array();

//retrieve every record and put it into an array that we can later turn into JSON
$i=0;
while($r = mysqli_fetch_assoc($result)){
	
    $rows[$i][rate_hr] = dec($r['rate_hr']);
    $rows[$i][rate_week] = dec($r['rate_week']);
    $rows[$i][rate_month] = dec($r['rate_month']);
    $rows[$i][rate_day] = dec($r['rate_day']);

$i++;
}

//echo result as json
echo json_encode($rows);


	mysqli_close($con);
	}else{

            $rows = array(
                "message" => "Reates not found"
            ); 
		echo json_encode([$rows]);
		}
}

/* For discounts       */

 if (isset($_GET['space_id']) && $_GET['space_id']!="" && $_GET['discount']!="") {
    
	//include('db.php');
	$space_id = $_GET['space_id'];
	$SQLQRY="SELECT * FROM discounts WHERE space_id =".$space_id;
	$result = mysqli_query($con,$SQLQRY);
	if(mysqli_num_rows($result)>0){
	   $rows = array();

//retrieve every record and put it into an array that we can later turn into JSON
$i=0;
while($r = mysqli_fetch_assoc($result)){
	
    $rows[$i][discount_hr] = dec($r['discount_hr']);
    $rows[$i][discount_week] = dec($r['discount_week']);
    $rows[$i][discount_month] = dec($r['discount_month']);
    $rows[$i][discount_day] = dec($r['discount_day']);

$i++;
}

//echo result as json
echo json_encode($rows);
	mysqli_close($con);
	}else{
            $rows = array(
                "message" => "discounts not found"
            ); 
		echo json_encode([$rows]);
		}
}
/* For spaces       */

if (isset($_GET['space_id']) && $_GET['space_id']!="" && $_GET['space']!="") {
    
	//include('db.php');
	$space_id = $_GET['space_id'];
	$SQLQRY="SELECT * FROM space WHERE space_id =".$space_id;
	$result = mysqli_query($con,$SQLQRY);
	if(mysqli_num_rows($result)>0){
	   $rows = array();

//retrieve every record and put it into an array that we can later turn into JSON
$i=0;
while($r = mysqli_fetch_assoc($result)){
	$image=$r['space_image'];
    $rows[$i][space_amenities] = dec($r['space_amenities']);
    $rows[$i][space_hours] = dec($r['space_hours']);
    $rows[$i][space_things] = dec($r['space_things']);
    $rows[$i][space_image] = 'https://eparkingnext.com/PMS/spaces/'.$image;

$i++;
}

//echo result as json
echo json_encode($rows);
	mysqli_close($con);
	}else{
            $rows = array(
                "message" => "spaces not found"
            ); 
		echo json_encode([$rows]);
		}
}

?>