<?php
include_once("includes/header.php"); 
include_once("includes/db_connect.php");
$user = $_SESSION['user_details']['user_id'];

?>
<style>
.parking_space 
{
float:left; 
padding-top:80px;
width:280px; 
font-size:20px;
color:#101746;
font-weight:bold;
padding-left:10px;
}
</style>
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>Slot Bookings</h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<?php
            if($_REQUEST['delmsg']) 
            { 
            ?>
            <div class="alert alert-success" role="alert" style="clear:both"><?=$_REQUEST['delmsg']?></div>
            <?php
            }
            ?>
            <?php
            if($_REQUEST['msg']) 
            { 
            ?>
            <div class="alert alert-success" role="alert" style="clear:both"><?=$_REQUEST['msg']?></div>
            <?php
            }
            ?>
<section id="maincontent">
   <div class="container">
           <fieldset>
            <legend>Slot Bookings</legend>
            <div class="col1">
                <div class="contact">
                    <form action="parking.php" enctype="multipart/form-data" method="post" name="frm_parking">
                        
            <div style="clear:both; "></div>
            <form name="frm_parking" action="lib/parking.php" method="post">
                <div class="static">
                <?php
                $query = "SELECT * FROM `user` JOIN parking ON user.user_id=parking.parking_user_id WHERE user.user_id = '".$user."'";
                $rs=mysqli_query($con,$query);
                if(mysqli_num_rows($rs)) 
                {
                while($data = mysqli_fetch_assoc($rs))
                {
                    $parking_space_id = $data['parking_space_id'];
                    $parking_slot_number = $data['parking_slot_number'];
                ?>
                    <div style="float:left; border:1px solid; margin:5px;">
                            <div style="float:left; border:1px solid; margin:4px;">
                                <div style="float:left; padding:2px;"><img src="images/booked.png" style="height:60px;"> </div>
                                <div style="text-align:center; font-weight:bold; font-size:13px;">Slot - <?=$parking_slot_number?></div>
                                <a href="viewslot.php?space_id=<?=$parking_space_id?>&slot_no=<?=$parking_slot_number?>">View</a>|
                                <a href="cancelslotuser.php?space_id=<?=$parking_space_id?>&slot_no=<?=$parking_slot_number?>&user=<?=$_SESSION['user_details']['user_id']?>" onclick="javascript:cancelslot($(this));return false;">Cancel</a>
                            </div>
                    </div>
                    <?php
                }
                }
                else
                {
                    echo "Bookings are not available";
                }
                    ?>
                </div>
                <input type="hidden" name="act" />
                <input type="hidden" name="parking_id" />
            </form>
            </fieldset>
    </div>
</section>
<!--  For customer slot cancellation  -->
<script>
function cancelslot(anchor)
{
   var conf = confirm('Are you sure want to delete this slot?');
   if(conf)
      window.location=anchor.attr("href");
}
</script>

<?php include_once("includes/footer.php"); ?>