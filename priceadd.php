<?php 

    include_once("includes/header.php"); 
    if($_REQUEST[space_id])
    {
        $SQL="SELECT * FROM `prices` WHERE space_id = $_REQUEST[space_id]";
        $rs=mysqli_query($con,$SQL);
        $data=mysqli_fetch_assoc($rs);
       
    }
?>
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>Parking Price Entry Form</h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section id="maincontent">
   <div class="container">
           <fieldset>
            <legend>Parking Price Entry Form</legend>
                <?php if($_GET['msg']) { ?>
                    <div class="alert alert-success" role="alert"><?=$_GET['msg']?></div>
                <?php } ?>
                
                <?php if($_GET['status']) { ?>
                    <div class="alert alert-success" role="alert"><?=$_GET['status']?></div>
                <?php } ?>
                
                <form action="lib/priceadd.php" enctype="multipart/form-data" method="post" name="frm_location" class="form-horizontal my-forms">
                    
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Rate/hr</label> 
                        <div class="controls"><input name="rate_hr" type="number" class="bar" oninvalid="setCustomValidity('Please Enter Rate/hr ')" oninput="setCustomValidity('')" onKeyDown="if(this.value.length==4 && event.keyCode!=4) return false;" required value="<?=dec($data[rate_hr])?>" /></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Rate/week</label> 
                        <div class="controls"><input name="rate_week" type="number" class="bar" oninvalid="setCustomValidity('Please Enter Rate/week ')" oninput="setCustomValidity('')" onKeyDown="if(this.value.length==4 && event.keyCode!=4) return false;" required value="<?=dec($data[rate_week])?>"  /></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Rate/month</label> 
                        <div class="controls"><input name="rate_month" type="number" class="bar" oninvalid="setCustomValidity('Please Enter Rate/month ')" oninput="setCustomValidity('')" onKeyDown="if(this.value.length==4 && event.keyCode!=4) return false;" required value="<?=dec($data[rate_month])?>"  /></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Rate/day</label> 
                        <div class="controls"><input name="rate_day" type="number" class="bar" oninvalid="setCustomValidity('Please Enter Rate/day ')" oninput="setCustomValidity('')" onKeyDown="if(this.value.length==4 && event.keyCode!=4) return false;" required value="<?=dec($data[rate_day])?>"  /></div>
                    </div>
                    
                    
                    <div class="control-group clear">
                        <div class="controls"><button type="submit" class="btn btn-primary">Save Details</button>
                        <button type="reset" class="btn btn-danger">Reset Details</button> 
                        <?php
                        if($data[rate_hr] != '' || $data[rate_week] != '' ||$data[rate_month] != '' ||$data[rate_day] != '' ||$data[rate_premium] != '')
                        {
                        ?>
                        <?php
                        if($data[status] == '1')
                        {
                        ?>
                        <a class="discount" href="lib/pricestatus.php?space_id=<?=$_GET[space_id]?>&act=<?=$data[id]?>&st=act">Active</a>
                        <?php
                        }
                        else
                        {
                        ?>
                        <a class="discount" href="lib/pricestatus.php?space_id=<?=$_GET[space_id]?>&inact=<?=$data[id] ?>&st=inact">In Active</a>
                        <?php
                        }
                        }
                        ?>
                    </div>
                    </div>
                    <input type="hidden" name="act" value="save_price">
                    <input type="hidden" name="id" value="<?=$data[id]?>">
                    <input type="hidden" name="space_id" value="<?=$_REQUEST['space_id']?>">
                    <input type="hidden" name="user_id" value="<?=$_SESSION['user_details']['user_id']?>">
                    <input name="updated_rate_hr" type="hidden" value="<?=$data[rate_hr]?>" />
                    <input name="updated_rate_week" type="hidden" value="<?=$data[rate_week]?>" />
                    <input name="updated_rate_month" type="hidden" value="<?=$data[rate_month]?>" />
                    <input name="updated_rate_day" type="hidden" value="<?=$data[rate_day]?>" />
                    <input name="updated_rate_premium" type="hidden" value="<?=$data[rate_premium]?>" />
                </form>
        </fieldset>
    </div>
</section>
<?php include_once("includes/footer.php"); ?> 