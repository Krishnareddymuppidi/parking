<?php
include_once("includes/db_connect.php");
//header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Headers: *'); 
header('Access-Control-Allow-Methods: POST, GET, PUT, OPTIONS, PATCH, DELETE');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Authorization, Content-Type, x-xsrf-token, x_csrftoken, Cache-Control, X-Requested-With');
header("Content-Type:application/json");

$SQL="SELECT * FROM parking ORDER BY parking_id DESC LIMIT 1";
$rs = mysqli_query($con,$SQL);

	if(mysqli_num_rows($rs)>0){
	   $rows = array();

//retrieve every record and put it into an array that we can later turn into JSON
$i=0;
while($r = mysqli_fetch_assoc($rs)){
	$image=dec($r['parking_space_image']);
    $rows[$i][parking_id] = $r['parking_id'];
    $rows[$i][parking_car_no] = $r['parking_car_no'];
    $rows[$i][parking_charges] = dec($r['parking_charges']);
    $rows[$i][parking_date] = $r['parking_date'];
    $rows[$i][parking_intime] = $r['parking_intime'];
    $rows[$i][parking_outtime] = $r['parking_outtime'];
    $rows[$i][parking_entry_date] = $r['parking_entry_date'];
    $rows[$i][parking_exit_date] = $r['parking_exit_date'];
    $rows[$i][parking_qr_image] = 'https://eparkingnext.com/PMS/qr_images/'.$r['parking_qr_image'];
    $rows[$i][parking_space_image] = 'https://eparkingnext.com/PMS/spaces/'.$image;
    $rows[$i][message] = 'success';
$i++;
}

//echo result as json
echo json_encode($rows);


	mysqli_close($con);
	}else{

            $rows = array(
                "message" => "Data not found"
            ); 
		echo json_encode([$rows]);
		}


?>