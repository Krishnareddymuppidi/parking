<?php 
    include_once("includes/header.php"); 
    $status = 1;
    if($_REQUEST[parking_id])
    {
        $SQL="SELECT * FROM `parking` WHERE parking_id = $_REQUEST[parking_id]";
        $rs=mysqli_query($con,$SQL);
        $data=mysqli_fetch_assoc($rs);
        $heading = "Add Car Parking";
    }
    $space_id = $_REQUEST['space_id'];
    $gspace_id = $_GET['space_id'];
    if($space_id)
    {
        $data['parking_space_id'] = $space_id;
        $data['parking_slot_number'] = $_REQUEST['slot_no'];
    }
    
if($_REQUEST['parking_car_no'])
    {
        $SQL="SELECT * FROM `parking` WHERE parking_car_no = '$_REQUEST[parking_car_no]' AND parking_status = 1";
        echo $SQL;
        $rs=mysqli_query($con,$SQL);
        $datacar=mysqli_fetch_assoc($rs);
        $heading = "Release Parking for ".$_REQUEST['parking_car_no'];
        $status = 0;
        if(!mysqli_num_rows($rs))
        {
            header("location:search-parking.php?msg=No Car Found !!!");
            exit;
        }
    }
?>
<?php
    $user = $_GET['user'];
    if($user == '')
    {
        $parking_user_id == '0';
    }
    else
    {
        $parking_user_id = $_GET['user'];
    }
?>
<link rel="stylesheet" href="accordion/css/style.css">
<script>
jQuery(function() {
    jQuery( "#parking_entry_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
       yearRange: "0:+1",
       dateFormat: 'd MM,yy'
    });
    jQuery( "#parking_exit_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
       yearRange: "0:+1",
       dateFormat: 'd MM,yy'
    });
});
</script> 
<style>
.span3 {
    width: 215px;
    margin-top: -4px;
}
@media (max-width:450px){
  html
{
    overflow-x: hidden !important;
}
.accordion
{
    width: 100%;
}
#date, #date2, #date_week, #date_month
{
    margin-top: 0px !important;
}
#date1, #date3
{
    margin-top: -54px !important;
}
#intime, #intime1
{
    margin-left: 9px !important;
}
}
@media (min-width: 768px) and (max-width: 979px)
{
.span4 {
   margin-top: 0px !important;
}
.span2 {
    width: 100% !important;
}
}

</style>
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                <?php
    $request_url = $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $location = substr($request_url, -24);
?>
                
                    <h3><?php echo dec($location); ?></h3>
                    <!--  Slot checking  -->
                    <?php
        $SQL="SELECT * FROM `space` WHERE space_id = '$space_id'";
        $rs=mysqli_query($con,$SQL);
        $data=mysqli_fetch_assoc($rs);
        $space = $data['space_total_parkings'];
        for($i=1;$i<=$space;$i++)
        {
            $com[] = $i;
            $arr1[] = $i. ",";
        }
    
        $array1 = $arr1;
          $array = implode('',$arr);
        $SQL1="SELECT parking_slot_number FROM `parking` WHERE parking_space_id = '$space_id' AND parking_slot_number NOT IN('$array')GROUP BY parking_slot_number";
        $rs1=mysqli_query($con,$SQL1);
        $data1=mysqli_fetch_all($rs1);
        $arr=[];
        // Convert multidimentional array to single dimentional array
        array_walk_recursive($data1, function($k){global $arr; $arr[]=$k;});
    
    
        $result=array_diff($com,$arr);
        $slot_number = array_values($result)[0];
                    ?>
                    <!--slot checking ends   -->
                </div>
            </div>
            </div>
        </div>
    </div>
</section>

<section id="maincontent">
   <div class="container">
   
           <fieldset>
            <legend>Car Parking Entry Details</legend>
                <?php if($_REQUEST['msg']) { ?>
                    <div class="alert alert-success" role="alert"><?=$_REQUEST['msg']?></div>
                <?php } ?>
                <form action="payment.php" enctype="multipart/form-data" method="post" name="frm_parking" class="form-horizontal my-forms">
                    
                    <input name="parking_mode" type="hidden" class="bar" value="<?=$_GET['mode']?>"/>
                    <input name="parking_space_id" type="hidden" class="bar" value="<?=$space_id?>"/>
                        <input name="parking_slot_number" type="hidden" class="bar" value="<?=$slot_number?>"/>
                
                    <div class="control-group">
                        <label class="control-label" for="inputEmail" style="text-align: left;">Car Number:</label> 
                        <div class="controls"><input id="parking_car_no" name="parking_car_no" type="text" class="bar" value="<?=$datacar[parking_car_no]?>"/></div>
                        <p style="text-align: center; float: right;" class="carnocheck"></p>
                    </div>
                    
                    <?php
                    if($data[parking_entry_date] != '')
                    {
                    ?>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Entry Date</label> 
                        <div class="controls"><input name="parking_entry_date" type="text" class="bar" value="<?=$data[parking_entry_date]?>" readonly /></div>
                    </div>
                    <?php
                    }
                    ?>                    
                    <div class="control-group"></div>
                    
                    <div class="clear"></div>
                    <div class="control-group">Please select the price list</div>
                    <div class="clear"></div>
                       <div class="container">
                    <?php
                    /* For prices */
                    $SQLQRY = "SELECT * from prices WHERE space_id = ".$gspace_id."";
                                    $res = mysqli_query($con,$SQLQRY);
                                    $data=mysqli_fetch_assoc($res);
                    /* For discounts */
                    $SQLQRY1 = "SELECT * from discounts WHERE space_id = ".$gspace_id."";
                                    $res1 = mysqli_query($con,$SQLQRY1);
                                    $data1=mysqli_fetch_assoc($res1);
                    ?>
                    <!--  Prices calculations   -->
                    <!-- Per hour rate   -->
                      <?php
                      $hour_rate = dec($data['rate_hr']) - dec($data1['discount_hr']);
                      $week_rate = dec($data['rate_week']) - dec($data1['discount_week']);
                      $month_rate = dec($data['rate_month']) - dec($data1['discount_month']);
                      $day_rate = dec($data['rate_day']) - dec($data1['discount_day']);
                      ?>
        <div class="row">
        <div class="span8">
    <div class="row">
    <div class="span2"><input type="radio" name="price" class="price" value="hour" style="margin-top: -2px;" onclick="perhr(this);" >   Per Hour: RM. <?php echo $hour_rate; ?></div>
    <div id="hour" class="desc">
    <div class="span2">
    <label class="control-label" for="inputEmail">
    <input type="date" id="date" class="date" name="parking_entry_date" style="width: 156px; margin-top: -5px;" ></label><br><br>
    <input type="date" id="date1" class="date1" name="parking_entry_date1" style="width: 156px; margin-top: -5px;" >
    </div>
    <div class="span3">
    <label class="control-label" for="inputEmail">In time:
    <input name="parking_intime_hour"  type="time" id="intime" class="intime" style="width: 78px;" value="<?=$data[parking_intime]?>"/><br>
    Out time:
    <input name="parking_outtime_hour"  type="time" id="outtime" class="outtime" style="width: 78px; margin-top: 5px;" value="<?=$data[parking_intime]?>"/>
    </label> 
    </div>
    <div class="span1">
    <input name="parking_charges_hour" placeholder="Amount to pay" id="tot_amount" style="width: 145px;" type="text" readonly>
    </div>
    </div>
    </div>
    
    <div class="row">
    <div class="span2"><input type="radio" class="price" name="price" value="day" style="margin-top: -2px;" >   Per Day: RM. <?php echo $day_rate; ?></div>
    <div id="day" class="desc">
    <div class="span2">
    <label class="control-label" for="inputEmail">
    <input type="date" id="date2" class="date2" name="parking_entry_date_day" style="width: 156px; margin-top: -5px;" ></label><br><br>
    <input type="date" id="date3" class="date3" name="parking_entry_date2" style="width: 156px; margin-top: -5px;" >
    <p class="date3"></p>
    </div>
    <div class="span3">
    <label class="control-label" for="inputEmail">In time:
    <input name="parking_intime_day"  type="time" id="intime1" class="intime1" style="width: 78px;" value="<?=$data[parking_intime]?>"/><br>
    Out time:
    <input name="parking_outtime_day"  type="time" id="outtime1" class="outtime1" style="width: 78px; margin-top: 5px;" value="<?=$data[parking_intime]?>"/>
    </label> 
    </div>
    <div class="span1">
    <input name="parking_charges_day" placeholder="Amount to pay" id="tot_amount_day" style="width: 145px;" type="text" readonly>
    </div>
    </div>
    </div>
    
    <div class="row">
     <div class="span2"><input type="radio" name="price" class="price" value="week" style="margin-top: -2px;" >   Per Week: RM. <?php echo $week_rate; ?></div>
      <div id="week" class="desc">
      <div class="span4">
        <div class="control-group">
                        <label class="control-label" for="inputEmail">
                            <input type="date" id="date_week" class="date_week" name="parking_entry_date_week" style="width: 156px; margin-top: -5px;">
                        </label>
                        <div class="controls">
                            <input type="date" id="date1_week" class="date1_week" name="parking_exit_date" style="width: 156px;">
                            <p class="date_week"></p>
                        </div>
        </div>
                    <div class="clear"></div>
      </div>
    
      <div class="span2"><input name="parking_charges_week" id="tot_amount_week" style="width: 145px;" type="text" placeholder="Amount to pay" readonly></div>
      </div>
    </div>
    
    <div class="row">
    
     <div class="span2"> <input type="radio" name="price" class="price" value="month" style="margin-top: -2px;" >   Per Month: RM. <?php echo $month_rate; ?></div>
      <div id="month" class="desc">
      <div class="span4">
        <div class="control-group">
                        <label class="control-label" for="inputEmail">
                            <input type="date" id="date_month" name="parking_entry_date_month" style="width: 156px; margin-top: -5px;">
                            </label>
                        <div class="controls">
                            <input type="date" id="date1_month" name="parking_exit_date1" style="width: 156px;">
                            <p class="date1_month"></p>
                        </div>
        </div>
                    <div class="clear"></div>
    </div>
    
      <div class="span2"><input name="parking_charges_month" id="tot_amount_month" style="width: 145px;" type="text" placeholder="Amount to pay" readonly></div>
     </div>
    </div>
    <?php
        $SQLQRY2="SELECT * FROM `space` WHERE space_id = '$space_id'";
        $rs2=mysqli_query($con,$SQLQRY2);
        $data2=mysqli_fetch_assoc($rs2);
        $space_image = $data2['space_image'];

?>
    <input type="hidden" name="parking_space_image" value="<?php echo $space_image ?>" >
    <div class="control-group clear">
                        <div class="controls">
                            <button type="submit" id="submit" class="btn btn-primary">Save Details</button>
                            <button type="reset" class="btn btn-danger">Reset Details</button> 
                        </div>
                    </div>
    </div>
    <div class="span4" style="width: 320px; margin-top: -160px;">
    <br>
    <div class="container">
    <!-- For fetch space details(image,amenities,timings etc..)  -->

<?php
if(!$space_image)
{
?>
<img src="spaces/noimage.jpg" />
<?php    
}
else
{
?>
<img src="spaces/<?php echo $space_image; ?>" width="435" />
<?php
}
?>
<p>&nbsp;</p>
    <div class="accordion">
    <?php
    $no_data = "No data";
    ?>
     <div class="accordion__item">
        <div class="accordion__item__header">
         Amenities
        </div>
    
        <div class="accordion__item__content">
          <p> 
          <?php 
          if(!$data2['space_amenities'])
          {
              echo $no_data;
          }
          else
          {
          echo dec($data2['space_amenities']); 
          }
          ?>
          </p>   </div>
      </div>
    
    
      <div class="accordion__item">
        <div class="accordion__item__header">
         Parking Timings
        </div>
    
        <div class="accordion__item__content">
          <p>
          <?php 
          if(!$data2['space_hours'])
          {
              echo $no_data;
          }
          else
          {
          echo dec($data2['space_hours']); 
          }
          ?>
          </p>
        </div>
      </div>
    
      <div class="accordion__item">
        <div class="accordion__item__header">
         Things you should know
        </div>
    
        <div class="accordion__item__content">
          <p>
          <?php 
          if(!$data2['space_things'])
          {
              echo $no_data;
          }
          else
          {
          echo dec($data2['space_things']); 
          }
          ?>
          </p>
        </div>
      </div>
    
    </div> <!-- id accordion end -->

  </div>
    </div>
  </div>
</div>
                    <?php
                    if(!$space_id && !$_REQUEST['slot_no'])
                    {
                    ?>
                    <legend>Car Parking Exit Details</legend>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Exit Date</label> 
                        <div class="controls"><input name="parking_exit_date" id="parking_exit_date" type="text" class="bar" required value="<?=$data[parking_exit_date]?>" /></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Exit Time</label> 
                        <div class="controls"><input name="parking_outtime" type="time" class="bar" required value="<?=$data[parking_outtime]?>"/></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Parking Charges</label> 
                        <div class="controls"><input name="parking_charges" type="text" class="bar" required value="<?=$data[parking_charges]?>"/></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Description</label> 
                        <div class="controls"><textarea name="parking_description" required cols="" rows="6"><?=$data[parking_description]?></textarea></div>
                    </div>
                    <input type="hidden" name="parking_status_upd" value="0">
                    <?php
                    }
                    ?>
                    
                    <input type="hidden" name="parking_user_id" value="<?=$parking_user_id?>">
                    <input type="hidden" name="act" value="save_parking">
                    <input type="hidden" name="parking_status" value="<?=$status?>">
                    <input type="hidden" name="parking_id" value="<?=$data[parking_id]?>">
                </form>
                </fieldset>
    </div>
</section>

<?php include_once("includes/footer.php"); ?> 

  <script src="accordion/js/accordion.js"></script>
<!--  For disable previous dates    -->
<script>
$(function(){
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    //alert(maxDate);
    $('#date').attr('min', maxDate);
});

$(function(){
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    //alert(maxDate);
    $('#date1').attr('min', maxDate);
});

$(function(){
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    //alert(maxDate);
    $('#date2').attr('min', maxDate);
});

$(function(){
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    //alert(maxDate);
    $('#date3').attr('min', maxDate);
});

$(function(){
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    //alert(maxDate);
    $('#date_month').attr('min', maxDate);
});
$(function(){
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    //alert(maxDate);
    $('#date1_month').attr('min', maxDate);
});

$(function(){
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    //alert(maxDate);
    $('#date_week').attr('min', maxDate);
});

$(function(){
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    //alert(maxDate);
    $('#date1_week').attr('min', maxDate);
});
</script>
<!--  For radio buttons selections   -->
<script type="text/javascript">
$(document).ready(function() {
    $("div.desc").hide();
    $("input[name$='price']").click(function() {
        var test = $(this).val();
        $("div.desc").hide();
        $("#" + test).show();
    });
});

</script>
<script>
$(document).ready(function() {
    $("div.desc").hide();
    $("input[name$='price']").click(function() {
        var test = $(this).val();
        $("div.desc").hide();
        $("#" + test).show();
    });
});

</script>
<!--  For hour price calculation  -->
<script>
            function calculateAmount(val) {
                var tot_price = val * <?=$hour_rate?>;
                /*display the result*/
                var divobj = document.getElementById('tot_amount');
                divobj.value = tot_price;
            }
</script>
<script>
$('.outtime,.intime,.date,.date1').on('change', function(){
    var date = $('#date').val();
    var date1 = $('#date1').val();
    var intime = $('#intime').val();
    var outtime = $('#outtime').val();
    var newintime = intime.split("-").reverse().join("-");
    var newouttime = outtime.split("-").reverse().join("-");
    
    var timein = date.concat(" ", intime);
    var timeout = date1.concat(" ", outtime);
    
    function diff_hours(dt2, dt1) 
 {

  var diff =(dt2.getTime() - dt1.getTime()) / 1000;
  diff /= (60 * 60);
  return Math.abs(Math.round(diff));
  
 }
dt1 = new Date(timein);
dt2 = new Date(timeout);

dt3 = diff_hours(dt1, dt2);
var tot_price = dt3 * <?=$hour_rate?>;
                /*display the result*/
                var divobj = document.getElementById('tot_amount');
                divobj.value = tot_price;
//var startDate = Date.parse($('#date').val());
//var endDate = Date.parse($('#date1').val());
//var timeDiff = endDate - startDate;
 //daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
});

</script>
<!-- For day price -->
<script>
$('.outtime1,.intime1,.date2,.date3').on('blur', function(){
    var date = $('#date2').val();
    var date1 = $('#date3').val();
    var intime = $('#intime1').val();
    var outtime = $('#outtime1').val();
    var newintime = intime.split("-").reverse().join("-");
    var newouttime = outtime.split("-").reverse().join("-");
    
    var timein = date.concat(" ", intime);
    var timeout = date1.concat(" ", outtime);
    
    
     var startDate = Date.parse($('#date2').val());
     var endDate = Date.parse($('#date3').val());
     var timeDiff = endDate - startDate;
     daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
     if(daysDiff ==0)
     {
                  $('.date3').html('Invalid date');
                  $('.date3').css('color','#F00');
                  $('#date3').val(''); 
                  $('#date3').focus(); 
                  $('#tot_amount_day').val(''); 
     }
     else
     {
                var tot_price = daysDiff * <?=$day_rate?>;
                /*display the result*/
                var divobj = document.getElementById('tot_amount_day');
                divobj.value = tot_price; 
                //$('.date3').html('Success! date is valid');
                $('.date3').css('color','#060');
     }
});
</script>

<!-- For week price -->
<script>
 $('.date_week,.date1_week').on('blur', function(){
    var date = $('#date_week').val();
    var date1 = $('#date1_week').val();
    //var timeDiff = date1 - date;
            var startDate = Date.parse($('#date_week').val());
            var endDate = Date.parse($(this).val());
            var timeDiff = endDate - startDate;
            daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
            //alert(daysDiff);
            if(daysDiff!=7)
            {
                //alert("dsdsdsd");
                  $('.date_week').html('Invalid date');
                  $('.date_week').css('color','#F00');
                  $('#date1_week').val(''); 
                  $('#date1_week').focus(); 
                  $('#tot_amount_week').val(''); 
            }
            else
            {
                var tot_price = <?=$week_rate?>;
                /*display the result*/
                var divobj = document.getElementById('tot_amount_week');
                divobj.value = tot_price;
                  $('.date_week').html('Success! date is valid');
                  $('.date_week').css('color','#060');
            }
});

</script>

<!-- For month price -->
<script>
 $('#date1_month').on('blur', function(){
    var date = $('#date_month').val();
    var date1 = $(this).val();
    //var timeDiff = date1 - date;
    var startDate = Date.parse($('#date_month').val());
    var endDate = Date.parse($(this).val());
    var timeDiff = endDate - startDate;
        daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
            //alert(daysDiff);
            if(daysDiff!=30)
            {
                  $('.date1_month').html('Invalid date');
                  $('.date1_month').css('color','#F00');
                  $('#date1_month').val(''); 
                  $('#date1_month').focus(); 
                  $('#tot_amount_month').val(''); 
            }
            else
            {
                var tot_price = <?=$month_rate?>;
                /*display the result*/
                var divobj = document.getElementById('tot_amount_month');
                divobj.value = tot_price;
                  $('.date1_month').html('Success! date is valid');
                  $('.date1_month').css('color','#060');
            }
});
<!-- Per hour radio button selection remaining fields should empty(if selected)  -->
function perhr(radio){
    if(radio.checked)
        {
            $('#date_week').val('');
            $('#date1_week').val('');
            $('#tot_amount_week').val('');
        }
}
</script>

<script>
$(document).ready(function(){
    $("#submit").click(function(){
        var parking_car_no = $('#parking_car_no').val();
        
        if(parking_car_no == '')
        {
        alert("Please enter car number");
        return false;
        }
        if ($('input[name="price"]:checked').length == 0)
        {
            alert("Please select price in the list");
            return false;
        }
        var price = $("input[type='radio'].price:checked").val();
        if(price == 'hour')
        {
            var date = $('#date').val();
            var date1 = $('#date1').val();
            var intime = $('#intime').val();
            var outtime = $('#outtime').val();
            var tot_amount = $('#tot_amount').val();
            
            if(date == '')
            {
            alert("Please select from date");
            return false;
            }
            else if(date1 == '')
            {
            alert("Please select to date");
            return false;
            }
            else if(date>date1)
            {
            alert("From date is less than to date");
            return false;
            }
            else if(intime == '')
            {
             alert("Please select intime");
             return false;
            }
            else if(outtime == '')
            {
             alert("Please select outtime");
             return false;
            }
            else if(tot_amount == '')
            {
            alert("Please select amount");
            return false;            
            }
        }
        if(price == 'day')
        {
            var date2 = $('#date2').val();
            var date3 = $('#date3').val();
            var intime1 = $('#intime1').val();
            var outtime1 = $('#outtime1').val();
            var tot_amount_day = $('#tot_amount_day').val();
            if(date2 == '')
            {
            alert("Please select from date");
            return false;
            }
            else if(date3 == '')
            {
            alert("Please select to date");
            return false;
            }
            else if(date2>date3)
            {
            alert("From date is less than to date");
            return false;
            }
            else if(intime1 == '')
            {
             alert("Please select intime");
             return false;
            }
            else if(outtime1 == '')
            {
             alert("Please select outtime");
             return false;
            }
            else if(tot_amount_day == '')
            {
            alert("Please select amount");
            return false;            
            }
        }
        if(price == 'week')
        {
            var date_week = $('#date_week').val();
            var date1_week = $('#date1_week').val();
            var tot_amount_week = $('#tot_amount_week').val();
            if(date_week == '')
            {
            alert("Please select from date");
            return false;
            }
            else if(date1_week == '')
            {
            alert("Please select to date");
            return false;
            }
            else if(date_week>date1_week)
            {
            alert("From date is less than to date");
            return false;
            }
            else if(tot_amount_week == '')
            {
            alert("Please select amount");
            return false;            
            }
        }
        if(price == 'month')
        {
            var date_month = $('#date_month').val();
            var date1_month = $('#date1_month').val();
            var tot_amount_month = $('#tot_amount_month').val();
            if(date_month == '')
            {
            alert("Please select from date");
            return false;
            }
            else if(date1_month == '')
            {
            alert("Please select to date");
            return false;
            }
            else if(date_month>date1_month)
            {
            alert("From date is less than to date");
            return false;
            }
            else if(tot_amount_month == '')
            {
            alert("Please select amount");
            return false;            
            }
        }
    });
});
</script>
<!--  For email duplication check -->
 <script>
 $('#parking_car_no').on('blur', function(){
    var parking_car_no = $(this).val();
   // alert(parking_car_no);
       // var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
       if(parking_car_no!=''){      
       $.ajax({
                 type:"post",
                 url:"carnocheck.php",
                 data:"parking_car_no="+parking_car_no,
                 success:function(response){
                     var data = $.trim(response);
                     if(data==1){
                        $('.carnocheck').html('This car number has already exists');
                        $('.carnocheck').css('color','#F00');
                        $('#parking_car_no').val(''); 
                        $('#parking_car_no').focus(); 
                     }
                     if(data==0){
                        $('.carnocheck').html('Success! car number Available');
                        $('.carnocheck').css('color','#060');
                     }
                     //$('#SUBCASTE_SEL').html(data);
                 }
        });
 }
});
    </script>