<?php include_once("includes/header.php"); ?> 

<style>
.bar:hover
{
    outline: none;
}
</style>
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>Documents </h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section id="maincontent">
<?php
$SQL="SELECT * FROM `space` WHERE space_owner_id =".$_SESSION['user_details']['user_id'];
        $rs=mysqli_query($con,$SQL);
        $data=mysqli_fetch_assoc($rs);
        $rows = mysqli_num_rows($rs);
        
        $space_id= $data['space_id'];
        $space_location_id= $data['space_location_id'];
?>
   <div class="container">
           <fieldset>
            <legend>Documents</legend>
                <?php if($_REQUEST['msg']) { ?>
                    <div class="alert alert-success" role="alert"><?=$_REQUEST['msg']?></div>
                <?php } ?>
                
                <?php if($_REQUEST['error']) { ?>
                    <div class="alert alert-danger" role="alert"><?=$_REQUEST['error']?></div>
                <?php } ?>
                
                <?php if($_REQUEST['exists']) { ?>
                    <div class="alert alert-danger" role="alert"><?=$_REQUEST['exists']?></div>
                <?php } ?>
                <form action="lib/documents.php" enctype="multipart/form-data" method="post" name="frm_location" class="form-horizontal">                        
                <div class="control-group">
                        <label class="control-label" for="inputEmail">Document 1</label> 
                        <div class="controls"><input name="document1" id="document1" type="file" class="bar" required /></div>
                         <div class="document1" style="margin-left: 83px;"></div>
                        </div>
                        <div class="control-group">
                        <label class="control-label" for="inputEmail">Document 2</label> 
                        <div class="controls"><input name="document2" id="document2" type="file" class="bar" /></div>
                        <div class="document2" style="margin-left: 83px;"></div>
                        </div>
                        <div class="control-group">
                        <label class="control-label" for="inputEmail">Document 3</label> 
                        <div class="controls"><input name="document3" id="document3" type="file" class="bar" /></div>
                        <div class="document3" style="margin-left: 83px;"></div>
                        </div>
                        <div class="clear"></div>
                        <div class="control-group clear">
                            <div class="controls"><button type="submit" name="Upload" class="btn btn-primary">Upload</button>
                            <button type="reset" class="btn btn-danger">Reset</button> 
                        </div>
                        <input type="hidden" name="space_id" value="<?php echo $space_id; ?>" >
                        <input type="hidden" name="space_location_id" value="<?php echo $space_location_id; ?>" >
                        
                        </form>
                </fieldset>
    </div>
</section>

<?php include_once("includes/footer.php"); ?> 
