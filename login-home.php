<?php 
    include_once("includes/header.php"); 
?> 
<style>

/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
#mydatatable th
{
background-color: #464444 !important;
}
#filter
{
    margin-top: -10px;
    background: #004a87;
    border: 2px solid #004a87;
    color: #fff;
    border-radius: 4px;
}
#mobdiv
{
    display: none;
}
.links
{
    color: #fff;
}
@media (max-width:450px){
  html, body
{
    overflow-x: hidden !important;
}
#mydatatable1_length, #datatable1_length
{
    float: left !important;
}
#mydatatable_length
{
    float: left !important;
}
#mydatatable2_length
{
    float: left !important;
}
#mobdiv
{
    display: block !important;
    width: 25em;
    overflow-x: auto;
    white-space: nowrap;
    margin-top: 9px;
}
.login-home
{
    display: none;
}
ul.login-home li {
padding-left: 4px;
}
ul.login-home li a
{
    font-size: 12px;
}

.login-home
{
    margin-left: -23px !important;
}
#maintab
{
overflow-x: scroll !important;
max-width: 40% !important;
display: block !important;
white-space: nowrap !important;
}
#maincontent
{
    width: 825px !important;
}
.docstatus
{
    padding-top: 40px !important;
}
}
</style>
<section id="subintro">
   <div class="jumbotron subhead" id="overview">
      <div class="container">
         <div class="row">
            <div class="span12">
               <div class="centered">
                  <h3>Dashboard for <?php  echo dec($_SESSION['user_details']['user_name']); ?></h3>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<?php
if($_SESSION['user_details']['user_type'] == '2zuzSedsis/30APdeGhL3g==')
{
    ?>
    <script>window.location="admindashboard.php"</script>
<?php
}
?>
<?php
if($_SESSION['user_details']['user_level_id'] == '3')
{
    ?>
    <script>window.location="userdashboard.php"</script>
    <?php
}
?>
<?php
if($_SESSION['user_details']['user_level_id'] == '4')
{
    ?>
    <script>window.location="operatordashboard.php"</script>
    <?php
}
?>
<section id="maincontent">
   <div class="container">
            <?php
            if($_REQUEST['msg']) 
            { 
            ?>
                <div class="msg"><?=$_REQUEST['msg']?></div>
            <?php
            }
            ?>
            <?php
            if($_REQUEST['loc']) 
            { 
            ?>
                <div class="msg"><?=$_REQUEST['loc']?></div>
            <?php
            }
            ?>
            <?php
            if($_REQUEST['space']) 
            { 
            ?>
                <div class="msg"><?=$_REQUEST['space']?></div>
            <?php
            }
            ?>
            <?php
            if($_REQUEST['doc']) 
            { 
            ?>
                <div class="msg"><?=$_REQUEST['doc']?></div>
            <?php
            }
            ?>
            <?php
            $uname = dec($_SESSION['user_details']['user_name']);
            ?>
            <div style="text-align: right; margin-top: -19px; font-size: 18px;">Welcome <?php  echo ucfirst($uname); ?></div>
            <fieldset>

            <legend>Welcome to Car Parking System</legend>
                <table style="width: 100%;" id="maintab">
                    <tr>
                        <td style="width: 17%; position: absolute;">
                            <?php if($_SESSION['user_details']['user_level_id'] == 1) {?>
                                <ul class='login-home'>
                                        <li><a href="location.php">Add Location</a></li>    
                                        <li><a href="space.php">Add Parking Space</a></li>    
                                        <li><a href="location-listing.php">Assign Parking</a></li>
                                        <li><a href="user.php">Add System User</a></li>
                                        <li><a href="search-parking.php">Search Car</a></li>
                                        <li><a href="location-report.php">Location Report</a></li>
                                        <li><a href="space-report.php">Parking Space Report</a></li>
                                        <li><a href="parking-report.php">Parking Report</a></li>    
                                        <li><a href="user-report.php">System User Report</a></li>    
                                        <li><a href="./user.php?user_id=<?php echo $_SESSION['user_details']['user_id']; ?>">My Account</a></li>
                                        <li><a href="change-password.php">Change Password</a></li>
                                        <li><a href="./lib/login.php?act=logout">Logout</a></li>
                                </ul>
                            <?php } ?>
                            <?php if($_SESSION['user_details']['user_level_id'] == 3) {?>
                                <ul class='login-home'>
                                        <li><a href="./index.php">Home</a></li>
                                        <li><a href="./about.php">About Us</a></li>
                                        <li><a href="locations.php">All Parking Locations</a></li>
                                        <li><a href="bookparking.php">Book Parking</a></li>
                                        <li><a href="./user.php?user_id=<?php echo $_SESSION['user_details']['user_id']; ?>">My Account</a></li>
                                        <li><a href="mybookings.php">My Bookings</a></li>
                                        <li><a href="change-password.php">Change Password</a></li>
                                        <li><a href="./lib/login.php?act=logout">Logout</a></li>
                                </ul>
                            <?php } ?>
                            <?php if($_SESSION['user_details']['user_level_id'] == 2) {?>
                            <ul class='login-home'>
                            
                            <?php
                            $SQL="SELECT * FROM documents";
         $rs=mysqli_query($con,$SQL);
         $data=mysqli_fetch_all($rs);
         $owner_id= $_SESSION['user_details']['user_id'];                
         $arr=[];
         array_walk_recursive($data, function($k){global $arr; $arr[]=$k;});
                                
         if(in_array($owner_id, $arr))
            {
                
            }
            else
            {
                            ?>
                            <li><a href="location.php">Add Location</a></li>
                            <li><a href="space.php">Add Parking Space</a></li>
                            <li><a href="documents.php">Upload Documents</a></li>
                            <?php
            }
            ?>
                            </ul>
                            <?php
                            }
                            ?>
                            <?php if($_SESSION['user_details']['user_level_id'] == 2 || $_SESSION['user_details']['user_level_id'] == 4) {?>
                                <ul class='login-home'>
                                        <!--<li><a href="./index.php">Home</a></li>
                                        <li><a href="./about.php">About Us</a></li>-->
                                        <!--<li><a href="locations.php">All Parking Locations</a></li>-->
                                        
                <?php
         $owner_id= $_SESSION['user_details']['user_id'];    
         $SQL2="SELECT * FROM documents WHERE owner_id = $owner_id  AND doc_status='Approved'";
         $rs2=mysqli_query($con,$SQL2);
         $data2=mysqli_fetch_all($rs2);
                     
         $arr=[];
         
         array_walk_recursive($data2, function($k){global $arr; $arr[]=$k;});
            if(in_array($owner_id, $arr))
            {
                ?>
                <li><a href="location.php">Add Location</a></li>
                <li><a href="space.php">Add Parking Space</a></li>
                <li><a href="documents.php">Upload Documents</a></li>
                <li><a href="operator.php">Create Operator</a></li>
                <li><a href="location-listing.php">Parking Spaces</a></li>
                
               

            <?php
            }
            else
            {
            ?>
            <?php
            }
            ?>
                 <li><a href="./user.php?user_id=<?php echo $_SESSION['user_details']['user_id']; ?>">My Account</a></li>
                 <li><a href="change-password.php">Change Password</a></li>
                 <li><a href="./lib/login.php?act=logout">Logout</a></li>
                                </ul>
                                 <div id="mobdiv">
  <button style="background:#385ee7; height: 38px;"><a href="location.php" class="links">Add Location</a></button>    
  <button style="background:#385ee7; height: 38px;"><a href="space.php" class="links">Add Parking Space</a></button>
  <button style="background:#385ee7; height: 38px;"><a href="documents.php" class="links">Upload Documents</a></button>
  <button style="background:#385ee7; height: 38px;"><a href="operator.php" class="links">Create Operator</a></button>
  <button style="background:#385ee7; height: 38px;"><a href="location-listing.php" class="links">Parking Spaces</a></button>    
  <button style="background:#385ee7; height: 38px;"><a href="./user.php?user_id=<?php echo $_SESSION['user_details']['user_id']; ?>" class="links">My Account</a></button>
  <button style="background:#385ee7; height: 38px;"><a href="change-password.php" class="links">Change Password</a></button>
  <button style="background:#385ee7; height: 38px;"><a href="./lib/login.php?act=logout" class="links">Logout</a></button>
</div>
                            <?php } ?>
                        </td>
                        <td style="width: 80%; vertical-align: top">
                            <?php
                            $user_id = $_SESSION['user_details']['user_id'];
                            $SQL1="SELECT * FROM documents WHERE owner_id = $user_id";
                            $rs1=mysqli_query($con,$SQL1);
                            if(mysqli_num_rows($rs1)) 
                                {
                                    while($datares1=mysqli_fetch_assoc($rs1))
                                    {
                                        ?>
                                        <h3 class="docstatus"><?php echo "Your document status is ".$datares1['doc_status']; ?></h3>
                                        <?php
                                    }
                                }
                                else
                                {
                                }
                                                    ?>
                                                    
<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'Mybookings')" id="defaultOpen">Walk-in Report</button>
  <button class="tablinks" onclick="openCity(event, 'Revenue')">Revenue Report</button>
  <button class="tablinks" onclick="openCity(event, 'Slotfilling')">Slot Filling Report</button>
  <button class="tablinks" onclick="openCity(event, 'Cancel')">Cancellation Report</button>
  <button class="tablinks" onclick="openCity(event, 'Upcoming')">Future Reservation Report</button>
</div>

<form>
<div id="Mybookings" class="tabcontent">
 Select Space: &nbsp;<select name="space_id3" id="space_id3" class="space_id3">
<option value="">Select Space</option>
<?php 
         $user_id = $_SESSION['user_details']['user_id'];  
         $SQL_QRY="SELECT * FROM space WHERE space_owner_id =" .$user_id;
         $rs3=mysqli_query($con,$SQL_QRY);
         while($data3=mysqli_fetch_assoc($rs3))
         {
 ?>
<option value="<?php echo $data3['space_id'];  ?>"><?php echo dec($data3['space_title']);  ?></option>
<?php
         }
?>
</select><br>
 
 <div>
From Date: <b style="visibility: hidden;">te</b><input type="date" name="walk_from_date" id="walk_from_date" class="walk_from_date" >
To Date: &nbsp;<input type="date" name="walk_to_date" id="walk_to_date" class="walk_to_date" >
</div>
<div id="load3" style="display:none" align="center"><img src="images/preloader.gif"/></div>
<div id="order_table3">  
                   
</div> 

</div>
</form>

<form>
<div id="Revenue" class="tabcontent">
Select Space: &nbsp;<select name="space_id" id="space_id">
<option value="">Select Space</option>
<?php 
         $user_id = $_SESSION['user_details']['user_id'];  
         $SQL_QRY="SELECT * FROM space WHERE space_owner_id =" .$user_id;
         $rs3=mysqli_query($con,$SQL_QRY);
         while($data3=mysqli_fetch_assoc($rs3))
         {
 ?>
<option value="<?php echo $data3['space_id'];  ?>"><?php echo dec($data3['space_title']);  ?></option>
<?php
         }
?>
</select><br>
Select Period:  &nbsp;<select id="timeperiod">
<option value="">Select Time period</option>
<option value="Hourly">Hourly</option>
<option value="Daily">Daily</option>
<option value="Weekly">Weekly</option>
<option value="Monthly">Monthly</option>
</select>
<div id="dailydate" style="display: none;">
Select Date: &nbsp;&nbsp;&nbsp;<input type="date" name="daily_date" id="daily_date" class="daily_date" >
</div>

<div id="weeklydate" style="display: none;">
From Date: <b style="visibility: hidden;">te</b><input type="date" name="week_from_date" id="week_from_date" class="week_from_date">
To Date: &nbsp;<input type="date" name="week_to_date" id="week_to_date" class="week_to_date" >
</div>

<div id="monthlydate" style="display: none;">
From Date: <b style="visibility: hidden;">te</b><input type="date" name="month_from_date" id="month_from_date" class="month_from_date" >
To Date: &nbsp;<input type="date" name="month_to_date" id="month_to_date" class="month_to_date" >
</div>

<div id="yearlydate" style="display: none;">
From Date: <b style="visibility: hidden;">te</b><input type="date" name="year_from_date" id="year_from_date" class="year_from_date" >
To Date: &nbsp;<input type="date" name="year_to_date" id="year_to_date" class="year_to_date" >
</div>

<br><br>
<div id="load" style="display:none" align="center"><img src="images/preloader.gif"/></div>
<div id="order_table">  
                   
</div> 

</div>
</form>


<form>
<div id="Slotfilling" class="tabcontent">

Select Space: &nbsp;<select name="space_id1" id="space_id1">
<option>Select Space</option>
<?php 
         $user_id = $_SESSION['user_details']['user_id'];  
         $SQL_QRY="SELECT * FROM space WHERE space_owner_id =" .$user_id;
         $rs3=mysqli_query($con,$SQL_QRY);
         while($data3=mysqli_fetch_assoc($rs3))
         {
 ?>
<option value="<?php echo $data3['space_id'];  ?>"><?php echo dec($data3['space_title']);  ?></option>
<?php
         }
?>
</select><br>
Select Period:  &nbsp;<select id="timeperiod1">
<option>Select Time period</option>
<option value="Hourly1">Hourly</option>
<option value="Daily1">Daily</option>
<option value="Weekly1">Weekly</option>
<option value="Monthly1">Monthly</option>
</select>
<div id="dailydate1" style="display: none;">
Select Date: &nbsp;&nbsp;&nbsp;<input type="date" name="daily_date1" id="daily_date1" class="daily_date1" >
</div>

<div id="weeklydate1" style="display: none;">
From Date: <b style="visibility: hidden;">te</b><input type="date" name="week_from_date1" id="week_from_date1" class="week_from_date1" >
To Date: &nbsp;<input type="date" name="week_to_date1" id="week_to_date1" class="week_to_date1" >
</div>

<div id="monthlydate1" style="display: none;">
From Date: <b style="visibility: hidden;">te</b><input type="date" name="month_from_date1" id="month_from_date1" class="month_from_date1" >
To Date: &nbsp;<input type="date" name="month_to_date1" id="month_to_date1" class="month_to_date1" >
</div>

<div id="yearlydate1" style="display: none;">
From Date: <b style="visibility: hidden;">te</b><input type="date" name="year_from_date1" id="year_from_date1" class="year_from_date1" >
To Date: &nbsp;<input type="date" name="year_to_date1" id="year_to_date1" class="year_to_date1" >
</div>

<br><br>
<div id="load1" style="display:none" align="center"><img src="images/preloader.gif"/></div>
<div id="order_table1">  
                   
</div>

</div>
</form>

<!-- For cancellation bookings  -->
<form>
<div id="Cancel" class="tabcontent">
 Select Space: &nbsp;<select name="space_id4" id="space_id4" class="space_id4">
<option value="">Select Space</option>
<?php 
         $user_id = $_SESSION['user_details']['user_id'];  
         $SQL_QRY="SELECT * FROM space WHERE space_owner_id =" .$user_id;
         $rs3=mysqli_query($con,$SQL_QRY);
         while($data3=mysqli_fetch_assoc($rs3))
         {
 ?>
<option value="<?php echo $data3['space_id'];  ?>"><?php echo dec($data3['space_title']);  ?></option>
<?php
         }
?>
</select><br>
 
 <div>
From Date: <b style="visibility: hidden;">te</b><input type="date" name="cancel_from_date" id="cancel_from_date" class="cancel_from_date" >
To Date: &nbsp;<input type="date" name="cancel_to_date" id="cancel_to_date" class="cancel_to_date" >
</div>
<div id="load4" style="display:none" align="center"><img src="images/preloader.gif"/></div>
<div id="order_table4">  
                   
</div> 

</div>
</form>

<form>
<div id="Upcoming" class="tabcontent">
 Select Space: &nbsp;<select name="space_id5" id="space_id5" class="space_id5">
<option>Select Space</option>
<?php 
         $user_id = $_SESSION['user_details']['user_id'];  
         $SQL_QRY="SELECT * FROM space WHERE space_owner_id =" .$user_id;
         $rs3=mysqli_query($con,$SQL_QRY);
         while($data3=mysqli_fetch_assoc($rs3))
         {
 ?>
<option value="<?php echo $data3['space_id'];  ?>"><?php echo dec($data3['space_title']);  ?></option>
<?php
         }
?>
</select>
<div id="load5" style="display:none" align="center"><img src="images/preloader.gif"/></div>
<div id="order_table5">  
                   
</div> 


</div>
</form>

<div class="modal fade" id="viewlog" role="dialog">
    <div class="modal-dialog">
 
     <!-- Modal content-->
     <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Booking Info</h4>
        <button type="button" class="close" data-dismiss="modal" style="margin-top: -46px;">&times;</button>
      </div>
      <div class="modal-body">
 
      </div>
     <!-- <div class="modal-footer">
       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>-->
     </div>
    </div>
   </div>
                            </td>
                        </tr>
                </table>
            </fieldset>
   </div>
</section>
<!--  For Daily revenue report checking -->
<script>  
      $(document).ready(function(){  
          $('.daily_date').on('change', function(){
               //event.preventDefault();
                var daily_date = $('#daily_date').val(); 
                var space_id = $('#space_id').val(); 
				var timeperiod = $('#timeperiod').val();
				//alert(timeperiod);				
			   
                if(dailydate != '')  
                {  
            $("#load").delay(3000).css("display","block");
                     $.ajax({  
                          url:"ownerrevenuereport.php",  
                          method:"POST",  
                          data:{daily_date:daily_date,space_id:space_id,timeperiod:timeperiod},  
                          success:function(data)  
                          {  
                         $("#load").css("display","none");
                               $('#order_table').html(data);            
                          }  
                     });  
                }                  
           });  
      });  
 </script>
 
 <!--  For Weekly revenue report checking -->
 <script>  
      $(document).ready(function(){  
          $('.week_to_date,.week_from_date').on('change', function(){
               //event.preventDefault();
                var week_from_date = $('#week_from_date').val();
                var week_to_date = $('#week_to_date').val();
                var space_id = $('#space_id').val();
                var timeperiod = $('#timeperiod').val(); 				
                //alert(daily_date);
                if(week_from_date != '')  
                {  
            $("#load").delay(3000).css("display","block");
                     $.ajax({  
                          url:"ownerrevenuereportweek.php",  
                          method:"POST",  
                          data:{week_from_date:week_from_date,week_to_date:week_to_date,space_id:space_id,timeperiod:timeperiod},  
                          success:function(data)  
                          {  
                         $("#load").css("display","none");
                               $('#order_table').html(data);            
                          }  
                     });  
                }                  
           });  
      });  
 </script>
 
 <!--  For Monthly revenue report checking -->
 <script>  
      $(document).ready(function(){  
          $('.month_to_date,.month_from_date').on('change', function(){
               //event.preventDefault();
                var month_from_date = $('#month_from_date').val();
                var month_to_date = $('#month_to_date').val();
                var space_id = $('#space_id').val(); 
				var timeperiod = $('#timeperiod').val();                 
                //alert(daily_date);
                if(month_from_date != '')  
                {  
            $("#load").delay(3000).css("display","block");
                     $.ajax({  
                          url:"ownerrevenuereportmonth.php",  
                          method:"POST",  
                          data:{month_from_date:month_from_date,month_to_date:month_to_date,space_id:space_id,timeperiod:timeperiod},  
                          success:function(data)  
                          {  
                         $("#load").css("display","none");
                               $('#order_table').html(data);            
                          }  
                     });  
                }                  
           });  
      });  
 </script>
 
  <!--  For Hourly revenue report checking -->
 <script>  
      $(document).ready(function(){  
          $('.year_to_date,.year_from_date').on('change', function(){
               //event.preventDefault();
                var year_from_date = $('#year_from_date').val();
                var year_to_date = $('#year_to_date').val();
                var space_id = $('#space_id').val();  
				var timeperiod = $('#timeperiod').val(); 	
                //alert(timeperiod);				
                //alert(daily_date);
                if(year_from_date != '')  
                {  
            $("#load").delay(3000).css("display","block");
                     $.ajax({  
                          url:"ownerrevenuereportyear.php",  
                          method:"POST",  
                          data:{year_from_date:year_from_date,year_to_date:year_to_date,space_id:space_id,timeperiod:timeperiod},  
                          success:function(data)  
                          {  
                         $("#load").css("display","none");
                               $('#order_table').html(data);            
                          }  
                     });  
                }                  
           });  
      });  
 </script>
 
 <!-- Revenue report ends  -->
 
 <!--  For Daily Slot filling report checking -->
<script>  
      $(document).ready(function(){  
          $('.daily_date1').on('change', function(){
               //event.preventDefault();
                var daily_date = $('#daily_date1').val(); 
                var space_id = $('#space_id1').val();  
				var timeperiod = $('#timeperiod1').val();				
                //alert(timeperiod);
                if(dailydate != '')  
                {  
            $("#load1").delay(3000).css("display","block");
                     $.ajax({  
                          url:"ownerslotfillingreport.php",  
                          method:"POST",  
                          data:{daily_date:daily_date,space_id:space_id,timeperiod:timeperiod},  
                          success:function(data)  
                          {  
                         $("#load1").css("display","none");
                               $('#order_table1').html(data);            
                          }  
                     });  
                }                  
           });  
      });  
 </script>
 
 <!--  For Weekly Slot filling report checking -->
 <script>  
      $(document).ready(function(){  
          $('.week_to_date1,.week_from_date1').on('change', function(){
               //event.preventDefault();
                var week_from_date = $('#week_from_date1').val();
                var week_to_date = $('#week_to_date1').val();
                var space_id = $('#space_id1').val(); 
                var timeperiod = $('#timeperiod1').val();                
                //alert(daily_date);
                if(week_from_date != '')  
                {  
            $("#load1").delay(3000).css("display","block");
                     $.ajax({  
                          url:"ownerslotfillreportweek.php",  
                          method:"POST",  
                          data:{week_from_date:week_from_date,week_to_date:week_to_date,space_id:space_id,timeperiod:timeperiod},  
                          success:function(data)  
                          {  
                         $("#load1").css("display","none");
                               $('#order_table1').html(data);            
                          }  
                     });  
                }                  
           });  
      });  
 </script>
 
 <!--  For Monthly Slot filling report checking -->
 <script>  
      $(document).ready(function(){  
          $('.month_to_date1,.month_from_date1').on('change', function(){
               //event.preventDefault();
                var month_from_date = $('#month_from_date1').val();
                var month_to_date = $('#month_to_date1').val();
                var space_id = $('#space_id1').val();  
                var timeperiod = $('#timeperiod1').val();				
                //alert(daily_date);
                if(month_from_date != '')  
                {  
            $("#load1").delay(3000).css("display","block");
                     $.ajax({  
                          url:"ownerslotfillingreportmonth.php",  
                          method:"POST",  
                          data:{month_from_date:month_from_date,month_to_date:month_to_date,space_id:space_id,timeperiod:timeperiod},  
                          success:function(data)  
                          {  
                         $("#load1").css("display","none");
                               $('#order_table1').html(data);            
                          }  
                     });  
                }                  
           });  
      });  
 </script>
 
  <!--  For Hourly Slot filling report checking -->
 <script>  
      $(document).ready(function(){  
          $('.year_to_date1,.year_from_date1').on('change', function(){
               //event.preventDefault();
                var year_from_date = $('#year_from_date1').val();
                var year_to_date = $('#year_to_date1').val();
                var space_id = $('#space_id1').val(); 
                var timeperiod = $('#timeperiod1').val();                
                //alert(daily_date);
                if(year_from_date != '')  
                {  
            $("#load1").delay(3000).css("display","block");
                     $.ajax({  
                          url:"ownerslotfillingreportyear.php",  
                          method:"POST",  
                          data:{year_from_date:year_from_date,year_to_date:year_to_date,space_id:space_id,timeperiod:timeperiod},  
                          success:function(data)  
                          {  
                         $("#load1").css("display","none");
                               $('#order_table1').html(data);            
                          }  
                     });  
                }                  
           });  
      });  
 </script>
 <!-- Slot filling report ends -->
 
 <!-- For walkins report  -->
<script>  
      $(document).ready(function(){  
          $('.walk_to_date,.walk_from_date,.space_id3').on('change', function(){
               //event.preventDefault();
                var walk_from_date = $('#walk_from_date').val();
                var walk_to_date = $('#walk_to_date').val();
                var space_id = $('#space_id3').val();                 
                //alert(daily_date);
                if(space_id == '')
                {
                    alert("Please select space");
                }
                else if(walk_from_date == '')
                {
                    alert("Please select from date");
                }
                else if(walk_to_date == '')
                {    
                    alert("Please select to date");
                }
                else if(walk_from_date>walk_to_date)
                {
                    alert("From date should be less than To date");
                }
                else
                {
                if(walk_from_date != '')  
                {  
            $("#load3").delay(3000).css("display","block");
                     $.ajax({  
                          url:"walkinreport.php",  
                          method:"POST",  
                          data:{walk_from_date:walk_from_date,walk_to_date:walk_to_date,space_id:space_id},  
                          success:function(data)  
                          {  
                         $("#load3").css("display","none");
                               $('#order_table3').html(data);            
                          }  
                     });  
                }  
                }                
           });  
      });  
 </script>
 <!-- Walk-in report ends  -->
 
 <!-- For Cancel report  -->
<script>  
      $(document).ready(function(){  
          $('.space_id4,.cancel_to_date,.cancel_from_date').on('change', function(){
               //event.preventDefault();
                var cancel_from_date = $('#cancel_from_date').val();
                var cancel_to_date = $('#cancel_to_date').val();
                var space_id = $('#space_id4').val();                 
                //alert(daily_date);
                if(space_id == '')
                {
                    alert("Please select space");
                    return false;
                }
                if(cancel_from_date == '')
                {
                    alert("Please select from date");
                }
                else if(cancel_to_date == '')
                {
                    alert("Please select to date");
                }
                else if(cancel_from_date>cancel_to_date)
                {
                    alert("From date should be less than To date");
                }
                else
                {
                if(cancel_from_date != '')  
                {  
            $("#load4").delay(3000).css("display","block");
                     $.ajax({  
                          url:"cancellationreport.php",  
                          method:"POST",  
                          data:{cancel_from_date:cancel_from_date,cancel_to_date:cancel_to_date,space_id:space_id},  
                          success:function(data)  
                          {  
                         $("#load4").css("display","none");
                               $('#order_table4').html(data);            
                          }  
                     });  
                } 
                }                
           });  
      });  
 </script>
 <!-- Cancel report ends  -->
 
  <!-- For Future reservation report  -->
<script>  
      $(document).ready(function(){  
          $('.space_id5').on('change', function(){
               //event.preventDefault();

                var space_id = $('#space_id5').val();                 
                //alert(daily_date);
                if(space_id5 != '')  
                {  
            $("#load5").delay(3000).css("display","block");
                     $.ajax({  
                          url:"futurereservationreport.php",  
                          method:"POST",  
                          data:{space_id:space_id},  
                          success:function(data)  
                          {  
                         $("#load5").css("display","none");
                               $('#order_table5').html(data);            
                          }  
                     });  
                }                  
           });  
      });  
 </script>
 <!-- Future reservation report ends  -->
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
<script>
function operator()
{
    alert("You don't have access to create an operator");
}
function loc()
{
    alert("Already you added a location");
}
function space()
{
    alert("Already you added a space");
}
</script>
<!-- Daily, Weekly, Monthly, Hourly Revenue Report dropdown on change -->
<script type="text/javascript">
    $(function () {
        $("#timeperiod").change(function () {
            if ($(this).val() == "Daily") {
                $("#dailydate").show();
            } else {
                $("#dailydate").hide();
            }
        });
        
         $("#timeperiod").change(function () {
            if ($(this).val() == "Weekly") {
                $("#weeklydate").show();
            } else {
                $("#weeklydate").hide();
            }
        });
        
        $("#timeperiod").change(function () {
            if ($(this).val() == "Monthly") {
                $("#monthlydate").show();
            } else {
                $("#monthlydate").hide();
            }
        });
        
        $("#timeperiod").change(function () {
            if ($(this).val() == "Hourly") {
                $("#yearlydate").show();
            } else {
                $("#yearlydate").hide();
            }
        });
    });
</script>

<!-- Daily, Weekly, Monthly, Hourly Slot filling Report dropdown on change -->
<script type="text/javascript">
    $(function () {
        $("#timeperiod1").change(function () {
            if ($(this).val() == "Daily1") {
                $("#dailydate1").show();
            } else {
                $("#dailydate1").hide();
            }
        });
        
         $("#timeperiod1").change(function () {
            if ($(this).val() == "Weekly1") {
                $("#weeklydate1").show();
            } else {
                $("#weeklydate1").hide();
            }
        });
        
        $("#timeperiod1").change(function () {
            if ($(this).val() == "Monthly1") {
                $("#monthlydate1").show();
            } else {
                $("#monthlydate1").hide();
            }
        });
        
        $("#timeperiod1").change(function () {
            if ($(this).val() == "Hourly1") {
                $("#yearlydate1").show();
            } else {
                $("#yearlydate1").hide();
            }
        });
    });
</script>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<?php include_once("includes/footer.php"); ?>