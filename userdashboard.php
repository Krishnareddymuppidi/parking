<?php 
include_once("includes/header.php"); 
$user_details = 'user_details';
define("USER_DETAILS", $user_details);

$user_level_id = 'user_level_id';
define("USER_LEVEL_ID", $user_level_id);  

$user_id = 'user_id';
define("USER_ID", $user_id);  

$space_title = 'space_title';
define("SPACE_TITLE", $space_title);

$parking_slot_number = 'parking_slot_number';
define("PARKING_SLOT_NUMBER", $parking_slot_number);

$parking_id = 'parking_id';
define("PARKING_ID", $parking_id);
?> 
<style>

/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
#mydatatable th
{
background-color: #464444 !important;    
}
ul.login-home li {
    background: #385ee7 !important;
}
#mobdiv
{
    display: none;
}
.links
{
    color: #fff;
}
@media (max-width:450px){
  html, body
{
    overflow-x: hidden !important;
}
#mydatatable1_length
{
    float: left !important;
}
#mydatatable_length
{
    float: left !important;
}
#mydatatable2_length
{
    float: left !important;
}
#mobdiv
{
    display: block;
}
.login-home
{
    display: none;
}
ul.login-home li {
padding-left: 4px;
}
ul.login-home li a
{
    font-size: 12px;
}

.login-home
{
    margin-left: -23px !important;
}
#maintab
{
overflow-x: scroll !important;
max-width: 40% !important;
display: block !important;
white-space: nowrap !important;
}
#maincontent
{
    width: 825px !important;
}
}
</style>
<script>
jQuery(document).ready(function() {
    jQuery('#mydatatable').DataTable();
});
jQuery(document).ready(function() {
    jQuery('#mydatatable1').DataTable();
});
jQuery(document).ready(function() {
    jQuery('#mydatatable2').DataTable();
});
</script>
<section id="subintro">
   <div class="jumbotron subhead" id="overview">
      <div class="container">
         <div class="row">
            <div class="span12">
               <div class="centered">
                  <h3 class="titledb">Dashboard for Car Parking System</h3>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<section id="maincontent">
   <div class="container">
            <?php
            if($_REQUEST['msg']) 
            { 
            ?>
                <div class="msg"><?=$_REQUEST['msg']?></div>
            <?php
            }
            ?>
            
            <?php
            if($_REQUEST['delmsg']) 
            { 
            ?>
                <div class="msg"><?=$_REQUEST['delmsg']?></div>
            <?php
            }
            ?>
            <?php
            $uname = dec($_SESSION[USER_DETAILS]['user_name']);
            ?>
            <div style="text-align: right; margin-top: -19px; font-size: 18px;">Welcome <?php  echo ucfirst($uname); ?></div>
            <fieldset>

            <legend>Welcome to Car Parking System</legend>
                <table style="width: 100%" id="maintab">
                    <tr>
                        <td style="width: 17%; position: absolute;" id="leftmen">
                            <?php if($_SESSION[USER_DETAILS][USER_LEVEL_ID] == 1) {?>
                                <ul class='login-home'>
                                        <li><a href="location.php">Add Location</a></li>    
                                        <li><a href="space.php">Add Parking Space</a></li>    
                                        <li><a href="location-listing.php">Assign Parking</a></li>
                                        <li><a href="user.php">Add System User</a></li>
                                        <li><a href="search-parking.php">Search Car</a></li>
                                        <li><a href="location-report.php">Location Report</a></li>
                                        <li><a href="space-report.php">Parking Space Report</a></li>
                                        <li><a href="parking-report.php">Parking Report</a></li>    
                                        <li><a href="user-report.php">System User Report</a></li>    
                                        <li><a href="./user.php?user_id=<?php echo $_SESSION[USER_DETAILS][USER_ID]; ?>">My Account</a></li>
                                        <li><a href="change-password.php">Change Password</a></li>
                                        <li><a href="./lib/login.php?act=logout">Logout</a></li>
                                </ul>
                            <?php } ?>
                            <?php if($_SESSION[USER_DETAILS][USER_LEVEL_ID] == 3) {?>
                                <ul class='login-home'>
                                        <!--<li><a href="./index.php">Home</a></li>
                                        <li><a href="./about.php">About Us</a></li>
                                        <li><a href="locations.php">All Parking Locations</a></li> -->
                                        <li><a href="bookparking.php">Book Parking</a></li>
                                        <li><a href="./user.php?user_id=<?php echo $_SESSION[USER_DETAILS][USER_ID]; ?>">My Account</a></li>
                                        <!--<li><a href="mybookings.php">My Bookings</a></li>-->
                                        <li><a href="change-password.php">Change Password</a></li>
                                        <li><a href="./lib/login.php?act=logout">Logout</a></li>
                                </ul>
                            <?php } ?>
                            <?php if($_SESSION[USER_DETAILS][USER_LEVEL_ID] == 2) {?>
                            <ul class='login-home'>
                            <li><a href="documents.php">Upload Documents</a></li>
                            </ul>
                            <?php
                            }
                            ?>
                            <?php if($_SESSION[USER_DETAILS][USER_LEVEL_ID] == 2 || $_SESSION[USER_DETAILS][USER_LEVEL_ID] == 4) {?>
                                <ul class='login-home'>
                                        
                                        <li><a href="location-listing.php">Book Parking</a></li>
                                        <li><a href="./user.php?user_id=<?php echo $_SESSION[USER_DETAILS][USER_ID]; ?>">My Account</a></li>
                                        <li><a href="change-password.php">Change Password</a></li>
                                        <li><a href="./lib/login.php?act=logout">Logout</a></li>
                                </ul>
                            <?php } ?>
                        </td>
                        <td style="width: 80%; vertical-align: top; padding-top: 11px;" id="dattab">
                   <?php if($_SESSION[USER_DETAILS][USER_LEVEL_ID] == 3) {?>
                   
                   <div id="mobdiv">
  <button style="background:#385ee7; height: 38px;"><a href="location-listing.php" class="links">Book Parking</a></button>    
  <button style="background:#385ee7; height: 38px;"><a href="./user.php?user_id=<?php echo $_SESSION[USER_DETAILS][USER_ID]; ?>" class="links">My Account</a></button>
  <button style="background:#385ee7; height: 38px;"><a href="change-password.php" class="links">Change Password</a></button>
  <button style="background:#385ee7; height: 38px;"><a href="./lib/login.php?act=logout" class="links">Logout</a></button>
</div>
                   <?php
                   }
                   ?>
<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'Upcoming')" id="defaultOpen">Upcoming Bookings</button>
  <button class="tablinks" onclick="openCity(event, 'Mybookings')">Booking History</button>
  <button class="tablinks" onclick="openCity(event, 'Cancel')">Cancelled Bookings</button>
</div>

<div id="Mybookings" class="tabcontent">
 <table class="table" id="mydatatable">
  <thead>
    <tr>
      <th scope="col">Parking Space Name</th>
      <!--<th scope="col">Parking Slot Number</th>-->
      <th scope="col">Parking Car No</th>
      <th scope="col">Parking In Time</th>
      <th scope="col">Parking Entry Date</th>
      <th scope=="col">Action</th>
    </tr>
  </thead>
  <tbody>
  <?php
        $SQLQRY="SELECT * FROM parking JOIN space ON parking.parking_space_id=space.space_id JOIN user ON parking.parking_user_id=user.user_id WHERE parking.parking_user_id =".$_SESSION[USER_DETAILS][USER_ID];
        $rsdata=mysqli_query($con,$SQLQRY);
        if(mysqli_num_rows($rsdata)) 
        {
            while($datares=mysqli_fetch_assoc($rsdata))
            {
               
  ?>
    <tr>
      <td><?php echo dec($datares[SPACE_TITLE]);  ?></td>
      <!--<td><?php //echo $datares[PARKING_SLOT_NUMBER];  ?></td>-->
      <td><?php echo $datares['parking_car_no'];  ?></td>
      <td><?php echo $datares['parking_intime'];  ?></td>
      <td><?php echo $datares['parking_entry_date'];  ?></td>
      <td>
      <a style="cursor: pointer;" href='viewuser.php?parking_id=<?=$datares[PARKING_ID] ?>&space_title=<?=$datares[SPACE_TITLE] ?>' >View</a>
      <!--|<a href="slotcancel.php?space_id=<?//=$datares['parking_space_id']?>&slot_no=<?//=$datares[PARKING_SLOT_NUMBER]?>&user=<?//=$_SESSION[USER_DETAILS][USER_ID]?>&parking_id=<?//=$datares[PARKING_ID] ?>&space_title=<?//=$datares[SPACE_TITLE] ?>">Cancel</a>-->
      </td>
    </tr>
   <?php
            }
        }
        else
        {
            ?>
            <?php
        }
        ?>
  </tbody>
</table>
</div>

<div id="Upcoming" class="tabcontent">
  <table class="table" id="mydatatable1">
  <thead>
    <tr>
      <th scope="col">Parking Space Name</th>
      <!--<th scope="col">Parking Slot Number</th>-->
      <th scope="col">Parking Car No</th>
      <th scope="col">Parking In Time</th>
      <th scope="col">Parking Entry Date</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  <?php
        $date = date('Y-m-d');
        $SQLQRY="SELECT * FROM parking JOIN space ON parking.parking_space_id=space.space_id JOIN user ON parking.parking_user_id=user.user_id WHERE parking.parking_date>'$date' AND parking.parking_user_id =".$_SESSION[USER_DETAILS][USER_ID];
        $rsdata=mysqli_query($con,$SQLQRY);
        if(mysqli_num_rows($rsdata)) 
        {
            while($datares=mysqli_fetch_assoc($rsdata))
            {
  ?>
    <tr>
      <td><?php echo dec($datares[SPACE_TITLE]);  ?></td>
      <!--<td><?php //echo $datares[PARKING_SLOT_NUMBER];  ?></td>-->
      <td><?php echo $datares['parking_car_no'];  ?></td>
      <td><?php echo $datares['parking_intime'];  ?></td>
      <td><?php echo $datares['parking_entry_date'];  ?></td>
     <td>
      <a style="cursor: pointer;" href='viewuser.php?parking_id=<?=$datares[PARKING_ID] ?>&space_title=<?=$datares[SPACE_TITLE] ?>' >View</a>|
      <a href="slotcancel.php?space_id=<?=$datares['parking_space_id']?>&slot_no=<?=$datares[PARKING_SLOT_NUMBER]?>&user=<?=$_SESSION[USER_DETAILS][USER_ID]?>&parking_id=<?=$datares[PARKING_ID] ?>&space_title=<?=$datares[SPACE_TITLE] ?>">Cancel</a>
      </td>
    </tr>
   <?php
            }
        }
        else
        {
            ?>
            <?php
        }
        ?>
  </tbody>
</table>
</div>
<div class="modal fade" id="viewlog" role="dialog">
    <div class="modal-dialog">
 
     <!-- Modal content-->
     <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Booking Info</h4>
        <button type="button" class="close" data-dismiss="modal" style="margin-top: -46px;">&times;</button>
      </div>
      <div class="modal-body">
 
      </div>
     <!-- <div class="modal-footer">
       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>-->
     </div>
    </div>
   </div>

<div id="Cancel" class="tabcontent">

<table class="table" id="mydatatable2">
  <thead>
    <tr>
      <th scope="col">Parking Space Name</th>
      <!--<th scope="col">Parking Slot Number</th>-->
      <th scope="col">Parking Car No</th>
      <th scope="col">Parking In Time</th>
      <th scope="col">Parking Entry Date</th>
    </tr>
  </thead>
  <tbody>
  <?php
        $SQLQRY="SELECT * FROM parking_cancel JOIN space ON parking_cancel.space_id=space.space_id JOIN user ON parking_cancel.user_id=user.user_id WHERE parking_cancel.user_id =".$_SESSION[USER_DETAILS][USER_ID];
        $rsdata=mysqli_query($con,$SQLQRY);
        if(mysqli_num_rows($rsdata)) 
        {
            while($datares=mysqli_fetch_assoc($rsdata))
            {
  ?>
    <tr>
      <td><?php echo dec($datares[SPACE_TITLE]);  ?></td>
      <!--<td><?php //echo $datares['slot_id'];  ?></td>-->
      <td><?php echo $datares['car_no'];  ?></td>
      <td><?php echo $datares['intime'];  ?></td>
      <td><?php echo $datares['entry_date'];  ?></td>
    </tr>
   <?php
            }
        }
        else
        {
            ?>
            <?php
        }
        ?>
  </tbody>
</table>        
        
</div>
                        </td>
                        </tr>
                </table>
            </fieldset>
   </div>
</section>
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
<!--  For customer slot cancellation  -->

<script>
$(document).ready(function(){

 $('.userinfo').click(function(){
   
   var userid = $(this).data('id');

   // AJAX request
   $.ajax({
    url: 'viewslot.php',
    type: 'post',
    data: {userid: userid},
    success: function(response){ 
      // Add response in Modal body
      $('.modal-body').html(response);

      // Display Modal
      $('#viewlog').modal('show'); 
    }
  });
 });
});
</script>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>


<?php include_once("includes/footer.php"); ?>