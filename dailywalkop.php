<?php 
    include_once("includes/header.php"); 
    include_once("includes/db_connect.php");
    $user_id= $_REQUEST["user_id"]; 
    $date2 = $_REQUEST["date2"];
    $date3 = $_REQUEST["date3"];
    $date = 'd-m-Y';
    define("DATE", $date);
    $new_date2 = date(DATE, strtotime($date2));
    $new_date3 = date(DATE, strtotime($date3));
?>
<style>
th
{
    background: #ddeeef;
    border: 1px solid #88707073;
	color: #000;
}
tr, td
{
    border: 1px solid #88707073;
}
#btnExport2
{
    background: #2430bb;
    color: #fff;
    border: 2px solid #2430bb;
    border-radius: 4px;	
	margin-top: -74px;
}
</style>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
    <script type="text/javascript">
        $("body").on("click", "#btnExport2", function () {
            html2canvas($('#datatable2')[0], {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("Walkin-report.pdf");
                }
            });
        });
    </script>
<script>
$(document).ready(function() {
    $('#datatable2').dataTable();
    
     $("[data-toggle=tooltip]").tooltip();
    
} );

</script>
<?php
if($date2 != '')
{
?>
    <?php
    $parking_mode = 'operator';
    $user_space_id = $_SESSION['user_details']['user_space_id'];
    $SQL="SELECT * FROM `parking` WHERE parking_space_id = '$user_space_id' AND parking_mode = '$parking_mode' AND parking_booked_date BETWEEN '$date2' AND  '$date3'";
    
    $rs1=mysqli_query($con,$SQL);
    
    $slots = mysqli_num_rows($rs1);
    if($slots > 0) 
    {
    ?>
        <div class="static" id="order_table2">
        <p style="text-align: right;" ><input type="button" id="btnExport2" value="Export to pdf" /></p>
        <p>&nbsp;</p>
        <table style="text-align: center; width: 800px;" id="datatable2" class="table table-striped table-bordered" >
        <thead>
		<tr>
        <!--<th>Parking Slot Number</th>-->
        <th>Parking Car Number</th>
        <th>Parking Charges</th>
        <th>Booking Date</th>
        </tr>
        </thead>
		<tbody>
        <?php
        while($data=mysqli_fetch_array($rs1))
        {
        $parking_booked_date = $data['parking_booked_date'];
        $booked_date = date(DATE, strtotime($parking_booked_date))
        ?>
        <tr>
        <!--<td><?//= $data['parking_slot_number'] ?></td>-->
        <td><?= $data['parking_car_no'] ?></td>
        <td><?= dec($data['parking_charges']) ?></td>
        <td><?= $booked_date ?></td>
        </tr>
        <?php
        }
        ?>
        <?php 
         echo "<h4>Walk-ins between ".$new_date2. " and " .$new_date3.  " is ". $slots ."</h4>"; 
    ?>
	</tbody>
        </table>
        </div>
    <?php
    }
    else
    {
    ?>
        <div class="static" id="order_table2">
                Slots not found
        </div>
    <?php
    }
    ?>
<?php
}
?>