<?php 
    include_once("includes/header.php"); 
    include_once("includes/db_connect.php");
    $user_id= $_REQUEST["user_id"]; 
    $date = $_REQUEST["date"];
    $new_date = date('d-m-Y', strtotime($date));
?>
<style>
th
{
    background: #ddeeef;
    border: 1px solid #88707073;
	color: #000;
}
tr, td
{
    border: 1px solid #88707073;
}
#btnExport1
{
    background: #2430bb;
    color: #fff;
    border: 2px solid #2430bb;
    border-radius: 4px;	
	margin-top: -77px;
}
</style>
 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
    <script type="text/javascript">
        $("body").on("click", "#btnExport1", function () {
            html2canvas($('#datatable1')[0], {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("Cancellation-report.pdf");
                }
            });
        });
    </script>
<script>
$(document).ready(function() {
    $('#datatable1').dataTable();
    
     $("[data-toggle=tooltip]").tooltip();
    
} );

</script>
<?php
if($date != '')
{
?>
    <?php
    $user_space_id = $_SESSION['user_details']['user_space_id'];
    $SQL="SELECT * FROM `parking_cancel` WHERE space_id = '$user_space_id' AND cancel_date = '$date'";
    
    $rs1=mysqli_query($con,$SQL);
    $slots = mysqli_num_rows($rs1);
    if($slots > 0) 
    {
    ?>
        <div class="static" id="order_table1">
        <p style="text-align: right;" ><input type="button" id="btnExport1" value="Export to pdf" /></p>
        <p>&nbsp;</p>
        <table style="text-align: center; width: 800px;" id="datatable1" class="table table-striped table-bordered" >
        <thead>
		<tr>
        <!--<th>Parking Slot Number</th>-->
        <th>Parking Car Number</th>
        <th>Parking Charges</th>
        <th>Booking Date</th>
        </tr>
        </thead>
		<tbody>
        <?php
        while($data=mysqli_fetch_array($rs1))
        {
        $parking_cancel_date = $data['cancel_date'];
        $cancel_date = date('d-m-Y', strtotime($parking_cancel_date))
        ?>
        <tr>
        <!--<td><?//= $data['slot_id'] ?></td>-->
        <td><?= $data['car_no'] ?></td>
        <td><?= dec($data['price']) ?></td>
        <td><?= $cancel_date ?></td>
        </tr>
        <?php
        }
        ?>
        <?php 
        echo "<h4>Cancellations on ".$cancel_date. "  is ". $slots ."</h4>";  
        ?>
		</tbody>
        </table>
        </div>
    <?php
    }
    else
    {
    ?>
        <div class="static" id="order_table1">
                Slots not found
        </div>
    <?php
    }
    ?>
<?php
}
?>