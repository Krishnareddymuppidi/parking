<?php
session_start();
include_once("../includes/db_connect.php");
$owner_id = $_SESSION['user_details']['user_id'];
if (isset($_POST['Upload'])) {
    $space_id= $_POST['space_id'];
    $space_location_id= $_POST['space_location_id'];

$document1 = $_FILES['document1']['name'];
$document2 = $_FILES['document2']['name'];
$document3 = $_FILES['document3']['name'];

$extension1 = pathinfo($document1, PATHINFO_EXTENSION);
$extension2 = pathinfo($document2, PATHINFO_EXTENSION);
$extension3 = pathinfo($document3, PATHINFO_EXTENSION);

$size1 = $_FILES['document1']['size'];
$size2 = $_FILES['document2']['size'];
$size3 = $_FILES['document3']['size'];
 
$tempname1 = $_FILES["document1"]["tmp_name"];
$tempname2 = $_FILES["document2"]["tmp_name"];
$tempname3 = $_FILES["document3"]["tmp_name"];
$folder1 = "../documents/".$document1; 
$folder2 = "../documents/".$document2; 
$folder3 = "../documents/".$document3; 

$db_document1=enc($document1);
$db_document2=enc($document2);
$db_document3=enc($document3);

$date = date('Y-m-d'); 
if(file_exists($folder1))
{
$exists = "File name ${document1} already exists";
header("Location:../documents.php?exists=$exists");
}


else
{
  if (!in_array($extension1, ['doc', 'pdf', 'docx', 'rtf'])) {
            $msgerror = "You file extension must be .doc, .rtf .pdf or .docx";
            header("Location:../documents.php?error=$msgerror");
    } else if ($_FILES['document1']['size'] > 5000000) { // file shouldn't be larger than 5Megabyte
            $msgerror = "File too large!";
            header("Location:../documents.php?error=$msgerror");
    } else {
        $extension = end(explode(".", $_FILES["document1"]["name"]));
        $usr_name = dec($_SESSION['user_details']['user_name']);
        $db_document1 = enc($usr_name);
        $ext1 = enc($extension);
        
        /* For document2     */
        $extension2 = end(explode(".", $_FILES["document2"]["name"]));
        $usr_name2=$usr_name.'1';
        $db_document2 = enc($usr_name2);
        $ext2 = enc($extension2);
        if($document2 == '')
        {
            $db_document2='';
        }
        
        /* For document3     */
        $extension3 = end(explode(".", $_FILES["document3"]["name"]));
        $usr_name3=$usr_name.'2';
        $db_document3 = enc($usr_name3);
        $ext3 = enc($extension3);
         if($document3 == '')
        {
            $db_document3='';
        }

        move_uploaded_file($tempname1, "../documents/{$usr_name}.{$extension}");
        move_uploaded_file($tempname2, "../documents/{$usr_name2}.{$extension2}");
        move_uploaded_file($tempname3, "../documents/{$usr_name3}.{$extension3}");
        // Get all the submitted data from the form 
        $sql = "INSERT INTO documents (owner_id,space_id,space_location_id,document1,document2,document3,ext1,ext2,ext3,date) VALUES ('$owner_id','$space_id','$space_location_id','$db_document1','$db_document2','$db_document3','$ext1','$ext2','$ext3','$date')"; 
  
        // Execute query 
        $res= mysqli_query($con, $sql); 
          
        // Now let's move the uploaded image into the folder: image 
        if ($res)  { 
		/* SMS Sending   */
       $user_mobile=dec($_SESSION['user_details']['user_mobile']);
	  $sms_mob1='91'.$user_mobile;

		$post1 = [
    'receiver_number' => $sms_mob1,
    'receiver_message' => 'Message From DSS Parking Mangement System: Thank you for upload documents, we will get back to you shortly'
];

$ch1 = curl_init('https://www.clinicnext.com/op/hms/public/api/send_message');
curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch1, CURLOPT_POSTFIELDS, $post1);

// execute!
$response = curl_exec($ch1);

// close the connection, release resources used
curl_close($ch1);

		/* SMS ends  */
            $msg = "Documents uploaded successfully"; 
            header("Location:../login-home.php?doc=$msg");
        }
        else{ 
            $msg = "Failed to upload File";
            header("Location:../documents.php?msg=$msg");
      }
    }
}
}     