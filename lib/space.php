<?php
    include_once("../includes/db_connect.php");
    include_once("../includes/functions.php");
    if($_REQUEST[act]=="save_space")
    {
        save_space();
        exit;
    }
    if($_REQUEST[act]=="delete_space")
    {
        delete_space();
        exit;
    }
    if($_REQUEST[act]=="update_space_status")
    {
        update_space_status();
        exit;
    }
    
    ###Code for save space#####
    function save_space()
    {
        global $con;
        $R=$_REQUEST;
        $space_location_id = mysqli_real_escape_string($con,$_REQUEST[space_location_id]);        
        $space_title = mysqli_real_escape_string($con,enc($_REQUEST[space_title]));
        $space_amenities = mysqli_real_escape_string($con,enc($R[space_amenities]));
        $space_hours = mysqli_real_escape_string($con,enc($R[space_hours]));
        $space_things = mysqli_real_escape_string($con,enc($R[space_things]));
        $space_description = mysqli_real_escape_string($con,enc($R[space_description]));
        $space_owner_id = $_SESSION['user_details']['user_id'];
        /*  For image uploading   */    
        $image_name = $_FILES[space_image][name];
        $location = $_FILES[space_image][tmp_name];
        if($image_name!="")
        {
            move_uploaded_file($location,"../spaces/".$image_name);
        }
                
        if($R[space_id])
        {
            $statement = "UPDATE `space` SET";
            $cond = "WHERE `space_id` = '$R[space_id]'";
            $msg = "Data Updated Successfully.";
        }
        else
        {
            $statement = "INSERT INTO `space` SET";
            $cond = "";
            $msg="Parking space added successfully, please upload your documents";
        }
        $SQL=   $statement." 
                `space_location_id` = '$space_location_id', 
                `space_title` = '$space_title', 
                `space_total_parkings` = '$R[space_total_parkings]', 
                `space_image` = '$image_name',
                `space_amenities` = '$space_amenities',
                `space_hours` = '$space_hours',
                `space_things` = '$space_things',
                `space_owner_id` = '$space_owner_id',
                `space_description` = '$space_description'". 
                 $cond;
        $rs = mysqli_query($con,$SQL);
        header("Location:../login-home.php?space=$msg");
    }
#########Function for delete space##########3
function delete_space()
{
    global $con;
    /////////Delete the record//////////
    $SQL="DELETE FROM space WHERE space_id = $_REQUEST[space_id]";
    mysqli_query($con,$SQL);
    header("Location:../space-report.php?msg=Deleted Successfully.");
}
?>
