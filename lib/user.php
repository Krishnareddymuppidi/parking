<?php
    session_start();
	error_reporting(0);
    include_once("../includes/db_connect.php");
    include_once("../includes/functions.php");
    if($_REQUEST[act]=="save_user")
    {
        save_user();
        exit;
    }
    
    if($_REQUEST[act]=="save_operator")
    {
        save_operator();
        exit;
    }
    if($_REQUEST[act]=="delete_user")
    {
        delete_user();
        exit;
    }
    if($_REQUEST[act]=="get_report")
    {
        get_report();
        exit;
    }
    ###Code for save user#####
    function save_user()
    {
        global $con;
        $R=$_REQUEST;
        $username = mysqli_real_escape_string($con,$_REQUEST[user_username]);
        $password = mysqli_real_escape_string($con,$_REQUEST[user_password]);
        $user_confirm_password = mysqli_real_escape_string($con,$_REQUEST[user_confirm_password]);
        $name = mysqli_real_escape_string($con,$_REQUEST[user_name]);
        $add1 = mysqli_real_escape_string($con,$_REQUEST[user_add1]);
        $add2 = mysqli_real_escape_string($con,$_REQUEST[user_add2]);
        $city = mysqli_real_escape_string($con,$_REQUEST[user_city]);
        $state = mysqli_real_escape_string($con,$_REQUEST[user_state]);
        $country = mysqli_real_escape_string($con,$_REQUEST[user_country]);
        $email = mysqli_real_escape_string($con,$_REQUEST[user_email]);
        $mobile = mysqli_real_escape_string($con,$_REQUEST[user_mobile]);
        $gender = mysqli_real_escape_string($con,$_REQUEST[user_gender]);
        $dob = mysqli_real_escape_string($con,$_REQUEST[user_dob]);
        $user_level_id = mysqli_real_escape_string($con,$_REQUEST[user_level_id]);
        if($password != $user_confirm_password)
        {
        header("Location:../user.php?role=$user_level_id&pwd=notmatch");
        exit;    
        }
        if(!preg_match("/^[a-zA-Z\s]+$/", $username))
        {
        header("Location:../user.php?role=$user_level_id&user=usererror");
        exit;
        }
        if(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{6,10}$/', $password))
        {
        header("Location:../user.php?role=$user_level_id&pass=passerror");
        exit;
        }
        if(!preg_match("/^[a-zA-Z\s]+$/", $name))
        {
        header("Location:../user.php?role=$user_level_id&name=nameerror");
        exit;
        }
        if(!preg_match("/[A-Za-z0-9\-\\,.]+/", $add1))
        {
        header("Location:../user.php?role=$user_level_id&add1=add1error");
        exit;
        }
        if(!preg_match("/[A-Za-z0-9\-\\,.]+/", $add2))
        {
        header("Location:../user.php?role=$user_level_id&add2=add2error");
        exit;
        }
        if(!$city)
        {
        header("Location:../user.php?role=$user_level_id&city=cityerror");
        exit;
        }
        if(!$state)
        {
        header("Location:../user.php?role=$user_level_id&state=stateerror");
        exit;
        }
        if(!$country)
        {
        header("Location:../user.php?role=$user_level_id&country=countryerror");
        exit;
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
        {
        header("Location:../user.php?role=$user_level_id&email=emailerror");
        exit;
        }
        if(!preg_match('/^[0-9]{10}+$/', $mobile)) 
        {
        header("Location:../user.php?role=$user_level_id&mobile=mobileerror");
        exit;
        }

        $user_username=enc($username);
        $user_password=enc($password);
        $user_name=enc($name);
        $user_add1=enc($add1);
        $user_add2=enc($add2);
        $user_city=enc($city);
        $user_state=enc($state);
        $user_country=enc($country);
        $user_email=enc($email);
        $user_mobile=enc($mobile);
        $user_gender=enc($gender);
        $user_dob=enc($dob);
        ///Checking Username Exits or not ////
        $SQL="SELECT * FROM user WHERE user_username = $user_username";
        $rs=mysqli_query($con,$SQL);
        $data=mysqli_fetch_assoc($rs);
        if($data['user_username'] && $R['user_id'] == "") {
            header("Location:../user.php?msg=Username Already Exits. Kindly choose another....");
            return;
        }
        /////////////////////////////////////
        $image_name = $_FILES[user_image][name];
        $location = $_FILES[user_image][tmp_name];
        if($image_name!="")
        {
            move_uploaded_file($location,"../uploads/".$image_name);
        }
        else
        {
            $image_name = $R[avail_image];
        }
        if($R[user_id])
        {
            $statement = "UPDATE `user` SET";
            $cond = "WHERE `user_id` = '$R[user_id]'";
            $msg = "Data Updated Successfully.";
            $condQuery = "";
        }
        else
        {
            $statement = "INSERT INTO `user` SET";
            $condQuery = "`user_username` = '$user_username', 
                          `user_password` = '$user_password',"; 
            $cond = "";
            $msg="Data saved successfully.";
        }
        $SQL=   $statement." 
                `user_level_id` = '$R[user_level_id]', 
                ".
                 $condQuery
                 ."
                `user_name` = '$user_name', 
                `user_add1` = '$user_add1', 
                `user_add2` = '$user_add2', 
                `user_city` = '$user_city', 
                `user_state` = '$user_state', 
                `user_country` = '$user_country', 
                `user_email` = '$user_email', 
                `user_mobile` = '$user_mobile', 
                `user_gender` = '$user_gender', 
                `user_dob` = '$user_dob',
                `user_image` = '$image_name'". 
                 $cond;
        $rs = mysqli_query($con,$SQL);
	
        
        /*  For email function  */
        
        function send_mail($fromname, $fromaddress, $toaddress, $subject, $message,$cc)
        {
       $headers  = "MIME-Version: 1.0\n";
       $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
       $headers .= "X-Priority: 3\n";
       $headers .= "X-MSMail-Priority: Normal\n";
       $headers .= "X-Mailer: php\n";
       $headers .= "From: \"".$fromname."\" <".$fromaddress.">\n";
          if($cc != '') 
          {     
             $headers .= "Cc:".$cc."\r\n";
          }
       mail($toaddress, $subject, $message, $headers);
        }
    
    if(!empty($_POST['name']))
    {
        $fromname = $R['user_name'];
        $user_add1 = $R['user_add1'];
        $user_city = $R['user_city'];
        $user_state = $R['user_state'];
        $user_country = $R['user_country'];
        $user_mobile = $R['user_mobile'];
        $user_gender = $R['user_gender'];
        $user_dob = $R['user_dob'];
        
        $fromaddress = 'info@carparkingsystem.com';
        $toname = 'Admin';
        
        $toaddress = $R['user_email'];
        $cc = '';
        $subject = 'Registration of carparking systems';
        $message = '<html><body><fieldset style="border-color: #FF0000"><form><table  align="center" cellpadding="0" cellspacing="5" bordercolor="#FFFFFF">
       <tr align="center">
       <td colspan="2" align="center" valign="top" style="color:#FF0000"><U><strong>Registration Details</strong></U></td>
       </tr>                     
                   <tr> 
                <td width="256" align="left" valign="top"><div align="right"><strong>Name 
                            : </strong></div></td>
                        <td align="left" valign="top" width="259">
                          '.$fromname.'
                          </td>
                      </tr>
                      <tr> 
                        <td align="left" valign="top"><div align="right"><strong>Email Address:</strong></div></td>
                        <td align="left" valign="top" width="259"> 
                          '.$toaddress.'
                          </td>
                      </tr>
                      <tr> 
                        <td align="left" valign="top"><div align="right"><strong>Mobile Number: 
                            </strong></div></td>
                        <td align="left" valign="top" width="259"> 
                          '.$user_mobile.'
                         </td>
                      </tr>
                      <tr> 
                        <td align="left" valign="top"><div align="right"><strong>Address: 
                            </strong></div></td>
                        <td align="left" valign="top" width="259"> 
                          '.$user_add1.'
                         </td>
                      </tr>
                       <tr> 
                        <td align="left" valign="top"><div align="right"><strong>City: 
                            </strong></div></td>
                        <td align="left" valign="top" width="259"> 
                          '.$user_city.'
                         </td>
                      </tr>
                       <tr> 
                        <td align="left" valign="top"><div align="right"><strong>State: 
                            </strong></div></td>
                        <td align="left" valign="top" width="259"> 
                          '.$user_state.'
                         </td>
                      </tr>
                       <tr> 
                        <td align="left" valign="top"><div align="right"><strong>Country: 
                            </strong></div></td>
                        <td align="left" valign="top" width="259"> 
                          '.$user_country.'
                         </td>
                      </tr>
                       <tr> 
                        <td align="left" valign="top"><div align="right"><strong>Gender: 
                            </strong></div></td>
                        <td align="left" valign="top" width="259"> 
                          '.$user_gender.'
                         </td>
                      </tr>
                       <tr> 
                        <td align="left" valign="top"><div align="right"><strong>Date of birth: 
                            </strong></div></td>
                        <td align="left" valign="top" width="259"> 
                          '.$user_dob.'
                         </td>
                      </tr>
                      <tr> 
                        <td align="left" valign="top"></td>
                        <td align="left" valign="top" width="259"></td>
                      </tr>
                    </table></form></fieldset></body></html>';
        send_mail($fromname, $fromaddress, $toname, $toaddress, $subject, $message, $cc);
    }
        /*    Email function ends   */
        
        //// Creating User Leaves /////
        if($R[user_id] == "") {
            $id = mysqli_insert_id();
        }
        if($_SESSION['login']!=1)
        {
            	/* SMS Sending  */
		$sms_mob='91'.$mobile;

		$post = [
    'receiver_number' => $sms_mob,
    'receiver_message' => 'Message From Parking Mangement System: Thank you for registering with us'
];

$ch = curl_init('https://www.clinicnext.com/op/hms/public/api/send_message');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

// execute!
$response = curl_exec($ch);

// close the connection, release resources used
curl_close($ch);

		/* SMS Ends    */
		
            header("Location:../login.php?msg=You are registered successfully. Login with your credential !!!");
            exit;
        }
        else if($_SESSION['user_details']['user_level_id'] == 3) {
            header("Location:../user.php?user_id=".$_SESSION['user_details']['user_id']."&msg=Your account updated successfully !!!");
            exit;
        }
        header("Location:../user-report.php?msg=$msg");
    }
#########Function for delete user##########3
function delete_user()
{
    global $con;
    $SQL="SELECT * FROM user WHERE user_id = $_REQUEST[user_id]";
    $rs=mysqli_query($con,$SQL);
    $data=mysqli_fetch_assoc($rs);
    
    /////////Delete the record//////////
    $SQL="DELETE FROM user WHERE user_id = $_REQUEST[user_id]";
    mysqli_query($con,$SQL);
    
    //////////Delete the image///////////
    if($data[user_image])
    {
        unlink("../uploads/".$data[user_image]);
    }
    header("Location:../user-report.php?msg=Deleted Successfully.");
}
/* For operator saving */
function save_operator()
{

	
    global $con;
        $R=$_REQUEST;
        if($_REQUEST[user_password] != $_REQUEST[user_confirm_password])
        {
            header("Location:../operator.php?pwd=pwdnotmatch");
            exit;
        }
		
		
        $user_username = enc($_REQUEST[user_username]);
        $user_password = enc($_REQUEST[user_password]);
        $user_name = enc($_REQUEST[user_name]);
        $user_add1 = enc($_REQUEST[user_add1]);
        $user_add2 = enc($_REQUEST[user_add2]);
        $user_city = enc($_REQUEST[user_city]);
        $user_state = enc($_REQUEST[user_state]);
        $user_country = enc($_REQUEST[user_country]);
        $user_email = enc($_REQUEST[user_email]);
        $user_mobile = enc($_REQUEST[user_mobile]);
        $user_dob = enc($_REQUEST[user_dob]);
        
        ///Checking Username Exits or not ////
        $SQL="SELECT * FROM user WHERE user_username = $user_username";
        $rs=mysqli_query($con,$SQL);
        $data=mysqli_fetch_assoc($rs);
        if($data['user_username'] && $R['user_id'] == "") {
            header("Location:../operator.php?msg=Username Already Exits. Kindly choose another....");
            return;
        }
        /////////////////////////////////////
        $image_name = $_FILES[user_image][name];
        $location = $_FILES[user_image][tmp_name];
        if($image_name!="")
        {
            move_uploaded_file($location,"../uploads/".$image_name);
        }
        else
        {
            $image_name = $R[avail_image];
        }
        
        if($R[user_id])
        {
            $statement = "UPDATE `user` SET";
            $cond = "WHERE `user_id` = '$R[user_id]'";
            $msg = "Data Updated Successfully.";
            $condQuery = "";
        }
        else
        {
            $statement = "INSERT INTO `user` SET";
            $condQuery = "`user_username` = '$user_username', 
                          `user_password` = '$user_password',"; 
            $cond = "";
            $msg="Data saved successfully.";
        }
        $created_by= $_SESSION['user_details']['user_id'];
        $SQL=   $statement." 
                `user_level_id` = '$R[user_level_id]', 
                ".
                 $condQuery
                 ."
                `user_name` = '$user_name', 
                `user_add1` = '$user_add1', 
                `user_add2` = '$user_add2', 
                `user_city` = '$user_city', 
                `user_state` = '$user_state', 
                `user_country` = '$user_country', 
                `user_email` = '$user_email', 
                `user_mobile` = '$user_mobile', 
                `user_dob` = '$user_dob',
                `created_by` = '$created_by',
                `user_space_id` = '$R[user_space_id]',
                `user_image` = '$image_name'". 
                 $cond;
        $rs = mysqli_query($con,$SQL);
        $msg="Operator created successfully.";
        
        	
		
    if($rs)
    {		
        	/* SMS Sending  */
        	$mobile1 = $_REQUEST[user_mobile];
		$sms_mob1='91'.$mobile1;

		$post1 = [
    'receiver_number' => $sms_mob1,
    'receiver_message' => 'Message From Parking Mangement System: Thank you for registering with us'
];

$ch1 = curl_init('https://www.clinicnext.com/op/hms/public/api/send_message');
curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch1, CURLOPT_POSTFIELDS, $post1);

// execute!
$response = curl_exec($ch1);

// close the connection, release resources used
curl_close($ch1);

		/* SMS Ends    */
        header("Location:../operator.php?msg=$msg");
    }
}
?>