<?php
    include_once("../includes/db_connect.php");
    include_once("../includes/functions.php");
    
    if($_GET[st]=="act")
    {
        active();
        exit;
    }
    if($_GET[st]=="inact")
    {
        inactive();
        exit;
    }
    ###Code for deactive status#####
    function active()
    {
        global $con;
        $R=$_GET;                            
        if($R[act])
        {
            $statement = "UPDATE `prices` SET";
            $cond = "WHERE `id` = '$R[act]'";
            $msg = "Staus In-Activated Successfully.";
        }
        $SQL=   $statement." 
                `status` = '0'". 
                 $cond;
        $SQL1="SELECT * FROM `space` WHERE space_id = $R[space_id]";
        $rst=mysqli_query($con,$SQL1);
        $data=mysqli_fetch_assoc($rst);
        $space_name=$data[space_title];
        
        $rs = mysqli_query($con,$SQL);
        $user_id = $_SESSION['user_details']['user_id'];
        $title = "Status";
        $upd = "de-actived";
        date_default_timezone_set('Asia/Kolkata');
        $date = date('d-m-Y h:i:s');
        $newdate = date('Y-m-d');
        $QRY = "INSERT INTO activitylog (user_id,space_name,title,upd_val,datetime,date)
     VALUES ('$user_id','$space_name','$title','$upd','$date','$newdate')";
     $rs1 = mysqli_query($con,$QRY);
        header("Location:../priceadd.php?space_id=$R[space_id]&status=$msg");
    }
    ###Code for active status#####
    function inactive()
    {
        global $con;
        $R=$_GET;                            
        if($R[inact])
        {
            $statement = "UPDATE `prices` SET";
            $cond = "WHERE `id` = '$R[inact]'";
            $msg = "Staus Activated Successfully.";
        }
        $SQL=   $statement." 
                `status` = '1'". 
                 $cond;
                 
        $SQL1="SELECT * FROM `space` WHERE space_id = $R[space_id]";
        $rst=mysqli_query($con,$SQL1);
        $data=mysqli_fetch_assoc($rst);
        $space_name=$data[space_title];
        
        $rs = mysqli_query($con,$SQL);
        $user_id = $_SESSION['user_details']['user_id'];
        $title = "Status";
        $upd = "actived";
        date_default_timezone_set('Asia/Kolkata');
        $date = date('d-m-Y h:i:s');
        $newdate = date('Y-m-d');
        $QRY = "INSERT INTO activitylog (user_id,space_name,title,upd_val,datetime,date)
     VALUES ('$user_id','$space_name','$title','$upd','$date','$newdate')";
     $rs1 = mysqli_query($con,$QRY);
        header("Location:../priceadd.php?space_id=$R[space_id]&status=$msg");
    }
?>