<?php
    include_once("../includes/db_connect.php");
    include_once("../includes/functions.php");
    if($_REQUEST[act]=="save_location")
    {
        save_location();
        exit;
    }
    if($_REQUEST[act]=="delete_location")
    {
        delete_location();
        exit;
    }
    if($_REQUEST[act]=="update_location_status")
    {
        update_location_status();
        exit;
    }
    
    ###Code for save location#####
    function save_location()
    {
        global $con;
        $R=$_REQUEST;    
        $locationname = mysqli_real_escape_string($con,$_REQUEST[location_name]);
        $locationemail = mysqli_real_escape_string($con,$_REQUEST[location_email]);
        $locationlat = mysqli_real_escape_string($con,$_REQUEST[location_lat]);
        $locationlog = mysqli_real_escape_string($con,$_REQUEST[location_log]);
        $locationcontact = mysqli_real_escape_string($con,$_REQUEST[location_contact]);
        $locationzipcode = mysqli_real_escape_string($con,$_REQUEST[location_zipcode]);
        $locationaddress = mysqli_real_escape_string($con,$_REQUEST[location_address]);
        
        if(!preg_match("/^[a-zA-Z\s]+$/", $locationname))
        {
        header("Location:../location.php?name=nameerror");
        exit;
        }
        if (!filter_var($locationemail, FILTER_VALIDATE_EMAIL)) 
        {
        header("Location:../location.php?email=emailerror");
        exit;
        }
        if(!preg_match('/^[0-9]{10}+$/', $locationcontact)) 
        {
        header("Location:../location.php?contact=contacterror");
        exit;
        }
        if(!$locationzipcode) 
        {
        header("Location:../location.php?zipcode=zipcodeerror");
        exit;
        }
        
        $location_owner_id = $_SESSION['user_details']['user_id'];
        $location_name=enc($locationname);
        $location_contact=enc($locationcontact);
        $location_email=enc($locationemail);
        $location_lat=enc($locationlat);
        $location_log=enc($locationlog);
        $location_zipcode=enc($locationzipcode);
        $location_address=enc($locationaddress);
                
        if($R[location_id])
        {
            $statement = "UPDATE `location` SET";
            $cond = "WHERE `location_id` = '$R[location_id]'";
            $msg = "Data Updated Successfully.";
        }
        else
        {
            $statement = "INSERT INTO `location` SET";
            $cond = "";
            $msg="Location added successfully, Please add parking space";
        }
        $SQL=   $statement." 
                `location_name` = '$location_name', 
                `location_contact` = '$location_contact', 
                `location_email` = '$location_email', 
                `location_lat` = '$location_lat', 
                `location_log` = '$location_log',
                `location_zipcode` = '$location_zipcode',
                `location_owner_id` = '$location_owner_id',
                `location_address` = '$location_address'". 
                 $cond;
        $rs = mysqli_query($con,$SQL);
        header("Location:../login-home.php?loc=$msg");
    }
#########Function for delete location##########3
function delete_location()
{
    global $con;
    /////////Delete the record//////////
    $SQL="DELETE FROM location WHERE location_id = $_REQUEST[location_id]";
    mysqli_query($con,$SQL);
    header("Location:../location-report.php?msg=Deleted Successfully.");
}
?>
