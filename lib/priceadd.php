<?php
    include_once("../includes/db_connect.php");
    include_once("../includes/functions.php");

    if($_REQUEST[act]=="save_price")
    {
        save_price();
        exit;
    }
    if($_REQUEST[act]=="delete_location")
    {
        delete_location();
        exit;
    }
    if($_REQUEST[act]=="update_price")
    {
        update_price();
        exit;
    }
    
    ###Code for save prices#####
    function save_price()
    {
        global $con;
        $R=$_REQUEST;
        $rate_hr = enc($_REQUEST[rate_hr]);
        $rate_week = enc($_REQUEST[rate_week]);
        $rate_month = enc($_REQUEST[rate_month]);
        $rate_day = enc($_REQUEST[rate_day]);
        $updated_rate_hr = enc($_REQUEST[updated_rate_hr]);
        $updated_rate_week = enc($_REQUEST[updated_rate_week]);
        $updated_rate_month = enc($_REQUEST[updated_rate_month]);
        $updated_rate_day = enc($_REQUEST[updated_rate_day]);
        $updated_rate_premium = enc($_REQUEST[updated_rate_premium]);
        $rate_premium = enc($_REQUEST[rate_premium]);
        
        if($R[id])
        {
            $statement = "UPDATE `prices` SET";
            $cond = "WHERE `space_id` = '$R[space_id]'";
            $msg = "Price Updated Successfully.";
        }
        else
        {
            $statement = "INSERT INTO `prices` SET";
            $cond = "";
            $msg="Price saved successfully.";
        }
        $SQL=   $statement." 
                `space_id` = '$R[space_id]',
                `rate_hr` = '$rate_hr', 
                `rate_week` = '$rate_week', 
                `rate_month` = '$rate_month', 
                `rate_day` = '$rate_day',
                `user_id` = '$R[user_id]',
                `updated_rate_hr` = '$updated_rate_hr',
                `updated_rate_week` = '$updated_rate_week',
                `updated_rate_month` = '$updated_rate_month',
                `updated_rate_day` = '$updated_rate_day',
                `updated_rate_premium` = '$updated_rate_premium',
                `rate_premium` = '$rate_premium'". 
                 $cond;
        $rs = mysqli_query($con,$SQL);
        
            $SQL="SELECT * FROM `space` WHERE space_id = $R[space_id]";
        $rs=mysqli_query($con,$SQL);
        $data=mysqli_fetch_assoc($rs);
        $space_name=$data[space_title];
        date_default_timezone_set('Asia/Kolkata');
        $date = date('d-m-Y h:i:s');
        $newdate = date('Y-m-d');
        if(($R[updated_rate_hr]!=$R[rate_hr]) && ($R[updated_rate_hr] != '') )
        {
        $upd = "From".' '.$R[updated_rate_hr].' '. "to".' '. $R[rate_hr];
        $title="Rate/hr";
        $QRY = "INSERT INTO activitylog (user_id,space_name,title,upd_val,datetime,date)
     VALUES ('$R[user_id]','$space_name','$title','$upd','$date','$newdate')";
     $rs1 = mysqli_query($con,$QRY);
        }
        if(($R[updated_rate_week]!=$R[rate_week]) && ($R[updated_rate_week] != ''))
        {
        $upd = "From".' '.$R[updated_rate_week].' '. "to".' '. $R[rate_week];
        $title="Rate/week";
        $QRY = "INSERT INTO activitylog (user_id,space_name,title,upd_val,datetime,date)
     VALUES ('$R[user_id]','$space_name','$title','$upd','$date','$newdate')";
     $rs1 = mysqli_query($con,$QRY);
        }
        if(($R[updated_rate_month]!=$R[rate_month]) && ($R[updated_rate_month] != ''))
        {
        $upd = "From".' '.$R[updated_rate_month].' '. "to".' '. $R[rate_month];
        $title="Rate/month";
        $QRY = "INSERT INTO activitylog (user_id,space_name,title,upd_val,datetime,date)
     VALUES ('$R[user_id]','$space_name','$title','$upd','$date','$newdate')";
     $rs1 = mysqli_query($con,$QRY);
        }
        if(($R[updated_rate_day]!=$R[rate_day]) && ($R[updated_rate_day] != ''))
        {
        $upd = "From".' '.$R[updated_rate_day].' '. "to".' '. $R[rate_day];
        $title="Rate/day";
        $QRY = "INSERT INTO activitylog (user_id,space_name,title,upd_val,datetime,date)
     VALUES ('$R[user_id]','$space_name','$title','$upd','$date','$newdate')";
     $rs1 = mysqli_query($con,$QRY);
        }
        if(($R[updated_rate_premium]!=$R[rate_premium]) && ($R[updated_rate_premium] != ''))
        {
        $upd = "From".' '.$R[updated_rate_premium].' '. "to".' '. $R[rate_premium];
        $title="Rate/premium";
        $QRY = "INSERT INTO activitylog (user_id,space_name,title,upd_val,datetime,date)
     VALUES ('$R[user_id]','$space_name','$title','$upd','$date','$newdate')";
     $rs1 = mysqli_query($con,$QRY);
        }
        
        header("Location:../priceadd.php?space_id=$R[space_id]&msg=$msg");
    }
#########Function for delete location##########3
function delete_location()
{
    global $con;
    /////////Delete the record//////////
    $SQL="DELETE FROM location WHERE location_id = $_REQUEST[location_id]";
    mysqli_query($con,$SQL);
    header("Location:../location-report.php?msg=Deleted Successfully.");
}
?>