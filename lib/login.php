<?php
    session_start();
    
    include_once("../includes/db_connect.php");
    if($_REQUEST[act]=="check_login")
    {
        check_login();
    }
    if($_REQUEST[act]=="logout")
    {
        logout();
    }
    if($_REQUEST[act] == "change_password")
    {
        change_password();
    }
####Function check user#######
function check_login()
{
    global $con;
    $email = mysqli_real_escape_string($con,$_REQUEST[user_email]);
    $password = mysqli_real_escape_string($con,$_REQUEST[user_password]);
    
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
    {
    header("Location:../login.php?usererror=usernameerror");
    exit;
    }
    if(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{6,10}$/', $password)) 
    {
     header("Location:../login.php?passerror=passworderror");
     exit;
    }
  
    $user_email=enc($email); 
    $user_password=enc($password);
    $SQL="SELECT * FROM user WHERE user_email = '$user_email' AND user_password = '$user_password'";
    $rs = mysqli_query($con,$SQL);
    if(mysqli_num_rows($rs))
    {
        $_SESSION[login]=1;
        $_SESSION['user_details'] = mysqli_fetch_assoc($rs);
        
        /*   For update timestamp    */
        date_default_timezone_set('Asia/Kolkata');
        $currentTime = date( 'd-m-Y h:i:s A', time () );
        $SQL = "UPDATE `user` SET user_login_time = '$currentTime' WHERE `user_id` = ".$_SESSION['user_details']['user_id'];
        $rs = mysqli_query($con,$SQL);

        header("Location:../login-home.php");
    }
    else
    {
        header("Location:../login.php?msg=Invalid User and Password.");
    }
}
####Function logout####
function logout()
{
    $_SESSION[login]=0;
    $_SESSION['user_details'] = 0;
    header("Location:../login.php?msg=Logout Successfullly.");
}
#####Function for changing the password ####
function change_password() {
    global $con;
    $R = $_REQUEST;
    $newpassword = mysqli_real_escape_string($con,$_REQUEST[user_new_password]);
    $confirmpassword = mysqli_real_escape_string($con,$_REQUEST[user_confirm_password]);
    
    $user_new_password=enc($newpassword); 
    if($newpassword != $confirmpassword) {
        header("Location:../change-password.php?msg=Your new passsword and confirm password does not match!!!");
        exit;
    }
    $SQL = "UPDATE `user` SET user_password = '$user_new_password' WHERE `user_id` = ".$_SESSION['user_details']['user_id'];
    $rs = mysqli_query($con,$SQL);
    header("Location:../change-password.php?msg=Your Password Changed Successfully !!!");
    print $SQL;
    die;
}
?>