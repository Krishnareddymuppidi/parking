<?php 
    include_once("includes/header.php"); 
?> 
<section id="subintro">
   <div class="jumbotron subhead" id="overview">
      <div class="container">
         <div class="row">
            <div class="span12">
               <div class="centered">
                  <h3>Car Parking System</h3>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<section id="maincontent">
   <div class="container">
        <h4 class="heading colr">Become an Affiliate</h4>
        <div style="font-size:12px;">
            <strong>Grow your business with DSS PMS</strong>
<ul>
<li>Do you own a website and are looking for opportunities to increase your revenue?</li>
<li>Are you a travel company and looking for ways to offer parking to your customers and provide a complete holiday package?</li>
</ul>
<p>No matter your purpose, we at  DSS PMS  will find a solution that will suit your needs. <b>Join us today for free</b> and add an extra revenue stream to your business!</p>

<strong>How does it work?</strong>
<p>With a wide coverage of <b>parking in the entire country,</b> we provide services for hundreds of businesses across the globe.<br>
Airport, port, city or station parking - we can ensure a wide choice of competitive car park offers and a simple process for management of parking bookings.<br>
As a  DSS PMS  affiliate, you will earn a <b>sliding scale of commission,</b> based on the volume of bookings you generate. Click on the "Apply now" button at the bottom of the page to start earning money from parking today!
</p>
<strong>Why choose  DSS PMS  as an affiliate partner?</strong>
<ul>
<li><b>Global Parking Services </b>– As an affiliate member of  DSS PMS , you will receive access to a global parking network to monetize your website.</li>
<li><b>Multilingual Solutions </b>– We provide parking services in more than 25 languages. Based on your needs, you can choose to promote parking in a specific market, and we will provide you with local content and customer service.</li>
<li><b>Profitable </b>– We offer attractive commissions, starting from 5%. As our affiliate partner, you will profit from one of the highest revenue earning potentials in the industry, to optimise your conversion rates.</li>
<li><b>No Commitment </b>– Joining us is absolutely free. We have no membership fee and no commitment in terms of number of bookings.</li>
<li><b>Flexible Implementation</b> – We provide support with HTML integration as well as various deep linking options to suit your needs.</li>
<li><b>Real Time Statistics </b>–  DSS PMS  believes that transparency is a key to a successful partnership. As our affiliate you will have access to real time online statistics about your bookings. Many of our partners use this information to maximize their revenue stream.</li>
<li><b>Marketing Assistance </b>– Our internal marketing team is here to provide consultancy on how to plan the exposure of parking services on your website. Furthermore, we can offer support with the development of advertising materials for your website.</li>
</ul>
<p>Our application process is simple and straightforward! <b>To join  DSS PMS  today, simply click on the "Apply now" button above </b>and complete the application form. Our team will be in touch with you to set up your account so you can start earning money from parking straight away!</p>
 
  <div align="center"><input type="submit" name="submit" value="Apply Now" class="partner" ></div>
        
        </div>
    </div>
    </div>
</section>

<?php include_once("includes/footer.php"); ?> 