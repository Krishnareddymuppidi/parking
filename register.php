<?php
session_start();
ini_set("display_errors",0);
error_reporting(E_ERROR);

$user_details = 'user_details';
define("USER_DETAILS", $user_details);

$user_level_id = 'user_level_id';
define("USER_LEVEL_ID", $user_level_id);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Car Parking System</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- styles -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,600,700" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
  <link href="assets/css/docs.css" rel="stylesheet">
  <link href="assets/css/prettyPhoto.css" rel="stylesheet">
  <link href="assets/js/google-code-prettify/prettify.css" rel="stylesheet">
  <link href="assets/css/flexslider.css" rel="stylesheet">
  <link href="assets/css/jquery.dataTables.min.css" rel="stylesheet">
  <link href="assets/css/sequence.css" rel="stylesheet">
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/color/default.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="assets/css/jquery-ui.css">


  <!-- fav and touch icons -->
  <link rel="shortcut icon" href="assets/ico/favicon.ico">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    <!-- JavaScript Library Files -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/jquery.easing.js"></script>
  <script src="assets/js/google-code-prettify/prettify.js"></script>
  <script src="assets/js/modernizr.js"></script>
  <script src="assets/js/bootstrap.js"></script>
  <script src="assets/js/jquery.elastislide.js"></script>
  <script src="assets/js/sequence/sequence.jquery-min.js"></script>
  <script src="assets/js/sequence/setting.js"></script>
  <script src="assets/js/jquery.prettyPhoto.js"></script>
  <script src="assets/js/application.js"></script>
  <script src="assets/js/jquery.flexslider.js"></script>
  <script src="assets/js/hover/jquery-hover-effect.js"></script>
  <script src="assets/js/hover/setting.js"></script>
  <script type="text/javascript" src="assets/js/jquery-ui.js"></script>
  <script src="assets/js/jquery.dataTables.min.js"></script>

  <!-- Template Custom JavaScript File -->
  <script src="assets/js/custom.js"></script>
  <script src="https://maps.google.com/maps/api/js?sensor=fals&key=<?=$GOOGLE_API_KEY?>" type="text/javascript"></script>

<script>
$(document).ready(function(){
    $('input[type="radio"]').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        $(".box").not(targetBox).hide();
        $(targetBox).show();
    });
});
</script>
<style>
.box{
        color: #fff;
        padding: 20px;
        display: none;
        margin-top: 20px;
    }
    .owner, .customer
    {
        color: black;
    }
    label{ margin-right: 15px; }
</style>
</head>

<body>
<header>
    <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <!-- logo -->
          <a class="brand logo" href="index.php" style="color: #000000; font-size: 26px; margin-top: 10px;">
          <img src="images/logo-new.png" alt="logo" />
          </a>
          <!-- end logo -->
          <!-- top menu -->
          <div class="navigation">
            <nav>
              <ul class="nav topnav">
                <li class="dropdown"><a href="index.php">Home</a></li>
                <li class="dropdown"><a href="about.php">About</a></li>
                <li><a href="locations.php">All Parking Locations</a></li>
                <?php if($_SESSION[USER_DETAILS][USER_LEVEL_ID] == 1) {?>
                  <li><a href="login-home.php">Dashboard</a></li>
                  <li class="dropdown">
                            <a href="#">Administration</a>
                    <ul class="dropdown-menu">
                        <li><a href="location.php">Add Location</a></li>    
                        <li><a href="space.php">Add Parking Space</a></li>    
                        <li><a href="location-listing.php">Assign Parking</a></li>
                        <li><a href="user.php">Add System User</a></li>
                        <li><a href="search-parking.php">Search Car</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                    <a href="#">Report</a>
                    <ul class="dropdown-menu">
                        <li><a href="location-report.php">Location Report</a></li>
                        <li><a href="space-report.php">Parking Space Report</a></li>
                        <li><a href="parking-report.php">Parking Report</a></li>    
                        <li><a href="user-report.php">System User Report</a></li>
                        <?php
                        if($_SESSION[USER_DETAILS]['user_type'] == 'admin')
                        {
                        ?>
                        <li><a href="activitylog.php">Activity Log</a></li>    
                        <?php
                        }
                        ?>
                      </ul>
                    </li>
                <?php } if($_SESSION[USER_DETAILS][USER_LEVEL_ID] == 3 || $_SESSION[USER_DETAILS][USER_LEVEL_ID] == 4) {?>
                  <li><a href="login-home.php">Dashboard</a></li>
                <?php } ?>
                <?php
                if($_SESSION[USER_DETAILS][USER_LEVEL_ID] == 3)
                {
                    ?>
                <li><a href="bookparking.php">Book Parking</a></li>
                <?php
                }
                if($_SESSION[USER_DETAILS][USER_LEVEL_ID] == 2) {?>
                 <li><a href="login-home.php">Dashboard</a></li>
                <?php } if($_SESSION['login'] == 1) {?>
                  <li><a href="./user.php?user_id=<?php echo $_SESSION[USER_DETAILS]['user_id']; ?>">My Account</a></li>
                  <li><a href="change-password.php">Change Password</a></li>
                  <li><a href="./lib/login.php?act=logout">Logout</a></li>
                <?php } else { ?>
                  <li><a href="./login.php">Login</a></li>
                  <li><a href="./user.php">Register</a></li>
                  <li><a href="./contact.php">Contact Us</a></li>
                <?php }?>
              </ul>
            </nav>
          </div>
          <!-- end menu -->
        </div>
      </div>
    </div>
  </header>
 
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>New customers</h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section id="maincontent">
   <div class="container">
           <fieldset>
            <legend>New customers</legend>
            <p style="font-size: 14px; color: #455154;">Please select an option to continue</p>
                    <div class="row">
                    <div class="span3">
        <label><input type="radio" name="colorRadio" value="owner" style="margin-top: 0px;"> Become a Parking Owner</label>
        </div>
        <div class="span3">
        <label><input type="radio" name="colorRadio" value="customer" style="margin-top: 0px;"> Become a Customer</label>
   
    </div></div>
    <div class="owner box" style="line-height: 27px; margin-left: -18px;">
    
<b style="font-size: 14px;">Grow your business with DSS PMS</b>
<ul style="font-size: 14px;">
<li>Do you own a parking lot ? Join us and share your parking lot with our website.</li>
<li>You will get greater visibility to your parking lot and spaces through our website. </li>
</ul>
<b style="font-size: 14px;">How does it work?</b>
<p style="font-size: 14px;">We do great level of advertisement to the parking lots. Your site will be readily available and visible to all our registered customers. <br>
All you need to spend is a small percentage of your parking fee as our service fee. <br>
We maintain very transparent payment process. We publish daily reports to all the partners with daily business updates. </p>
<p style="font-size: 14px;">
Our application process is simple and straightforward! To join  DSS PMS  today, simply click on the "Apply now" button  and complete the application form. Our team will be in touch with you to set up your account so you can start earning money from parking straight away!
</p>
<a href="user.php?role=2" style="color: #fff; font-size: 15px; background: #6161c5; padding: 10px; border-radius: 5px;">Apply Now</a>    
    </div>
    <div class="customer box" style="line-height: 27px; font-size: 14px; margin-left: -18px;">
Do you want to become a customer with us, please click here <br><br>
<a href="user.php?role=3" style="color: #fff; font-size: 15px; background: #6161c5; padding: 10px; border-radius: 5px;">Apply Now</a>    
    </div>
    
            </fieldset>
    </div>
</section>
<?php include_once("includes/footer.php"); ?> 