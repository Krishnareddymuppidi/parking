<?php 
    include_once("includes/header.php"); 
    include_once("includes/db_connect.php"); 
    $SQL="SELECT * FROM `location`";
    $rs=mysqli_query($con,$SQL);
?>
<script>
function delete_location(location_id)
{
    if(confirm("Do you want to delete the parking location?"))
    {
        this.document.frm_location.location_id.value=location_id;
        this.document.frm_location.act.value="delete_location";
        this.document.frm_location.submit();
    }
}
</script>
<script>
jQuery(document).ready(function() {
    jQuery('#mydatatable').DataTable();
});
</script>
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>All Location Report</h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section id="maincontent">
   <div class="container">
           <fieldset>
            <legend>All Location Report</legend>
            <?php
            if($_REQUEST['msg']) 
            { 
            ?>
                <div class="alert alert-success" role="alert"><?=$_REQUEST['msg']?></div>
            <?php
            }
            if(mysqli_num_rows($rs)) 
            {
            ?>
            <form name="frm_location" action="lib/location.php" method="post">
                <div class="static">
                <table style="width:100%" id="mydatatable" class="table table-striped table-advance table-hover" >
                    <thead>
                      <tr class="tablehead bold">
                        <td scope="col">ID</td>
                        <td scope="col">Location Name</td>
                        <td scope="col">Contact</td>
                        <td scope="col">Latitude</td>
                        <td scope="col">Longitude</td>
                        <td scope="col">Action</td>
                      </tr>
                    </thead>
                    <tbody>
                    <?php 
                    $sr_no=1;
                    while($data = mysqli_fetch_assoc($rs))
                    {
                    ?>
                      <tr>
                        <td><?=$data[location_id]?></td>
                        <td><?=dec($data[location_name])?></td>
                        <td><?=dec($data[location_contact])?></td>
                        <td><?=dec($data[location_lat])?></td>
                        <td><?=dec($data[location_log])?></td>
                        <td style="text-align:center">
                        <a href="location.php?location_id=<?php echo $data[location_id] ?>" class="btn btn-primary">Edit</a> |     
                        <a href="Javascript:delete_location(<?=$data[location_id]?>)" class="btn btn-danger">Delete</a></td>
                      </tr>
                    <?php 
                    } 
                    ?>
                    </tbody>
                    </table>
                </div>
                <input type="hidden" name="act" />
                <input type="hidden" name="location_id" />
            </form>
            <?php 
            } 
            else 
            {
            ?>
                <div class="alert alert-success" role="alert">Locations are not available</div>
            <?php } ?>
            </fieldset>
    </div>
</section>
<?php include_once("includes/footer.php"); ?>