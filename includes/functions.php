<?php
session_start();
$SERVER_PATH = "https://eparkingnext.com/PMS/";
##Function for generating the dynamic options #######
function get_new_optionlist($table,$id_col,$value_col,$selected=0, $cond = 1)
{
    global $con;
    $SQL="SELECT * FROM $table WHERE $cond ORDER BY $value_col";
    $rs=mysqli_query($con,$SQL);
    $option_list="<option value=''>Please Select</option>";
    while($data=mysqli_fetch_assoc($rs))
    {
        if($selected==$data[$id_col])
        {
            $option_list.="<option value='$data[$id_col]' selected>$data[$value_col]</option>";
        }
        else
        {
            $option_list.="<option value='$data[$id_col]'>$data[$value_col]</option>";
        }
    }
    return $option_list;
}
##Function for generating the dynamic options #######
function get_checkbox($name,$table,$id_col,$value_col,$selected=0)
{
    global $con;
    $selected_array=explode(",",$selected);
    $SQL="SELECT * FROM $table ORDER BY $value_col";
    $rs=mysqli_query($con,$SQL);
    $option_list="<div class='checkboxlist'>";
    while($data=mysqli_fetch_assoc($rs))
    {
        if(in_array($data[$id_col],$selected_array))
        {
            $option_list.="<input type='checkbox' value='$data[$id_col]' name='".$name."[]' id='$name' checked>$data[$value_col]<br>";
        }
        else
        {
            $option_list.="<input type='checkbox' value='$data[$id_col]' name='".$name."[]' id='$name'>$data[$value_col]<br>";
        }
    }
    return $option_list."</div>";
}
## Function for getting vacant space #######
function get_vacant_space($location_id)
{
    global $con;
    $SQL="SELECT SUM(space_total_parkings) as total_space FROM space where space_location_id = ".$location_id;
    $rs=mysqli_query($con,$SQL);
    $data=mysqli_fetch_assoc($rs);
    return $data['total_space'];
}
## Function for getting vacant space #######
function get_occupied($location_id)
{
    global $con;
    $SQL="SELECT COUNT(*) as total_space FROM parking where parking_status = 1 AND parking_outtime = '' AND parking_space_id IN ( SELECT space_id from `space` WHERE space_location_id = ".$location_id.")";
    $rs=mysqli_query($con,$SQL);
    $data=mysqli_fetch_assoc($rs);
    return $data['total_space'];
}
?>