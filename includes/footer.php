<style>
#myBtn {
  /* position: fixed;
  bottom: 20px; */
  position: absolute;
    top: 203px;
  right: 30px;
  z-index: 99;
  font-size: 14px;
  border: none;
  outline: none;
  background-color: #385ee7;
  color: white;
  cursor: pointer;
  padding: 5px;
  border-radius: 4px;
}

#myBtn:hover {
  background-color: #555;
}
</style>
<a href="javascript:history.back()" id="myBtn" title="Go to top">Back</a>


<script>
//Get the button
var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } /*else {
    mybutton.style.display = "none";
  }*/
}
</script>
<!-- Footer
 ================================================== -->
 <footer class="footer" style="margin-top:30px;">
    <div class="container">
      <div class="row">
        <div class="span4">
          <div class="widget">
            <h5>Browse pages</h5>
            <ul class="regular">
              <li><a href="#">Parking Space</a></li>
              <li><a href="#">Space Management</a></li>
              <li><a href="register.php">Register</a></li>                    
              <li><a href="login.php">Login</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </div>
        </div>
        <div class="span4">
          <div class="widget">
            <h5>Modules of Project</h5>
            <ul class="regular">
              <li><a href="#">Parking Booking System</a></li>
              <li><a href="#">Space Management System</a></li>
              <li><a href="#">Google Map Management System</a></li>
              <li><a href="#">Booking Management System</a></li>                    
              <li><a href="#">User Management System</a></li>
              <li><a href="partner.php">Become a Partner</a></li>
              <li><a href="affiliate.php">Become an Affiliate</a></li>
            </ul>
          </div>
        </div>
        <div class="span4">
          <div class="widget">
            <!-- logo -->
            <a class="brand logo" href="index.php" style="color: #FFFFFF; font-size: 20px; margin-top: 35px;">
            Datadot Software Solution<br>
                        </a>
            <!-- end logo -->
            <address>
                            <!--<strong></strong><br> --><br>
                             ASIA PACIFIC HQ - MALAYSIA <br>
                             #55-11, The Boulevard Office, <br>
                             Mid Valley, 59200 Kuala Lumpur.<br>
                            <abbr title="Phone">P:</abbr> +60 3 2202 0041
                        </address>
          </div>
        </div>
      </div>
    </div>
    <div class="verybottom">
      <div class="container">
        <div class="row">
          <div class="span6">
            <p>
              &copy; Datadot Software Solution - All Rights Reserved
            </p>
          </div>
          <div class="span6">
            <div class="credits">              
              Designed by <a href="https://www.datadotlabs.com/" target="_blank">Datadot Software Solution</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
</body>
</html>