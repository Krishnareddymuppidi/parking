<?php 
    include_once("includes/header.php"); 
    if($_REQUEST[space_id])
    {
        $SQL="SELECT * FROM `discounts` WHERE space_id = $_REQUEST[space_id]";
        $rs=mysqli_query($con,$SQL);
        $data=mysqli_fetch_assoc($rs);
    }
?>
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>Parking Discount Entry Form</h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section id="maincontent">
   <div class="container">
           <fieldset>
            <legend>Parking Discount Entry Form</legend>
                <?php if($_GET['msg']) { ?>
                    <div class="alert alert-success" role="alert"><?=$_GET['msg']?></div>
                <?php } ?>
                
                <form action="lib/discountadd.php" enctype="multipart/form-data" method="post" name="frm_location" class="form-horizontal my-forms">
                    
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Discount/hr</label> 
                        <div class="controls"><input name="discount_hr" type="number" class="bar" oninvalid="setCustomValidity('Please Enter Rate/hr ')" oninput="setCustomValidity('')" onKeyDown="if(this.value.length==4 && event.keyCode!=4) return false;" required value="<?=dec($data[discount_hr])?>" /></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Discount/week</label> 
                        <div class="controls"><input name="discount_week" type="number" class="bar" oninvalid="setCustomValidity('Please Enter Rate/week ')" oninput="setCustomValidity('')" onKeyDown="if(this.value.length==4 && event.keyCode!=4) return false;" required value="<?=dec($data[discount_week])?>"  /></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Discount/month</label> 
                        <div class="controls"><input name="discount_month" type="number" class="bar" oninvalid="setCustomValidity('Please Enter Rate/month ')" oninput="setCustomValidity('')" onKeyDown="if(this.value.length==4 && event.keyCode!=4) return false;" required value="<?=dec($data[discount_month])?>"  /></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Discount/day</label> 
                        <div class="controls"><input name="discount_day" type="number" class="bar" oninvalid="setCustomValidity('Please Enter Rate/day ')" oninput="setCustomValidity('')" onKeyDown="if(this.value.length==4 && event.keyCode!=4) return false;" required value="<?=dec($data[discount_day])?>"  /></div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Discount Premium</label> 
                        <div class="controls"><input name="discount_premium" type="text" class="bar" required value="<?=dec($data[discount_premium])?>"   /></div>
                    </div>
                    
                    <div class="control-group clear">
                        <div class="controls"><button type="submit" class="btn btn-primary">Save Details</button>
                        <button type="reset" class="btn btn-danger">Reset Details</button> 
                    </div>
                    </div>
                    <input type="hidden" name="act" value="save_discount">
                    <input type="hidden" name="id" value="<?=$data[id]?>">
                    <input type="hidden" name="space_id" value="<?=$_REQUEST['space_id']?>">
                    <input type="hidden" name="user_id" value="<?=$_SESSION['user_details']['user_id']?>">
                    
                </form>
                </fieldset>
    </div>
</section>
<?php include_once("includes/footer.php"); ?> 