<?php 
    include_once("includes/header.php"); 
?> 
<section id="subintro">
   <div class="jumbotron subhead" id="overview">
      <div class="container">
         <div class="row">
            <div class="span12">
               <div class="centered">
                  <h3>Car Parking System</h3>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<section id="maincontent">
   <div class="container">
        <h4 class="heading colr">Become a Partner</h4>
        <div style="font-size:12px;">
            <strong>Grow your business with DSS PMS</strong>
            <ul>
<li>Do you own a parking lot ? Join us and share your parking lot with our website.</li>
<li>You will get greater visibility to your parking lot and spaces through our website. </li>
</ul>
<strong>How does it work?</strong>
<p>We do great level of advertisement to the parking lots. Your site will be readily available and visible to all our registered customers. </p>
<p>All you need to spend is a small percentage of your parking fee as our service fee. <p>
<p>We maintain very transparent payment process. We publish daily reports to all the partners with daily business updates. </p>

<p>Our application process is simple and straightforward! <strong>To join  DSS PMS  today, simply click on the "Apply now" button </strong> and complete the application form. Our team will be in touch with you to set up your account so you can start earning money from parking straight away!</p>
  <form action="./user.php" method="post">
  <div align="center"><input type="submit" name="submit" value="Apply Now" class="partner" ></div>
        </form>
        </div>
    </div>
    </div>
</section>

<?php include_once("includes/footer.php"); ?> 