<?php 
    //include_once("includes/header.php"); 
    include_once("includes/db_connect.php");
    $space_id= $_REQUEST["space_id"]; 
    $walk_from_date = $_REQUEST["walk_from_date"];
    $walk_to_date = $_REQUEST["walk_to_date"];
    $date = 'd-m-Y';
    define("DATE", $date);
    $new_walk_from_date = date(DATE, strtotime($walk_from_date));
    $new_walk_to_date = date(DATE, strtotime($walk_to_date));
?>
<style>
th
{
    background: #ddeeef;
    border: 1px solid #88707073;
	color: #000;
}
/*tr, td
{
    border: 1px solid #88707073;
}*/
#btnExport
{
    background: #2430bb;
    color: #fff;
    border: 2px solid #2430bb;
    border-radius: 4px;	
}
</style>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
    <script type="text/javascript">
        $("body").on("click", "#btnExport", function () {
            html2canvas($('#datatable')[0], {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("Walkin-report.pdf");
                }
            });
        });
    </script>
<script>
$(document).ready(function() {
    $('#datatable').dataTable();
    
     $("[data-toggle=tooltip]").tooltip();
    
} );

</script>
<?php
if($walk_from_date != '')
{
?>
    <?php
    $parking_mode = 'operator';
    $SQL="SELECT * FROM `parking` WHERE parking_space_id = '$space_id' AND parking_mode = '$parking_mode' AND parking_booked_date BETWEEN '$walk_from_date' AND  '$walk_to_date'";
    
    $rs1=mysqli_query($con,$SQL);
    $slots = mysqli_num_rows($rs1);
    if($slots > 0) 
    {
    ?>
        <div class="static" id="order_table3">
        <?php echo "Slots filled between ".$new_walk_from_date. " and " .$new_walk_to_date.  " is ". $slots;  ?>
        <p style="text-align: right;" ><input type="button" id="btnExport" value="Export to pdf" /></p>
		<p>&nbsp;</p>
        <table style="text-align: center; width: 800px; border: 1px solid #88707073;" id="datatable" class="table table-striped table-bordered" >
        <thead>
		<tr>
        <th>Parking Slot Number</th>
        <th>Parking Car Number</th>
        <th>Parking Charges</th>
        <th>Booking Date</th>
        </tr>
		</thead>
		<tbody>
		
        <?php
        while($data1=mysqli_fetch_assoc($rs1))
        {
        ?>
        <?php
        $booked_date = $data1['parking_booked_date'];
        $parking_booked_date = date(DATE, strtotime($booked_date));
        ?>
        <tr>
        <td><?= $data1['parking_slot_number'] ?></td>
        <td><?= $data1['parking_car_no'] ?></td>
        <td><?= dec($data1['parking_charges']) ?></td>
        <td><?= $parking_booked_date ?></td>
        </tr>
        <?php
        }
        ?>
		</tbody>
        </table>
        </div>
    <?php
    }
    else
    {
    ?>
        <div class="static" id="order_table3">
                Slots are not available
        </div>
    <?php
    }
    ?>
<?php
}
?>