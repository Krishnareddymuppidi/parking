<?php 
    include_once("includes/header.php"); 
    include_once("includes/db_connect.php"); 
    $SQL="SELECT * FROM `space` WHERE space_location_id = ".$_REQUEST['location_id'];
    $rs=mysqli_query($con,$SQL);
?>
<style>
.parking_space 
{
float:left; 
padding-top:30px;
width:280px; 
font-size:18px;
color:#101746;
font-weight:bold;
padding-left:10px;
}
</style>
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>All Parking Spaces</h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section id="maincontent">
   <div class="container">
           <fieldset>
            <legend>All Parking Space (Click on Parking to see all parking slots)</legend>
            <?php
            if($_GET['msg']) { 
            ?>
                <div class="alert alert-success" role="alert"><?=$_REQUEST['msg']?></div>
            <?php
            }
            if(mysqli_num_rows($rs)) {
            ?>
            <form name="frm_parking" action="lib/parking.php" method="post">
                <div class="static">
                    <?php 
                    $sr_no=1;
                    while($data = mysqli_fetch_assoc($rs))
                    {
                    ?>
                    <!--  For parking table slot numbers count based on space id  -->
                    <?php
                    $SQL1="SELECT * FROM `parking` WHERE parking_space_id = $data[space_id]";
                    $rs1=mysqli_query($con,$SQL1) or die(mysqli_error($con));
                    $data1 = mysqli_fetch_all($rs1);
                    $tot = mysqli_num_rows($rs1)
                    ?>
                    
                        <div style="float:left; border:1px solid; margin:5px;">
                            <table>
                                <tr>
                                    <td><a href="parking.php?space_id=<?=$data[space_id]?>&user=<?=$_SESSION['user_details']['user_id']?>"><img src="images/p.png" style="height:75px;"></a></td>
                                    <td class="parking_space"><?=$data['space_title']?></td>
                                    <td>
                                    <?php
                                    if($tot == $data[space_total_parkings])
                                    {
                                    ?>
                                    <a class="btn btn-primary" onclick="return booknow()">Book Now</a>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                    <a href="parking.php?space_id=<?=$data[space_id]?>&user=<?=$_SESSION['user_details']['user_id']?>" class="btn btn-primary">Book Now</a>
                                    <?php
                                    }
                                    ?>
                                    <?php
                                    if($_SESSION['user_details']['user_level_id'] == 2 || $_SESSION['user_details']['user_level_id'] == 4)
                                    {
                                    ?>
                                    <a href="priceadd.php?space_id=<?=$data[space_id]?>" class="btn btn-warning">Add/Edit Prices</a>
                                    <a href="discountadd.php?space_id=<?=$data[space_id]?>" class="discount">Add/Edit Discount</a></td>
                                    <?php
                                    }
                                    ?>
                                </tr>
                            </table>
                        </div>
                    <?php 
                    } 
                    ?>
                </div>
                <input type="hidden" name="act" />
                <input type="hidden" name="parking_id" />
            </form>
            <?php } else {?>
                <div class="alert alert-success" role="alert">You Parking Added Yet.</div>
            <?php } ?>
            </fieldset>
    </div>
</section>
<!-- For parking spaces exceed alert -->
<script>
function booknow()
{
    alert("Sorry currently no slots are available in this space please try in other space");
}
</script>
<?php include_once("includes/footer.php"); ?> 