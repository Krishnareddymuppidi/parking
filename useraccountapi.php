<?php
include_once("includes/db_connect.php");
header('Access-Control-Allow-Headers: *'); 
header('Access-Control-Allow-Methods: POST, GET, PUT, OPTIONS, PATCH, DELETE');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Authorization, Content-Type, x-xsrf-token, x_csrftoken, Cache-Control, X-Requested-With');
header("Content-Type:application/json");
 if (isset($_GET['parking_user_id']) && $_GET['parking_user_id']!="") {
    
   
   
	//include('db.php');
	$parking_user_id = $_GET['parking_user_id'];
	$SQLQRY="SELECT * FROM user WHERE user_id =".$parking_user_id;
	$result = mysqli_query($con,$SQLQRY);
	if(mysqli_num_rows($result)>0){
	   $rows = array();

//retrieve every record and put it into an array that we can later turn into JSON
$i=0;
while($r = mysqli_fetch_assoc($result)){
    /* For city */
    $user_city = dec($r['user_city']);
    $SQLQRY1="SELECT * FROM city WHERE city_id =".$user_city;
	$result1 = mysqli_query($con,$SQLQRY1);
	$r1 = mysqli_fetch_assoc($result1);
	
	
	/* For state */
    $user_state = dec($r['user_state']);
    $SQLQRY2="SELECT * FROM state WHERE state_id =".$user_state;
	$result2 = mysqli_query($con,$SQLQRY2);
	$r2 = mysqli_fetch_assoc($result2);
	
	
	/* For contry */
    $user_country = dec($r['user_country']);
    $SQLQRY3="SELECT * FROM country WHERE country_id =".$user_country;
	$result3 = mysqli_query($con,$SQLQRY3);
	$r3 = mysqli_fetch_assoc($result3);
	
	
    $rows[$i][user_id] = $r['user_id'];
    $rows[$i][user_name] = dec($r['user_name']);
    $rows[$i][user_email] = dec($r['user_email']);
    $rows[$i][user_mobile] = dec($r['user_mobile']);
    $rows[$i][user_add1] = dec($r['user_add1']);
    $rows[$i][user_add2] = dec($r['user_add2']);
    $rows[$i][user_city] = $r1['city_name'];
    $rows[$i][user_state] = $r2['state_name'];
    $rows[$i][user_country] = $r3['country_name'];

$i++;
}

//echo result as json
echo json_encode($rows);


	mysqli_close($con);
	}else{
		//response(NULL, NULL, 200,"No Record Found");
	
    	
            //$response["message"] = "Bookings not found";

            //echo json_encode($response);exit;
            $rows = array(
                "message" => "Users not found"
            ); 
		echo json_encode([$rows]);
		}
}

?>