<?php
    include_once("includes/header.php"); 
?>
<style>
/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
#mydatatable th
{
background-color: #464444 !important;    
}
.tab button
{
 font-size: 13px !important;
}
form
{
 margin: 0 0 0px !important;
}
td
{
    width: 320px;
    height: 160px;
    text-align: center;
    font-size: 15px;
    color: #fff;
}
</style>

<section id="subintro">
   <div class="jumbotron subhead" id="overview">
      <div class="container">
         <div class="row">
            <div class="span12">
               <div class="centered">
                  <h3>Dashboard for <?php echo dec($_SESSION['user_details']['user_name']); ?></h3>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<section id="maincontent">
   <div class="container">
            <?php
            if($_REQUEST['msg']) 
            { 
            ?>
                <div class="msg"><?=$_REQUEST['msg']?></div>
            <?php
            }
            ?>
            <?php
            if($_REQUEST['delmsg']) 
            { 
            ?>
                <div class="msg"><?=$_REQUEST['delmsg']?></div>
            <?php
            }
            ?>
            <div style="text-align: right; margin-top: -19px; font-size: 18px;">Welcome <?php  echo dec($_SESSION['user_details']['user_name']); ?></div>
            
            <table style="width: 100%; height: 395px;">
                <tr style="border: 0;">
                    <td style="width: 17%; position: absolute; border: 0;">
                        <?php if($_SESSION['user_details']['user_level_id'] == 1) {?>
                            <ul class='login-home'>
                                    <li><a href="location.php">Add Location</a></li>    
                                    <li><a href="space.php">Add Parking Space</a></li>    
                                    <li><a href="location-listing.php">Assign Parking</a></li>
                                    <li><a href="user.php">Add System User</a></li>
                                    <li><a href="search-parking.php">Search Car</a></li>
                                    <li><a href="location-report.php">Location Report</a></li>
                                    <li><a href="space-report.php">Parking Space Report</a></li>
                                    <li><a href="parking-report.php">Parking Report</a></li>    
                                    <li><a href="user-report.php">System User Report</a></li>    
                                    <li><a href="./user.php?user_id=<?php echo $_SESSION['user_details']['user_id']; ?>">My Account</a></li>
                                    <li><a href="change-password.php">Change Password</a></li>
                                    <li><a href="./lib/login.php?act=logout">Logout</a></li>
                            </ul>
                        <?php } ?>
                        <?php if($_SESSION['user_details']['user_level_id'] == 3) {?>
                            <ul class='login-home'>
                                    <!--<li><a href="./index.php">Home</a></li>
                                    <li><a href="./about.php">About Us</a></li>
                                    <li><a href="locations.php">All Parking Locations</a></li> -->
                                    <li><a href="operatorbookparking.php">Book Parking</a></li>
                                    <li><a href="./user.php?user_id=<?php echo $_SESSION['user_details']['user_id']; ?>">My Account</a></li>
                                    <!--<li><a href="mybookings.php">My Bookings</a></li>-->
                                    <li><a href="change-password.php">Change Password</a></li>
                                    <li><a href="./lib/login.php?act=logout">Logout</a></li>
                            </ul>
                        <?php } ?>
                        <?php if($_SESSION['user_details']['user_level_id'] == 2) {?>
                        <ul class='login-home'>
                        <li><a href="documents.php">Upload Documents</a></li>
                        </ul>
                        <?php
                        }
                        ?>
                        <?php if($_SESSION['user_details']['user_level_id'] == 2 || $_SESSION['user_details']['user_level_id'] == 4) {?>
                            <ul class='login-home'>
                                    <li><a href="operatorbookparking.php">Book Parking</a></li>
                                    <li><a href="operatorreport.php">Operator Report</a></li>
                                    <li><a href="slotscheck.php">Slots Check</a></li>
                                    <li><a href="vehicledetails.php">Vehicle Check</a></li>
                                    <li><a href="./user.php?user_id=<?php echo $_SESSION['user_details']['user_id']; ?>">My Account</a></li>
                                    <li><a href="change-password.php">Change Password</a></li>
                                    <li><a href="./lib/login.php?act=logout">Logout</a></li>
                            </ul>
                        <?php } ?>
                    </td>
                    <td style="border: 0; width: 80%; vertical-align: top; padding-top: 11px;">

<table>
<!-- For no.of reservations -->
<?php
$user_id = $_SESSION['user_details']['user_id'];
$created_by =  $_SESSION['user_details']['created_by'];
$parking_mode= 'operator';
$space_id= $_SESSION['user_details']['user_space_id'];
$today=date('Y-m-d');
$qry = "SELECT * FROM `parking` WHERE parking_booked_date = '".$today."' AND parking_space_id = '".$space_id."' AND parking_user_id = '".$user_id."' AND parking_mode = '".$parking_mode."'";
$rset=mysqli_query($con,$qry);
$reservations = mysqli_num_rows($rset);
?>
<!-- For already reservations & available -->
<?php
$query = "SELECT * FROM `space` WHERE space_owner_id = '".$created_by."' AND space_id = '".$space_id."'";
$rs=mysqli_query($con,$query);
$data = mysqli_fetch_assoc($rs);
$total_slots = $data['space_total_parkings'];

$query1 = "SELECT * FROM `space` JOIN parking ON space.space_id=parking.parking_space_id WHERE space.space_owner_id = '".$created_by."' AND space.space_id = '".$space_id."'";
$rs1=mysqli_query($con,$query1);
$filled = mysqli_num_rows($rs1);
?>
<!-- For today leaving cars -->
<?php
$qry1 = "SELECT * FROM `parking` WHERE parking_exit_date = '".$today."' AND parking_space_id = '".$space_id."' AND parking_user_id = '".$user_id."' AND parking_mode = '".$parking_mode."'";
$rset1=mysqli_query($con,$qry1);
$leaving = mysqli_num_rows($rset1);
?>
<tr>
<td style="background: #a255b3;">Avl spaces: <?=$total_slots-$filled ?></td>
<td style="background: #067dc1;">Already filled slots: <?=$filled?></td>
</tr>
<tr>
<td style="background: #f67c27;">No.of reservations today: <?=$reservations ?></td>
<td style="background: #a6a6a6;">Today leaving: <?=$leaving?></td>
</tr>
</table>
                    </td>
                </tr>
            </table>
   </div>
</section>
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>


<?php include_once("includes/footer.php"); ?>