<?php 
    include_once("includes/header.php"); 
    include_once("includes/db_connect.php"); 
    $SQL="SELECT * FROM `space`, `location` WHERE space_location_id = location_id";
    $rs=mysqli_query($con,$SQL);
?>
<script>
function delete_space(space_id)
{
    if(confirm("Do you want to delete the parking space?"))
    {
        this.document.frm_space.space_id.value=space_id;
        this.document.frm_space.act.value="delete_space";
        this.document.frm_space.submit();
    }
}
</script>
<script>
jQuery(document).ready(function() {
    jQuery('#mydatatable').DataTable();
});
</script>
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>Parking Space Report</h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section id="maincontent">
   <div class="container">
           <fieldset>
            <legend>Parking Space Report</legend>
            <?php
            if($_REQUEST['msg']) { 
            ?>
                <div class="alert alert-success" role="alert"><?=$_REQUEST['msg']?></div>
            <?php
            }
            if(mysqli_num_rows($rs)) {
            ?>
            <form name="frm_space" action="lib/space.php" method="post">
                <div class="static">
                <table style="width:100%" id="mydatatable" class="table table-striped table-advance table-hover" >
                    <thead>
                      <tr class="tablehead bold">
                        <td scope="col">ID</td>
                        <td scope="col">Location Name</td>
                        <td scope="col">Space Name</td>
                        <td scope="col">Capacity</td>
                        <td scope="col">Action</td>
                      </tr>
                    </thead>
                    <tbody>
                    <?php 
                    $sr_no=1;
                    while($data = mysqli_fetch_assoc($rs))
                    {
                    ?>
                      <tr>
                        <td><?=$data[space_id]?></td>
                        <td><?=dec($data[location_name])?></td>
                        <td><?=dec($data[space_title])?></td>
                        <td><?=dec($data[space_total_parkings])?> Parking Slots</td>
                        <td style="text-align:center">
                        <a class="btn btn-primary" href="space.php?space_id=<?php echo $data[space_id] ?>">Edit</a> |     
                        <a  class="btn btn-danger"href="Javascript:delete_space(<?=$data[space_id]?>)">Delete</a></td>
                      </tr>
                    <?php } ?>
                    </tbody>
                    </table>
                </div>
                <input type="hidden" name="act" />
                <input type="hidden" name="space_id" />
            </form>
            <?php } else {?>
                <div class="alert alert-success" role="alert">Spaces are not available.</div>
            <?php } ?>
            </fieldset>
    </div>
</section>
<?php include_once("includes/footer.php"); ?> 