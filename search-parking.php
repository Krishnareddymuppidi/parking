<?php 
    include_once("includes/header.php"); 
    if($_REQUEST[parking_id])
    {
        $SQL="SELECT * FROM `parking` WHERE parking_id = $_REQUEST[parking_id]";
        $rs=mysqli_query($con,$SQL);
        $data=mysqli_fetch_assoc($rs);
    }
    if($_REQUEST['space_id'])
    {
        $data['parking_space_id'] = $_REQUEST['space_id'];
        $data['parking_slot_number'] = $_REQUEST['slot_no'];
    }
?>
<section id="subintro">
    <div class="jumbotron subhead" id="overview">
        <div class="container">
            <div class="row">
            <div class="span12">
                <div class="centered">
                    <h3>Parking Space Reports</h3>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section id="maincontent">
   <div class="container">
           <fieldset>
            <legend>Search Car in Parking</legend>
                <?php if($_REQUEST['msg']) { ?>
                    <div class="alert alert-success" role="alert"><?=$_REQUEST['msg']?></div>
                <?php } ?>
                <form action="parking.php" enctype="multipart/form-data" method="post" name="frm_parking" class="form-horizontal my-forms">
                <div class="control-group">
                        <label class="control-label" for="inputEmail">Enter Car No.</label> 
                        <div class="controls"><input name="parking_car_no" type="text" class="bar" required value="<?=$data[parking_car_no]?>"/></div>
                    </div>
                    <div class="clear"></div>
                    <div class="control-group clear">
                        <div class="controls"><button type="submit" class="btn btn-primary">Search Details</button>
                        <button type="reset" class="btn btn-danger">Reset Details</button> 
                    </div>
                    <input type="hidden" name="act" value="save_parking">
                    <input type="hidden" name="parking_id" value="<?=$data[parking_id]?>">
                </form>
                </fieldset>
    </div>
</section>
<?php include_once("includes/footer.php"); ?> 