<?php
include_once("includes/header.php");
?>
    <link rel="stylesheet" href="qr_get/css/reset.css">
    <link rel="stylesheet" href="qr_get/css/styles.css">
    <script src="qr_get/js/jquery.min.js"></script>
    <script src="qr_get/js/webcam.min.js"></script>

    <script type="text/javascript" src="qr_get/js/qr/grid.js"></script>
    <script type="text/javascript" src="qr_get/js/qr/version.js"></script>
    <script type="text/javascript" src="qr_get/js/qr/detector.js"></script>
    <script type="text/javascript" src="qr_get/js/qr/formatinf.js"></script>
    <script type="text/javascript" src="qr_get/js/qr/errorlevel.js"></script>
    <script type="text/javascript" src="qr_get/js/qr/bitmat.js"></script>
    <script type="text/javascript" src="qr_get/js/qr/datablock.js"></script>
    <script type="text/javascript" src="qr_get/js/qr/bmparser.js"></script>
    <script type="text/javascript" src="qr_get/js/qr/datamask.js"></script>
    <script type="text/javascript" src="qr_get/js/qr/rsdecoder.js"></script>
    <script type="text/javascript" src="qr_get/js/qr/gf256poly.js"></script>
    <script type="text/javascript" src="qr_get/js/qr/gf256.js"></script>
    <script type="text/javascript" src="qr_get/js/qr/decoder.js"></script>
    <script type="text/javascript" src="qr_get/js/qr/qrcode.js"></script>
    <script type="text/javascript" src="qr_get/js/qr/findpat.js"></script>
    <script type="text/javascript" src="qr_get/js/qr/alignpat.js"></script>
    <script type="text/javascript" src="qr_get/js/qr/databr.js"></script>

    <script src="qr_get/js/effects.js"></script>
	
</head>
<body>

<div class="pageWrapper">
<br><br><br><br><br>
<h4>Please scan the QR Image Code to see the vehicle details.</h4>
    <div class="boxWrapper">
        <div id="example" style="width:320px; height:240px;"></div>
    </div>

    <div class="button">
        <a id="button" >Scan QR code</a>
    </div><br>

    <div class="boxWrapper auto">
        <div id="hiddenImg"></div>
        <div id="qrContent"><p>Vehicle number will be display here.</p></div>
    </div>
    <!-- Get the results from database via ajax call  -->
<div id="order_table"> 

</div>
</div>
<?php
include_once("includes/footer.php");
?>