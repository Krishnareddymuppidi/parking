<?php 
    include_once("includes/header.php"); 
    define("USER_DETAILS", 'user_details');
    $user_type = $_SESSION[USER_DETAILS]['user_type'];
    $user_level_id = $_SESSION[USER_DETAILS]['user_level_id'];
    $user_name = $_SESSION[USER_DETAILS]['user_name'];
    define("USER_TYPE", $user_type);
    define("USER_LEVEL_ID", $user_level_id);
    define("USER_NAME", $user_name);
    define("STATUS", $_REQUEST['status']);
    
    if(!defined(USER_TYPE))
    {
    header("Location:login.php");
    }
    
?> 
<style>

/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
#mydatatable th
{
background-color: #464444 !important;    
}

</style>
<script>
$(function(){
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    //alert(maxDate);
    $('#date').attr('min', maxDate);
});
$(function(){
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    //alert(maxDate);
    $('#date1').attr('min', maxDate);
});
$(function(){
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    //alert(maxDate);
    $('#date2').attr('min', maxDate);
});
$(function(){
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    //alert(maxDate);
    $('#date3').attr('min', maxDate);
});
</script>
<!-- For data table  -->
<script>
jQuery(document).ready(function() {
    jQuery('#mydatatable').DataTable();
});
jQuery(document).ready(function() {
    jQuery('#mydatatable1').DataTable();
});
jQuery(document).ready(function() {
    jQuery('#mydatatable2').DataTable();
});
jQuery(document).ready(function() {
    jQuery('#mydatatable3').DataTable();
});
</script>
<section id="subintro">
   <div class="jumbotron subhead" id="overview">
      <div class="container">
         <div class="row">
            <div class="span12">
               <div class="centered">
                  <h3>Dashboard for Car Parking System</h3>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<section id="maincontent">
   <div class="container">
            <?php
            if($_REQUEST['msg']) 
            { 
            ?>
                <div class="msg"><?=$_REQUEST['msg']?></div>
            <?php
            }
            ?>
            <?php
            if(STATUS) 
            { 
            ?>
                <div class="msg">Status updated successfully</div>
            <?php
            }
            ?>
            <?php
            if($_GET['resp'] == 'sent')
            {
            ?>
            <div class="msg">Documents sent successfully</div>
            <?php
            }
            ?>
            <?php
            if($_GET['resp'] == 'notsent')
            {
            ?>
            <div class="msg">Documents not sent</div>
            <?php
            }
            ?>
            <div style="text-align: right; margin-top: -19px; font-size: 18px;">Welcome <?php  echo dec(USER_NAME); ?></div>
            <fieldset>

            <legend>Welcome to Car Parking System</legend>
                <table style="width: 100%">
                    <tr>
                        <td style="width: 16%; position: absolute;">
                            <?php if(USER_LEVEL_ID == 1) {?>
                                <ul class='login-home'>
                                        <!--<li><a href="location.php">Add Location</a></li>    
                                        <li><a href="space.php">Add Parking Space</a></li>    
                                        <li><a href="location-listing.php">Assign Parking</a></li>-->
                                        <li><a href="user.php">Add System User</a></li>
                                        <!--<li><a href="search-parking.php">Search Car</a></li>-->
                                        <li><a href="location-report.php">Location Report</a></li>
                                        <li><a href="space-report.php">Parking Space Report</a></li>
                                        <li><a href="parking-report.php">Parking Report</a></li>    
                                        <li><a href="user-report.php">System User Report</a></li>    
                                        <li><a href="./user.php?user_id=<?php echo $_SESSION[USER_DETAILS]['user_id']; ?>">My Account</a></li>
                                        <li><a href="change-password.php">Change Password</a></li>
                                        <li><a href="./lib/login.php?act=logout">Logout</a></li>
                                </ul>
                            <?php } ?>
                            <?php if(USER_LEVEL_ID == 3) {?>
                                <ul class='login-home'>
                                        <li><a href="./index.php">Home</a></li>
                                        <li><a href="./about.php">About Us</a></li>
                                        <li><a href="locations.php">All Parking Locations</a></li>
                                        <li><a href="bookparking.php">Book Parking</a></li>
                                        <li><a href="./user.php?user_id=<?php echo $_SESSION[USER_DETAILS]['user_id']; ?>">My Account</a></li>
                                        <li><a href="mybookings.php">My Bookings</a></li>
                                        <li><a href="change-password.php">Change Password</a></li>
                                        <li><a href="./lib/login.php?act=logout">Logout</a></li>
                                </ul>
                            <?php } ?>
                            <?php if(USER_LEVEL_ID == 2) {?>
                            <ul class='login-home'>
                            <li><a href="documents.php">Upload Documents</a></li>
                            </ul>
                            <?php
                            }
                            ?>
                            <?php if(USER_LEVEL_ID == 2 || USER_LEVEL_ID == 4) {?>
                                <ul class='login-home'>
                                        <li><a href="./index.php">Home</a></li>
                                        <li><a href="./about.php">About Us</a></li>
                                        <li><a href="locations.php">All Parking Locations</a></li>
                                        <li><a href="location-listing.php">Book Parking</a></li>
                                        <li><a href="./user.php?user_id=<?php echo $_SESSION[USER_DETAILS]['user_id']; ?>">My Account</a></li>
                                        <li><a href="change-password.php">Change Password</a></li>
                                        <li><a href="./lib/login.php?act=logout">Logout</a></li>
                                </ul>
                            <?php } ?>
                        </td>
                        <td style="width: 80%; height: 400px; vertical-align: top; padding-top: 11px;">
<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'Waiting')" id="defaultOpen">Pending Documents</button>
  <button class="tablinks" onclick="openCity(event, 'Approval')">Apporoval Documents</button>
  <button class="tablinks" onclick="openCity(event, 'Price')">Parking Space Prices</button>
  <!--<button class="tablinks" onclick="openCity(event, 'Audit')">Transaction Log</button>-->
  <button class="tablinks" onclick="openCity(event, 'docstatus')">Document Status</button>
</div>

<div id="Waiting" class="tabcontent">
  <table class="table" id="mydatatable">
  <thead>
    <tr>
      <th scope="col">Owner Name</th>
      <th scope="col">Document1</th>
      <th scope="col">Document2</th>
      <th scope="col">Document3</th>
      <th scope="col">Status</th>
      <th scope="col">Valid dates</th>
      <th scope="col">Send</th>
    </tr>
  </thead>
  <tbody>
  <?php
        $SQLQRY="SELECT * FROM documents JOIN user ON documents.owner_id=user.user_id WHERE documents.status = 'Pending'";
        $rsdata=mysqli_query($con,$SQLQRY);
        if(mysqli_num_rows($rsdata)) 
        {
            while($datares=mysqli_fetch_assoc($rsdata))
            {
                define("DOC1", $datares['document1']);
                define("DOC2", $datares['document2']);
                define("DOC3", $datares['document3']);
                define("OWNER_ID", $datares['owner_id']);
                
  ?>
    <tr>
      <td><?php echo dec($datares['user_name']); ?></td>
      <td><a style="color: #666;" href="documents/<?php echo dec(DOC1).'.'.dec($datares['ext1']);  ?>" download><?php echo dec(DOC1).'.'.dec($datares['ext1']);  ?></a></td>
      <td><a style="color: #666;" href="documents/<?php echo dec(DOC2);  ?>" download><?php echo dec(DOC2);  ?></a></td>
      <td><a style="color: #666;" href="documents/<?php echo dec(DOC3);  ?>" download><?php echo dec(DOC3);  ?></a></td>
      <?php
      if($datares['valid_from_date']=='' && $datares['valid_to_date']=='')
      {
      ?>
      <td><a style="cursor: pointer;" onclick="return doccheck()"><?php echo $datares['status']; ?></a></td>
      <?php
      }
      else
      {
      ?>
      <td><a href="docstatus.php?id=<?php echo $datares['id']; ?>" onclick="javascript:docstatus($(this));return false;"><?php echo $datares['status']; ?></a></td>
      <?php
      }
      ?>
      <td>From: <input name="valid_from_date<?php echo OWNER_ID;?>" id="valid_from_date<?php echo OWNER_ID;?>" type="date" class="bar" value="<?php echo $datares['valid_from_date'];  ?>"    /><br>
        To: <input data-id4="<?php echo OWNER_ID;?>" name="valid_to_date" id="date1" type="date" class="date1" value="<?php echo $datares['valid_to_date'];  ?>" />
      </td>
    <td>
    <form action="legalmail.php" method="post">
    <input type="hidden" name="document1" value="<?= DOC1; ?>"  >
    <input type="hidden" name="document2" value="<?= DOC2; ?>"  >
    <input type="hidden" name="document3" value="<?= DOC3; ?>"  >
    <input type="hidden" name="ownername" value="<?= $datares['user_name']; ?>"  >
    <input type="hidden" name="owner_id" value="<?= OWNER_ID; ?>"  >
    <input type="hidden" name="space_location_id" value="<?= $datares['space_location_id']; ?>"  >
    <?php
    if($datares['islegal'] == '0')
    {
    ?>
    <button type="submit" style="background: #7e967f; color: #fff; border: 1px; margin-top: 15px;">Send for legal assisment</button>
    <?php
    }
    else
    {
    ?>
    Documents Sent for legal assisment
    <?php
    }
    ?>
    </form>
    </td>
    </tr>
    <!-- For documents dates validity -->
    <?php
            }
        }
        else
        {
    ?>
        <?php
        }
        ?>
  </tbody>
</table>
</div>

<div id="Approval" class="tabcontent">
  <table class="table" id="mydatatable1">
  <thead>
    <tr>
      <th scope="col">Owner Name</th>
      <th scope="col">Document1</th>
      <th scope="col">Document2</th>
      <th scope="col">Document3</th>
      <th scope="col">Status</th>
    </tr>
  </thead>
  <tbody>
  <?php
        $SQLQRY1="SELECT * FROM documents JOIN user ON documents.owner_id=user.user_id WHERE documents.status = 'Approved'";
        $rsdata1=mysqli_query($con,$SQLQRY1);
        if(mysqli_num_rows($rsdata1)) 
        {
            while($datares1=mysqli_fetch_assoc($rsdata1))
            {
                $document1 = $datares1['document1'];
                $document2 = $datares1['document2'];
                $document3 = $datares1['document3'];
                define("document1", $document1);
                define("document2", $document2);
                define("document3", $document3);
  ?>
    <tr>
      <td><?php echo dec($datares1['user_name']);  ?></td>
      <td><a style="color: #666;" href="documents/<?php echo document1;  ?>" download><?php echo dec(document1);  ?></a></td>
      <td><a style="color: #666;" href="documents/<?php echo document2;  ?>" download><?php echo dec(document2);  ?></a></td>
      <td><a style="color: #666;" href="documents/<?php echo document3;  ?>" download><?php echo dec(document3);  ?></a></td>
      
      <td><?php echo $datares1['status'];  ?></td>
    </tr>
   <?php
            }
        }
        else
        {
    ?>
        <?php
        }
        ?>
  </tbody>
</table>
</div>
<div id="Price" class="tabcontent">
  
  <table style="width:100%" id="mydatatable2" class="table table-striped table-advance table-hover" >
                    <thead>
                      <tr class="tablehead bold">
                        <td scope="col">Space Name</td>
                        <td scope="col">Owner Name</td>
                        <td scope="col">Hour Rate</td>
                        <td scope="col">Day Rate</td>
                        <td scope="col">Week Rate</td>
                        <td scope="col">Month Rate</td>
                        <td scope="col">Status</td>
                        <td scope="col">Valid Dates</td>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
        $SQLQRY2="SELECT * FROM prices JOIN space ON prices.space_id=space.space_id JOIN user ON prices.user_id=user.user_id";
        $rsdata2=mysqli_query($con,$SQLQRY2);
        if(mysqli_num_rows($rsdata2)) 
        {
            while($datares2=mysqli_fetch_assoc($rsdata2))
            {
                
                    ?>
                    <?php
                    $sel = 'selected="selected"';
                    define("SELECTED", $sel);
                    
                    ?>
                     <tr>
                        <td><?php echo dec($datares2['space_title']);  ?></td>
                        <td><?php echo dec($datares2['user_name']);  ?></td>
                        <td><?php echo dec($datares2['rate_hr']);  ?></td>
                        <td><?php echo dec($datares2['rate_day']);  ?></td>
                        <td><?php echo dec($datares2['rate_week']);  ?></td>
                        <td><?php echo dec($datares2['rate_month']);  ?></td>
                        <td style="text-align:center">
                         <select status-id="<?php echo $datares2['id'];?>" class="selectstatus" id="selectstatus">
            <option value="Inactive" <?php if($datares2['price_status'] == 'Inactive'){ echo SELECTED; } ?>>Inactive</option>
            <option value="active" <?php if($datares2['price_status'] == 'active'){ echo SELECTED; } ?>>Active</option>
         </select>
                        </td>
                        <td>
                        From: <input name="active_from_date<?php echo $datares2['id'];?>" id="active_from_date<?php echo $datares2['id'];?>" type="date" class="bar"  /><br>
                        To: <input data-id3="<?php echo $datares2['id'];?>" name="active_to_date" class="date3" type="date"  />
                        </td>
                      </tr>
                       
                      <?php
            }
        }
        ?>
                    </tbody>
                    </table>
</div>
<div id="Audit" class="tabcontent">
<br>
    <form>
        <table>
               <tr>
               <td width="200">&nbsp;</td>
               <td><input type="text" name="from_date" id="from_date"  placeholder="From Date" /></td> 
               <td><input type="text" name="to_date" id="to_date" placeholder="To Date" /></td>
               <td><input type="button" name="filter" id="filter" value="Search" class="btn btn-info" style="margin-top: -10px;" /></td>
                <div style="clear:both"></div> 
               </tr>
        </table>                
    </form>
<br />  
        <!-- Table data displaying here via ajax response-->
                <div id="order_table">  
                    
                </div> 
</div>

<div id="docstatus" class="tabcontent">
<br>
<table class="table" id="mydatatable3">
  <thead>
    <tr>
      <th scope="col">Owner Name</th>
	  <th scope="col">Status</th>
    </tr>
  </thead>
  <tbody>
  <?php
        $SQLQRY3="SELECT * FROM documents JOIN user ON documents.owner_id=user.user_id WHERE documents.status = 'Approved'";
		$rsdata3=mysqli_query($con,$SQLQRY3) or die(mysqli_error($con));
		if(mysqli_num_rows($rsdata3)) 
		{
			while($datares3=mysqli_fetch_assoc($rsdata3))
			{
  ?>
    <tr>
      <td><?php echo dec($datares3['user_name']);  ?></td>
      <td>
	  <select status-id1="<?php echo $datares3['id'];?>" class="selectstatus1" id="selectstatus1">
            
			<option value="Governmentprocess" <?php if ($datares3['doc_status'] == 'Governmentprocess') echo ' selected="selected"'; ?>>Government Process</option>
            <option value="Approved" <?php if ($datares3['doc_status'] == 'Approved') echo ' selected="selected"'; ?>>Approved</option>
			<option value="Pending" <?php if ($datares3['doc_status'] == 'Pending') echo ' selected="selected"'; ?>>Pending</option>
         </select>
	  </td>
    </tr>
   <?php
			}
		}
		else
		{
	?>
			
		<?php
		}
		?>
  </tbody>
</table>
</div>
                        </td>
                        </tr>
                </table>
            </fieldset>
   </div>
</section>

<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
<!-- For status change -->
<script>
function docstatus(anchor)
{
   var conf = confirm('Are you sure want to change this status?');
   if(conf)
      window.location=anchor.attr("href");
}
</script>
<script>
            $(document).ready(function(){
            $('.selectstatus').change(function(){
                
                var price_status = $(this).val();                  
                var id = $(this).attr("status-id"); 
            
                $.ajax({  
                          url:"pricestatus.php",  
                          method:"POST",  
                          data:{id: '' + id + '',price_status: '' + price_status + ''},  
                          success:function(data)  
                          {  
                                if(data==0)
                                {
                                    alert("Status Updated Successfully");
                                }
                                if(data==1)
                                {
                                    alert("Failed");
                                }
                          }
                     });
            });
        });
        </script>
        
        <script>
            $(document).ready(function(){
            $('.selectstatus1').change(function(){
                
                var doc_status = $(this).val();                  
                var id = $(this).attr("status-id1");
                $.ajax({  
                          url:"ownerdocstatus.php",  
                          method:"POST",  
                          data:{id: '' + id + '',doc_status: '' + doc_status + ''},  
                          success:function(data)  
                          {  
                                if(data==0)
                                {
                                    alert("Status Updated Successfully");
                                }
                                if(data==1)
                                {
                                    alert("Failed");
                                }
                          }  
                     });
            });
        });
        </script>
<script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'yy-mm-dd'   
           });  
           $(function(){  
                $("#from_date").datepicker();  
                $("#to_date").datepicker();  
           });  
           $('#filter').click(function(){  
                var from_date = $('#from_date').val();  
                var to_date = $('#to_date').val();  
                //alert(from_date);
                if(from_date>to_date)
                {
                    alert("From date should be less than to date");
                    return false;
                }
                else if(from_date != '' && to_date != '')  
                {  
                     $.ajax({  
                          url:"viewlog.php",  
                          method:"POST",  
                          data:{from_date:from_date, to_date:to_date},  
                          success:function(data)
                          {  
                               $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Date");  
                }  
           });  
      });  
</script>
 <script>
 
 $('.date3').on('change', function(){
    var id = $(this).attr("data-id3"); 
    var active_from_date = $("#active_from_date"+id).val();
    var active_to_date = $(this).val();
    //alert(active_from_date);
     if(active_from_date == '')
     {
         alert("Please select date");
     }
     else if(active_from_date>active_to_date)
     {
         alert("From date should be lesser than to date");
     }
     else
     {
        $.ajax({  
                          url:"owerprice.php",  
                          method:"POST",  
                          data:{id: '' + id + '',active_from_date: '' + active_from_date + '',active_to_date: '' + active_to_date + ''},  
                          success:function(data)  
                          {  
                                if(data==0)
                                {
                                    alert("Status Updated Successfully");
                                }
                                if(data==1)
                                {
                                    alert("Failed");
                                }
                          }  
                     }); 
         
     }
});
 </script>
 
 <script>
 
 $('.date1').on('change', function(){
    var id = $(this).attr("data-id4"); 
    var valid_from_date = $("#valid_from_date"+id).val();
    var valid_to_date = $(this).val();
    //alert(valid_to_date);
     if(valid_from_date == '')
     {
         alert("Please select date");
     }
     else if(valid_from_date>valid_to_date)
     {
         alert("From date should be lesser than to date");
     }
     else
     {
        $.ajax({  
                          url:"ownerspacestatus.php",  
                          method:"POST",  
                          data:{id: '' + id + '',valid_from_date: '' + valid_from_date + '',valid_to_date: '' + valid_to_date + ''},  
                          success:function(data)  
                          {  
                                if(data==0)
                                {
                                    alert("Status Updated Successfully");
                                    window.location.reload();
                                }
                                if(data==1)
                                {
                                    alert("Failed");
                                }
                          }  
                     });
     }
});
 </script>
 <script>
 function doccheck()
 {
     alert("Please select valid dates to chage this status");
 }
 </script>
 <div>&nbsp;</div>
 <div>&nbsp;</div>
 <div>&nbsp;</div>
 <div>&nbsp;</div>
 <div>&nbsp;</div>
 <div>&nbsp;</div>
 <div>&nbsp;</div>
 
<?php include_once("includes/footer.php"); ?>