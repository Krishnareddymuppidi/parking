<?php
include_once("includes/db_connect.php");
include_once("includes/functions.php");
$price = $_POST['price'];		
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Carparking system payment page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />

</head>
<body>
<div class="container">
<br>
<!-- Credit Card Payment Form - START -->

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <h3 class="text-center">Payment Details</h3>
                        <img class="img-responsive cc-img" src="http://www.prepbootstrap.com/Content/images/shared/misc/creditcardicons.png">
                    </div>
                </div>
                <div class="panel-body">
                <?php
                if($_POST['parking_entry_date1'] != '')
                {
                    $parking_exit_date = $_POST['parking_entry_date1'];
                }
                if($_POST['parking_entry_date2'] != '')
                {
                    $parking_exit_date = $_POST['parking_entry_date2'];
                }
                if($_POST['parking_exit_date'] != '')
                {
                    $parking_exit_date = $_POST['parking_exit_date'];
                }
                if($_POST['parking_exit_date1'] != '')
                {
                    $parking_exit_date = $_POST['parking_exit_date1'];
                }
                if($_POST['parking_outtime_hour'] != '')
                {
                    $parking_outtime = $_POST['parking_outtime_hour'];
                }
                if($_POST['parking_outtime_day'] != '')
                {
                    $parking_outtime = $_POST['parking_outtime_day'];
                }
                ?>
                
                <?php
                if($_POST['parking_mode'] != '')
                {
                    $parking_mode = $_POST['parking_mode'];
                }
                else
                {
                    $parking_mode = '';
                }
                ?>
                <?php
                $parking_car_no = mysqli_real_escape_string($con,$_POST['parking_car_no']);
                $parking_intime_hour = mysqli_real_escape_string($con,$_POST['parking_intime_hour']);
                $parking_intime_day = mysqli_real_escape_string($con,$_POST['parking_intime_day']);
                $parking_entry_date = mysqli_real_escape_string($con,$_POST['parking_entry_date']);
                $parking_entry_date_week = mysqli_real_escape_string($con,$_POST['parking_entry_date_week']);
                $parking_entry_date_month = mysqli_real_escape_string($con,$_POST['parking_entry_date_month']);
                $parking_entry_date_day = mysqli_real_escape_string($con,$_POST['parking_entry_date_day']);
                $parking_charges_hour = mysqli_real_escape_string($con,$_POST['parking_charges_hour']);
                $parking_charges_week = mysqli_real_escape_string($con,$_POST['parking_charges_week']);
                $parking_charges_month = mysqli_real_escape_string($con,$_POST['parking_charges_month']);
                $parking_charges_day = mysqli_real_escape_string($con,$_POST['parking_charges_day']);
                $parking_status = mysqli_real_escape_string($con,$_POST['parking_status']);
                $parking_description = mysqli_real_escape_string($con,$_POST['parking_description']);
                ?>
                <form action="lib/parking.php" enctype="multipart/form-data" method="post" name="frm_parking" class="form-horizontal my-forms">
                        <input type="hidden" name="parking_space_id" value="<?php echo $_POST['parking_space_id']; ?>" >
                        <input type="hidden" name="parking_slot_number" value="<?php echo $_POST['parking_slot_number']; ?>" >
                        <input type="hidden" name="parking_car_no" value="<?php echo $parking_car_no; ?>" >
                        <input type="hidden" name="parking_intime_hour" value="<?php echo $parking_intime_hour; ?>" >
                        <input type="hidden" name="parking_intime_day" value="<?php echo $parking_intime_day; ?>" >
                        <input type="hidden" name="parking_entry_date" value="<?php echo $parking_entry_date; ?>" >
                        <input type="hidden" name="parking_entry_date_week" value="<?php echo $parking_entry_date_week; ?>" >
                        <input type="hidden" name="parking_entry_date_month" value="<?php echo $parking_entry_date_month; ?>" >
                        <input type="hidden" name="parking_entry_date_day" value="<?php echo $parking_entry_date_day; ?>" >
                        <input type="hidden" name="parking_charges_hour" value="<?php echo $parking_charges_hour; ?>" >
                        <input type="hidden" name="parking_charges_week" value="<?php echo $parking_charges_week; ?>" >
                        <input type="hidden" name="parking_charges_month" value="<?php echo $parking_charges_month; ?>" >
                        <input type="hidden" name="parking_charges_day" value="<?php echo $parking_charges_day; ?>" >
                        
                        <input type="hidden" name="parking_mode" value="<?php echo $parking_mode; ?>" >
                        <input type="hidden" name="parking_exit_date" value="<?php echo $parking_exit_date; ?>" >
                        <input type="hidden" name="parking_outtime" value="<?php echo $parking_outtime; ?>" >
                        <input type="hidden" name="parking_status" value="<?php echo $parking_status; ?>" >
                        <input type="hidden" name="parking_description" value="<?php echo $parking_description; ?>" >
                        <input type="hidden" name="parking_user_id" value="<?php echo $_POST['parking_user_id']; ?>">
                        <input type="hidden" name="parking_space_image" value="<?php echo $_POST['parking_space_image']; ?>">
                        <input type="hidden" name="parking_type" value="<?php echo $price ?>" >
						<input type="hidden" name="act" value="save_parking">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>CARD NUMBER</label>
                                    <div class="input-group">
                                        <input type="tel" class="form-control" placeholder="Valid Card Number" />
                                        <span class="input-group-addon"><span class="fa fa-credit-card"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-7 col-md-7">
                                <div class="form-group">
                                    <label><span class="hidden-xs">EXPIRATION</span><span class="visible-xs-inline">EXP</span> DATE</label>
                                    <input type="tel" class="form-control" placeholder="MM / YY" />
                                </div>
                            </div>
                            <div class="col-xs-5 col-md-5 pull-right">
                                <div class="form-group">
                                    <label>CVV CODE</label>
                                    <input type="tel" class="form-control" placeholder="CVC" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>CARD OWNER</label>
                                    <input type="text" class="form-control" placeholder="Card Owner Names" />
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-warning btn-lg btn-block">Process payment</button>
                        </div>
                    </div>
                </div>
                </form>
                </div>
                
            </div>
        </div>
    </div>
</div>

<!-- Credit Card Payment Form - END -->

</div>

</body>
</html>