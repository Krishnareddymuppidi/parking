<?php
include_once("includes/db_connect.php");
header('Access-Control-Allow-Headers: *'); 
header('Access-Control-Allow-Methods: POST, GET, PUT, OPTIONS, PATCH, DELETE');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Authorization, Content-Type, x-xsrf-token, x_csrftoken, Cache-Control, X-Requested-With');
header("Content-Type:application/json");
if ($_SERVER['REQUEST_METHOD'] === 'DELETE' && isset($_GET['space_id']) && isset($_GET['slot_no'])) {

$space_id=$_GET['space_id'];
$slot_no=$_GET['slot_no'];
$SQLQRY2="SELECT * FROM parking WHERE parking_space_id = $space_id AND parking_slot_number = $slot_no";
$rsdata2=mysqli_query($con,$SQLQRY2);
$data=mysqli_fetch_assoc($rsdata2);    
    
    $parking_space_id = $data['parking_space_id'];
    $parking_slot_number = $data['parking_slot_number'];
    $parking_car_no = $data['parking_car_no'];
    $parking_intime = $data['parking_intime'];
    $parking_outtime = $data['parking_outtime'];
    $parking_entry_date = $data['parking_entry_date'];
    $parking_exit_date = $data['parking_exit_date'];
    $parking_charges = $data['parking_charges'];
    $parking_user_id = $data['parking_user_id'];
    $cancel_date= date('Y-m-d');
    $sql = "INSERT INTO parking_cancel (space_id,slot_id,car_no,intime,outtime,entry_date,exit_date,price,user_id,cancel_date) VALUES ('$parking_space_id','$parking_slot_number','$parking_car_no','$parking_intime','$parking_outtime','$parking_entry_date','$parking_exit_date','$parking_charges','$parking_user_id','$cancel_date')"; 
  
        // Execute query 
    $res= mysqli_query($con, $sql); 
    
    $SQL="DELETE FROM parking WHERE parking_space_id = $space_id AND parking_slot_number = $slot_no";
    $del_succ = mysqli_query($con,$SQL);
    if($del_succ)
    {
      $response=  "Slot cancelled successfully";
      $json_response = json_encode($response);
      echo $json_response;  
        
    }
    else
    {
      $response=  "Unable to cancel slot";
      $json_response = json_encode($response);
      echo $json_response;  
    }
}

?>