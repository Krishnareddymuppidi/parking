<?php 
    //include_once("includes/header.php"); 
    include_once("includes/db_connect.php");
    $space_id= $_REQUEST["space_id"]; 
    $cancel_from_date = $_REQUEST["cancel_from_date"];
    $cancel_to_date = $_REQUEST["cancel_to_date"];
    $date = 'd-m-Y';
    define("DATE", $date);
    $new_cancel_from_date = date(DATE, strtotime($cancel_from_date));
    $new_cancel_to_date = date(DATE, strtotime($cancel_to_date));
?>
<style>
th
{
    background: #ddeeef;
    border: 1px solid #88707073;
	color: #000;
}
/*tr, td
{
    border: 1px solid #88707073;
}*/
#btnExport
{
    background: #2430bb;
    color: #fff;
    border: 2px solid #2430bb;
    border-radius: 4px;	
}

</style>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
    <script type="text/javascript">
        $("body").on("click", "#btnExport", function () {
            html2canvas($('#datatable1')[0], {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("Cancellation-report.pdf");
                }
            });
        });
    </script>
<script>
$(document).ready(function() {
    $('#datatable1').dataTable();
    
     $("[data-toggle=tooltip]").tooltip();
    
} );

</script>
<?php
if($cancel_from_date != '')
{
?>
    <?php
    $SQL="SELECT * FROM `parking_cancel` WHERE space_id = '$space_id' AND cancel_date BETWEEN '$cancel_from_date' AND  '$cancel_to_date'";
    
    $rs1=mysqli_query($con,$SQL);
    $slots = mysqli_num_rows($rs1);
    if($slots > 0) 
    {
    ?>
        <div class="static" id="order_table3">
        <?php echo "Slots cancelled between ".$new_cancel_from_date. " and " .$new_cancel_to_date.  " is ". $slots;  ?>
        <p style="text-align: right;" ><input type="button" id="btnExport" value="Export to pdf" /></p>
		<p>&nbsp;</p>
        <table style="text-align: center; width: 800px; border: 1px solid #88707073;"  id="datatable1" class="table table-striped table-bordered">
        <thead>
		<tr>
        <th>Parking Slot Number</th>
        <th>Parking Car Number</th>
        <th>Parking Charges</th>
        <th>Cancelled Date</th>
        </tr>
		</thead>
		<tbody>
        <?php
        while($data1=mysqli_fetch_assoc($rs1))
        {
        ?>
        <?php
        $cancel_date = $data1['cancel_date'];
        $parking_cancel_date = date(DATE, strtotime($cancel_date));
        ?>
        <tr>
        <td><?= $data1['slot_id'] ?></td>
        <td><?= $data1['car_no'] ?></td>
        <td><?= dec($data1['price']) ?></td>
        <td><?= $parking_cancel_date ?></td>
        </tr>
        <?php
        }
        ?>
		</tbody>
        </table>
        </div>
    <?php
    }
    else
    {
    ?>
        <div class="static" id="order_table3">
                Cancelled slots are not available
        </div>
    <?php
    }
    ?>
<?php
}
?>