<?php 
    include_once("includes/header.php"); 
    $spaceid = 'space_id';
    define("SPACEID", $spaceid);
?> 
<section id="subintro">
   <div class="jumbotron subhead" id="overview">
      <div class="container">
         <div class="row">
            <div class="span12">
               <div class="centered">
                  <h3>Car Parking System</h3>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<script>
jQuery(document).ready(function() {
    jQuery('#mydatatable').DataTable();
});
</script>
<section id="maincontent">
   <div class="container">
        <h4 class="heading colr">Search Results</h4>
        <div style="font-size:12px;">
            <p>
            <?php
            $date = $_POST['parking_entry_date'];
            $parking_entry_date = date('d F,Y',strtotime($date));
            
            $SQLQRY = "SELECT * from parking WHERE parking_entry_date = '$parking_entry_date' AND parking_status=1";
                                $res = mysqli_query($con,$SQLQRY);
                                
                                 $rows = mysqli_num_rows($res);
                                if($rows == '0')
                                {
                                    ?>
                                    <?php
                                    $space_id = $_POST[SPACEID];
                                    $SQL="SELECT * FROM `space` WHERE space_id = $space_id";
                                    $rs=mysqli_query($con,$SQL);
                                    $data = mysqli_fetch_assoc($rs);
                                    ?>
                                    <div class="static">
                    <div style="float:left; border:1px solid; margin:5px;">
                        <?php for($i=1;$i<=$data['space_total_parkings'];$i++) { ?>
                            
                            <?php 
                            $SQL = "SELECT * from parking WHERE parking_entry_date = '$parking_entry_date' AND parking_space_id = ".$data[SPACEID]." AND parking_slot_number = $i AND parking_status = 1";
                            $rs = mysqli_query($con,$SQL);
                            if(!mysqli_num_rows($rs) ) { 
                            ?>
                            <div style="float:left; border:1px solid; margin:4px;">
                                <div style="float:left; padding:2px;"> 
                                    <a href="parking.php?space_id=<?=$data[SPACEID]?>&slot_no=<?=$i?>">
                                        <img src="images/available.png" style="height:60px;"> 
                                    </a>
                                </div>
                                <div style="text-align:center; font-weight:bold; font-size:13px;">Slot - <?=$i?></div></div>
                            <?php }  ?>
                            
                        <?php }?>
                    </div>
                </div>
                                <?php
                                }
                                else
                                {
                                ?>
            <div class="static">
                <table style="width:100%" id="mydatatable" class="table table-striped table-advance table-hover" >
                    <thead>
                      <tr class="tablehead bold">
                        <td scope="col">ID</td>
                        <td scope="col">Car No</td>
                        <td scope="col">User Name</td>
                        <td scope="col">Slot No</td>
                        <td scope="col">Entry Time</td>
                        <td scope="col">Entry Date</td>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    $SQLQRY = "SELECT * from parking WHERE parking_entry_date = '$parking_entry_date'";
                                $res = mysqli_query($con,$SQLQRY);
                                while($data = mysqli_fetch_assoc($res))
                                {
                    ?>
                      <tr>
                        <td><?=$data[parking_id]?></td>
                        <td><?=$data[parking_car_no]?></td>
                        <td>
                        <?php
                        $QRY = "SELECT * from user WHERE user_id = '$data[parking_user_id]'";
                                $res1 = mysqli_query($con,$QRY);
                                $data1=mysqli_fetch_assoc($res1);
                        ?>
                        <?=$data1[user_username]?></td>
                        <td><?=$data[parking_slot_number]?></td>
                        <td><?=$data[parking_intime]?></td>
                        <td><?=$data[parking_entry_date]?></td>
                      </tr>
                    <?php } ?>
                    </tbody>
                    </table>
                </div>
                <?php
                 }
                ?>
            </p>
        </div>
    </div>
    </div>
</section>
<?php include_once("includes/footer.php"); ?>