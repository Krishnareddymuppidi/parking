<?php
include_once("includes/db_connect.php");
header('Access-Control-Allow-Headers: *'); 
header('Access-Control-Allow-Methods: POST, GET, PUT, OPTIONS, PATCH, DELETE');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Authorization, Content-Type, x-xsrf-token, x_csrftoken, Cache-Control, X-Requested-With');
header("Content-Type:application/json");
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
   
	//include('db.php');
	$zipcode= enc($_POST["location_zipcode"]); 
	
	$SQLQRY="SELECT * FROM `location` JOIN `space` ON location.location_id=space.space_location_id WHERE location.location_zipcode LIKE '".$zipcode."%'";
	$result = mysqli_query($con,$SQLQRY);
	if(mysqli_num_rows($result)>0){
	   $rows = array();

//retrieve every record and put it into an array that we can later turn into JSON
$i=0;
while($r = mysqli_fetch_assoc($result)){
    $image=$r['space_image'];
    $rows[$i][space_id] = $r['space_id'];
    $rows[$i][space_title] = dec($r['space_title']);
    $rows[$i][location_name] = dec($r['location_name']);
    $rows[$i][space_image] = 'https://eparkingnext.com/PMS/spaces/'.$image;
    $rows[$i][message] = 'success';
$i++;
}

//echo result as json
echo json_encode($rows);


	mysqli_close($con);
	}else{
		//response(NULL, NULL, 200,"No Record Found");
    	
            //$response["message"] = "Bookings not found";

            //echo json_encode($response);exit;
            $rows = array(
                "message" => "Locations not found"
            ); 
		echo json_encode([$rows]);
		}
}

?>