<?php
include_once("includes/db_connect.php");
header('Access-Control-Allow-Headers: *'); 
header('Access-Control-Allow-Methods: POST, GET, PUT, OPTIONS, PATCH, DELETE');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Authorization, Content-Type, x-xsrf-token, x_csrftoken, Cache-Control, X-Requested-With');
header("Content-Type:application/json");
 if (isset($_GET['parking_id']) && $_GET['parking_id']!="") {
    
 	//include('db.php');
	$parking_id = $_GET['parking_id'];
	$date = date('Y-m-d');
	$SQLQRY="SELECT parking_id, parking_car_no, parking_intime, parking_outtime, parking_entry_date, parking_exit_date, parking_qr_image, parking_space_image FROM parking WHERE parking_date>'$date' AND parking_id =".$parking_id;
	$result = mysqli_query($con,$SQLQRY);
	if(mysqli_num_rows($result)>0){
	   $rows = array();

//retrieve every record and put it into an array that we can later turn into JSON
$i=0;
while($r = mysqli_fetch_assoc($result)){
    
    $image=dec($r['parking_space_image']);
    $rows[$i][parking_car_no] = $r['parking_car_no'];
    $rows[$i][parking_intime] = $r['parking_intime'];
    $rows[$i][parking_outtime] = $r['parking_outtime'];
    $rows[$i][parking_entry_date] = $r['parking_entry_date'];
    $rows[$i][parking_exit_date] = $r['parking_exit_date'];
    $rows[$i][parking_qr_image] = 'https://eparkingnext.com/PMS/qr_images/'.$r['parking_qr_image'];
    $rows[$i][parking_space_image] = 'https://eparkingnext.com/PMS/spaces/'.$image;
$i++;
}

//echo result as json
echo json_encode($rows);
	
	mysqli_close($con);
	}else{
		//response(NULL, NULL, 200,"No Record Found");
		$rows = array(
                "message" => "Bookings not found"
            ); 
		echo json_encode([$rows]);
		
		}
}

?>